#!/bin/bash

samples=32;
sm=025;
p=32;
film=ldrfilm
filmformat=png

for sh in $(seq  1 1 20)
do
	factor=$(echo "$sh*0.05" | bc)
	factor=1
	for i in $(seq 1 10 1)
	do
		../mitsuba-gl-build/binaries/mitsuba scenes/gl-triglobe-plate.xml -o output_plate/plate_$i\_$sh\_d.png  -Dshx=$factor -Dshy=$factor -Dshz=$factor -Dangle=$i -Dd=$sh -p 32
	done

done


