samples=128;
sm=025;
p=32;
film=ldrfilm
filmformat=png
b=4;
#film=hdrfilm
#filmformat=openexr

#bunny damask hair hair2 hair3 hairy_ball maple star twill velvet velvet_iso velvet_iso_light
for cam in 1; do

	for model in cedar_winter_1000; do

		echo $model;
		mtsgl=mitsuba
		
		# Render the input dataset
		# $mtsgl -o output/$model/$model\_ref_c$cam\_sm$sm.png \
		# 	   -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples \
		# 	   -Dmodel=$model -p $p -Dfilm=$film \
		# 	   -Dfilmformat=$filmformat -b $b \
		# 	   scenes/gl.xml 

		for lod in 5; do
			echo "";

			# $mtsgl scenes/gl-naive.xml \
			#	   -o output/$model/$model\_naive_c$cam\_l$lod\_sm$sm.png \
			#	   -Dlod=$lod -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples \
			#	   -Dmodel=$model -p $p -Dfilm=$film -Dfilmformat=$filmformat -b $b

			# Render the filtered data (aniso)
			$mtsgl -o output/$model/$model\_triglobe_c$cam\_l$lod\_sm$sm\_sp10.png \
				   -Dlod=$lod -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples \
				   -Dmodel=$model -p $p -Dfilm=$film -Dfilmformat=$filmformat -b $b \
				   scenes/gl-triglobe.xml

			# $mtsgl scenes/gl-triglobe-opt.xml \
			#	   -o output/$model/$model\_triglobe_opt_c$cam\_l$lod\_sm$sm.png \
			#	   -Dlod=$lod -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples \
			#	   -Dmodel=$model -p $p -Dfilm=$film -Dfilmformat=$filmformat

			# $mtsgl scenes/gl-triglobe-iso-shadowing.xml  \
			#	   -o output/$model/$model\_triglobe_iso_shadowing_c$cam\_l$lod\_sm$sm.png \
			#	   -Dlod=$lod -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples\
			#	   -Dmodel=$model -p $p -Dfilm=$film -Dfilmformat=$filmformat -b $b

			# $mtsgl scenes/gl-triglobe-mean-occlusion.xml \
			# 	   -o output/$model/$model\_triglobe_mean_occlusion_c$cam\_l$lod\_sm$sm.png \
			# 	   -Dlod=$lod -Dbounces=-1 -Dcamera=$cam -Dsamples=$samples \
			#	   -Dmodel=$model  -p $p -Dfilm=$film -Dfilmformat=$filmformat -b $b
			
		done;
	done;
done;
