#include <iostream>

#include "../volume/volume_test.h"
#include "../utils/utils.h"
#include "../utils/box3.h"
#include "../utils/ray.h"
#include "../sggx/sggx.h"

#include "../triglobes/coslobe_test.h"
#include "../triglobes/sinlobe_test.h"

void test_box3() {
	std::cout << "test_box3..." << std::endl;

	Box3 box(Eigen::Array3d(-1,-1,-1),Eigen::Array3d(1,1,1));
	std::random_device rd;
	std::default_random_engine sampler(rd());

	double mean = 0.0;
	int nb_loop = 2000;
	for (int i = 0; i < nb_loop; i++) {
		Ray ray = box.sampleRay(sampler, false);
		//std::cout << to_string(ray.d) << " " << to_string(ray.o) << " " << ray.tmax << std::endl;
		if (ray.d == Eigen::Vector3d(1,0,0)) {
			assert(ray.o(0) == -1.0);
			assert(ray.o(1) >= -1 && ray.o(1) <= 1);
			assert(ray.o(2) >= -1 && ray.o(2) <= 1);
			mean += ray.o(1) + ray.o(2);
			//std::cout << ray.o(1) << " " << ray.o(2) << std::endl;
		} else if (ray.d == Eigen::Vector3d(-1,0,0)) {
			assert(ray.o(0) == 1.0);
			assert(ray.o(1) >= -1 && ray.o(1) <= 1);
			assert(ray.o(2) >= -1 && ray.o(2) <= 1);
			mean += ray.o(1) + ray.o(2);
			//std::cout << ray.o(1) << " " << ray.o(2) << std::endl;
		} else if (ray.d == Eigen::Vector3d(0,1,0)) {
			assert(ray.o(1) == -1.0);
			assert(ray.o(0) >= -1 && ray.o(0) <= 1);
			assert(ray.o(2) >= -1 && ray.o(2) <= 1);
			mean += ray.o(0) + ray.o(2);
			//std::cout << ray.o(0) << " " << ray.o(2) << std::endl;
		} else if (ray.d == Eigen::Vector3d(0,-1,0)) {
			assert(ray.o(1) == 1.0);
			assert(ray.o(0) >= -1 && ray.o(0) <= 1);
			assert(ray.o(2) >= -1 && ray.o(2) <= 1);
			mean += ray.o(0) + ray.o(2);
			//std::cout << ray.o(0) << " " << ray.o(2) << std::endl;
		} else if (ray.d == Eigen::Vector3d(0,0,1)) {
			assert(ray.o(2) == -1.0);
			assert(ray.o(0) >= -1 && ray.o(0) <= 1);
			assert(ray.o(1) >= -1 && ray.o(1) <= 1);
			mean += ray.o(0) + ray.o(1);
			//std::cout << ray.o(0) << " " << ray.o(1) << std::endl;
		} else if (ray.d == Eigen::Vector3d(0,0,-1)) {
			assert(ray.o(2) == 1.0);
			assert(ray.o(0) >= -1 && ray.o(0) <= 1);
			assert(ray.o(1) >= -1 && ray.o(1) <= 1);
			mean += ray.o(0) + ray.o(1);
			//std::cout << ray.o(0) << " " << ray.o(1) << std::endl;
		} else {
			assert("test_box3: fail, should not be here" && 0);
		}
	}
	std::cout << "mean: (should be close to 0) " << mean / (nb_loop*2) << std::endl;
	std::cout << "test_box3 OK!" << std::endl;
}

void test_sampleSphere() {
	std::cout << "test_sampleSphere..." << std::endl;
	std::random_device rd;
	std::default_random_engine sampler(rd());
	for (int i = 0; i < 200; i++) {
		Eigen::Vector3d sample = sampleSphere(sampler);
		assert_almost_eq(sample.norm(), 1.0, 0.00000001);
		//std::cout << sample(0) << " " << sample(1) << " " << sample(2) << std::endl;
	}
	std::cout << "test_sampleSphere OK!" << std::endl;

}

//
// 
//
void test_sggx_convert_triglobe() {
	std::cout << "test_sggx_convert_triglobe..." << std::endl;
	SGGX sggx_1 = SGGX(Vector3d(1.0,0.5,0.2), Vector3d(0.0,0.0,0.0));
	TrigLobe triglobe_1 = sggx_1.toTrigLobe();
	assert(triglobe_1.n_cos == 4);
	assert(triglobe_1.n_sin == 6);
	assert(triglobe_1.w_cos == 0.3);
	assert(triglobe_1.w_sin == 0.4);
	assert(triglobe_1.XD_cos(0) == 1);
	assert(triglobe_1.XD_cos(1) == 0);
	assert(triglobe_1.XD_cos(2) == 0);
	assert(triglobe_1.XD_sin(0) == 0);
	assert(triglobe_1.XD_sin(1) == 0);
	assert(triglobe_1.XD_sin(2) == 1);

	SGGX sggx_2 = SGGX(Vector3d(0.2,1.0,0.5), Vector3d(0.0,0.0,0.0));
	TrigLobe triglobe_2 = sggx_2.toTrigLobe();
	assert(triglobe_2.n_cos == 4);
	assert(triglobe_2.n_sin == 6);
	assert(triglobe_2.w_cos == 0.3);
	assert(triglobe_2.w_sin == 0.4);
	assert(triglobe_2.XD_cos(0) == 0);
	assert(triglobe_2.XD_cos(1) == 1);
	assert(triglobe_2.XD_cos(2) == 0);
	assert(triglobe_2.XD_sin(0) == 1);
	assert(triglobe_2.XD_sin(1) == 0);
	assert(triglobe_2.XD_sin(2) == 0);

	// test also that axis are well computed
	Vector3d main_axis(7.0,-2.0,1.0);
	main_axis /= main_axis.norm();
	Vector3d X, Z;
	buildOrthonormalBasis(X, Z, main_axis);
	Matrix3d m_basis = makeMatrix(X, main_axis, Z);
	Matrix3d m_sggx = m_basis * makeMatrix(0.5,1.0,0.1) * m_basis.transpose();
	SGGX sggx_3 = SGGX(Vector3d(m_sggx(0,0),m_sggx(1,1),m_sggx(2,2)),
					   Vector3d(m_sggx(0,1),m_sggx(0,2),m_sggx(1,2)));
	TrigLobe triglobe_3 = sggx_3.toTrigLobe();
	//std::cout << triglobe_3.toString() << std::endl;
	assert_almost_eq(std::abs(triglobe_3.XD_cos.dot(main_axis)), 1.0, 1e-10);
	assert_almost_eq(std::abs(triglobe_3.XD_sin.dot(Z)), 1.0, 1e-10);
	std::cout << "test_sggx_convert_triglobe OK!" << std::endl;

	SGGX sggx_4 = SGGX(Vector3d(1.0,1.0,0.1), Vector3d(0.0,0.0,0.0));
	TrigLobe triglobe_4 = sggx_4.toTrigLobe();
	assert(triglobe_4.n_cos == 1);
	assert(triglobe_4.n_sin == 14);
	assert(triglobe_4.w_cos == 0);
	assert(triglobe_4.w_sin == 0.8);
	assert(triglobe_4.XD_sin(0) == 0);
	assert(triglobe_4.XD_sin(1) == 0);
	assert(triglobe_4.XD_sin(2) == 1);
	
}

int main(int argc, char *argv[])
{


	
	//test_sggx_convert_triglobe();
	
	// test Box3
	//test_box3();

	// test sampleSphere
	//test_sampleSphere();
	
	
	// test volume
	//VolumeTest volTest;
	//volTest.test_all();

	CosLobeTest coslobeTest;
	coslobeTest.test_all();

	SinLobeTest sinlobeTest;
	sinlobeTest.test_all();
	
	return 0;
}
