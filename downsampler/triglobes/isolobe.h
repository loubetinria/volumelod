#ifndef ISOLOBE_H
#define ISOLOBE_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include "../utils/utils.h"
#include "trigphase.h"

double iso_int_sigma_t(const Shadowing &sh);

double iso_int_sigma_ss(const Shadowing &sh);

double iso_sigma_t(Vector3d wi, const Shadowing &sh);

double iso_sigma_ss(Vector3d wi, const Shadowing &sh);

double iso_phase_ss(Vector3d wo, const Shadowing &sh);
	
double iso_phase_ms(Vector3d wo, const Shadowing &sh);

Vector3d iso_sample_ss(const Shadowing &sh, Sampler &sampler);

Vector3d iso_sample_ms(const Shadowing &sh, Sampler &sampler);

#endif // ISOLOBE_H
