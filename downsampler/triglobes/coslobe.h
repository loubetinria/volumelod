#ifndef COSLOBE_H
#define COSLOBE_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include "trigphase.h"

double coslobe_sigma_t(int n, Vector3d wi, Vector3d XD, const Shadowing &sh);

double coslobe_sigma(int n, Vector3d wi, Vector3d XD);

double coslobe_sigma_ss(int n, Vector3d wi, Vector3d XD, const Shadowing &sh);

double coslobe_phase_ss(int n, Vector3d wi, Vector3d wo,
						Vector3d XD, const Shadowing &sh);

double coslobe_D(int n, Vector3d w, Vector3d XD);

double coslobe_int_sigma_t(int n, Vector3d XD, const Shadowing &sh);

double coslobe_int_sigma_ss(int n, Vector3d XD, const Shadowing &sh);

double coslobe_phase_ms(int n, Vector3d wo, Vector3d XD, const Shadowing &sh);
	
Vector3d coslobe_sample_visible(int n, Vector3d wi, Vector3d XD,
								int &nb_it,
								Sampler &sampler);

void coslobe_sample_squared(int n, Vector3d wi, Vector3d XD,
							Vector3d &wo,
							Sampler &sampler);
	
double coslobe_pdf_squared(int n, Vector3d wi, Vector3d XD,
						   Vector3d wo);

Vector3d coslobe_sample_D(int n, Vector3d XD,
						  Sampler &sampler);

Vector3d coslobe_sample_sigma(int n, Vector3d XD,
							  Sampler &sampler);

double coslobe_pdf_sigma(int n, Vector3d w, Vector3d XD);

Vector3d coslobe_sample_ms(int n, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler);

Vector3d coslobe_sample_ss(int n, Vector3d wi, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler);
	
#endif // COSLOBE_H
