#include "trigphase.h"
#include "coslobe.h"
#include "sinlobe.h"
#include "isolobe.h"


// Not the most efficient implementation,
// But faster than std::pow. Only efficient for
// small values of n.
double fastpow(double x, int n) {
	int cur = 1;
	double cur_x = x;
	while (cur*2 <= n) {
		cur_x *= cur_x;
		cur *= 2;
	}
	while (cur < n) {
		cur_x *= x;
		cur += 1;
	}
	return cur_x;
}

double pdf_theta(double x, double ti_, double phi_, int n, double U) {
	double sinti = std::sin(ti_);
	double costi = std::cos(ti_);
	double cost = std::cos(x);
	double sint = std::sin(x);
	double sinphi = std::sin(phi_);
	double cosphi = std::cos(phi_);
	return -U+(2*sinti*sinti*cost*sint*sinphi*sinphi+2*sinti*sinti*sinphi*sinphi*x+8*sinti*sint*sinphi*costi*cosphi+4*costi*costi*cosphi*cosphi*x+x)/(M_PI*(6*costi*costi*cosphi*cosphi-2*costi*costi-2*cosphi*cosphi+3));
};

double pdf_phi(double x, double costi2_, double, int n, double U){
	double cosx =  std::cos(x);
	double cosx_to_2n = fastpow(cosx*cosx, n);
	return -U +(     -(3*costi2_-1)*(cosx_to_2n*cosx*cosx*cosx-1.0)*(2.0*n+1.0)
					 + 0.5*(2*costi2_-3)*(cosx_to_2n*cosx-1.0)*(2.0*n+3.0))/(8*costi2_*n+2*n+7);
};

Array3d TrigPhase::eval(Vector3d wi, Vector3d wo, Array3d albedo_ss, Array3d albedo_ms) const {

	double _sigma_ss_wi = sigma_ss(wi);
	double _sigma_t_wi = sigma_t(wi);
	double _phase_ss = phase_ss(wi, wo);

	Array3d res = albedo_ss * (_sigma_ss_wi/_sigma_t_wi) * _phase_ss;

	// add MS contribution only if there is some shadowing;
	if (sh.m_SA.trace() < 3.0) {
		double _phase_ms = phase_ms(wo);
		res += albedo_ms * (_sigma_t_wi-_sigma_ss_wi)/_sigma_t_wi * _phase_ms;
	}
	
	return res;
}


double TrigPhase::pdf(Vector3d wi, Vector3d wo, Array3d albedo_ss, Array3d albedo_ms) const {

	double _sigma_ss_wi = sigma_ss(wi);
	double _sigma_t_wi = sigma_t(wi);

	double albedo_ss_mean = albedo_ss.mean();
	double albedo_ms_mean = albedo_ms.mean();

	double weight_ss = albedo_ss_mean * _sigma_ss_wi;
	double weight_ms = albedo_ms_mean * (_sigma_t_wi-_sigma_ss_wi);
	double sum_weight = weight_ss+weight_ms;
	weight_ss /= sum_weight;
	weight_ms /= sum_weight;

	double _phase_ss = phase_ss(wi, wo);
	double res = weight_ss*_phase_ss;
	
	if (sh.m_SA.trace() < 3.0) {
		double _phase_ms = phase_ms(wo);
		res += weight_ms*_phase_ms;
	}
	
	return  res;
}

Vector3d TrigPhase::sample(Vector3d wi, Array3d albedo_ss, Array3d albedo_ms, Array3d &s,
						   double &pdf, Sampler &sampler) const {

	std::uniform_real_distribution<double> uniform_dist(0, 1);

	double _sigma_ss_wi = sigma_ss(wi);
	double _sigma_t_wi = sigma_t(wi);

	double albedo_ss_mean = albedo_ss.mean();
	double albedo_ms_mean = albedo_ms.mean();

	double weight_ss = albedo_ss_mean * (_sigma_ss_wi/_sigma_t_wi);
	double weight_ms = albedo_ms_mean * (_sigma_t_wi-_sigma_ss_wi)/_sigma_t_wi;
	double sum_weight = weight_ss+weight_ms;
	weight_ss /= sum_weight;
	weight_ms /= sum_weight;
	
	if (albedo_ss_mean == 0.0 && albedo_ms_mean == 0.0) {
		assert(0 && "Cannot sample if albedo is null");
	}
	
	Vector3d wo;
	double U = uniform_dist(sampler);

	while (true) {
		if (U <= weight_ss) {
			wo = sample_ss(wi, sampler);
		} else {
			if (sh.m_SA.trace() >= 3) {
				std::cout << "TrigPhase::sample DEBUG, U=" << U << std::endl;
				wo = sample_ss(wi, sampler);
			} else {
				wo = sample_ms(sampler);
			}
		}
		
		double _phase_ss = phase_ss(wi, wo);
		pdf = weight_ss*_phase_ss;
		s = albedo_ss * (_sigma_ss_wi/_sigma_t_wi) * _phase_ss;

		if (sh.m_SA.trace() < 3.0) {
			double _phase_ms = phase_ms(wo);
			pdf +=  weight_ms*_phase_ms;
			s += albedo_ms * (_sigma_t_wi-_sigma_ss_wi)/_sigma_t_wi * _phase_ms;
		}

		if (pdf > 0) {
			s /= pdf;
			return wo;
		} else {
			//Log(EWarn, "resampling, found pdf=0");
		}
	}
	return Vector3d();
}

Vector3d TrigPhase::sample_ss(Vector3d wi, Sampler &sampler) const {
	std::uniform_real_distribution<double> uniform_dist(0, 1);

	double weight_cos = tl.w_cos*coslobe_sigma_t(tl.n_cos, wi, tl.XD_cos, sh);
	double weight_sin = tl.w_sin*sinlobe_sigma_t(tl.n_sin, wi, tl.XD_sin, sh);
	double weight_iso = (1.0-tl.w_cos-tl.w_sin)*iso_sigma_t(wi, sh);
	double sum = weight_cos+weight_sin+weight_iso;
	weight_cos/=sum;
	weight_sin/=sum;
	weight_iso/=sum;

	double U2 = uniform_dist(sampler);
	Vector3d wo;
	if (U2 <= weight_cos) {
		wo = coslobe_sample_ss(tl.n_cos, wi, tl.XD_cos, sh, sampler);
	} else if (U2 <= weight_cos + weight_sin) {
		wo = sinlobe_sample_ss(tl.n_sin, wi, tl.XD_sin, sh, sampler);
	} else {
		wo = iso_sample_ss(sh, sampler);
	}
	return wo;
}

Vector3d TrigPhase::sample_ms(Sampler &sampler) const {
	std::uniform_real_distribution<double> uniform_dist(0, 1);

	double weight_cos = tl.w_cos*(coslobe_int_sigma_t(tl.n_cos, tl.XD_cos, sh)
								  - coslobe_int_sigma_ss(tl.n_cos, tl.XD_cos, sh));
	double weight_sin = tl.w_sin*(sinlobe_int_sigma_t(tl.n_sin, tl.XD_sin, sh)
								  - sinlobe_int_sigma_ss(tl.n_sin, tl.XD_sin, sh));
	double weight_iso = (1.0-tl.w_cos-tl.w_sin)*(iso_int_sigma_t(sh)
												 - iso_int_sigma_ss(sh));
	double sum = weight_cos+weight_sin+weight_iso;
	weight_cos/=sum;
	weight_sin/=sum;
	weight_iso/=sum;
	double U2 = uniform_dist(sampler);
	Vector3d wo;
	if (U2 <= weight_cos) {
		wo = coslobe_sample_ms(tl.n_cos, tl.XD_cos, sh, sampler);
	} else if (U2 <= weight_cos + weight_sin) {
		wo = sinlobe_sample_ms(tl.n_sin, tl.XD_sin, sh, sampler);
	} else {
		wo = iso_sample_ms(sh, sampler);
	}
	return wo;
}

double TrigPhase::sigma_t(Vector3d wi) const {
	return tl.w_cos*coslobe_sigma_t(tl.n_cos, wi, tl.XD_cos, sh)
		+ tl.w_sin*sinlobe_sigma_t(tl.n_sin, wi, tl.XD_sin, sh)
		+ (1.0-tl.w_cos-tl.w_sin)*iso_sigma_t(wi, sh);
}

/*
 * Careful: this is the ("single") scattering coefficient of the medium,
 * given by the coefficient for the single scattering lobe sigma_ss 
 * as well as the local multiple scatering given by sigma_ms.
 */
Array3d TrigPhase::sigma_s(Vector3d wi, Array3d albedo_ss, Array3d albedo_ms) const {
	double _sigma_ss_wi = sigma_ss(wi);
	double _sigma_t_wi = sigma_t(wi);
	return albedo_ss*_sigma_ss_wi + albedo_ms*(_sigma_t_wi-_sigma_ss_wi);
}

double TrigPhase::sigma_ss(Vector3d wi) const {
	return tl.w_cos*coslobe_sigma_ss(tl.n_cos, wi, tl.XD_cos, sh)
		+ tl.w_sin*sinlobe_sigma_ss(tl.n_sin, wi, tl.XD_sin, sh)
		+ (1.0-tl.w_cos-tl.w_sin)*iso_sigma_ss(wi, sh);
}
	
double TrigPhase::int_sigma_t() const {
	return tl.w_cos*coslobe_int_sigma_t(tl.n_cos, tl.XD_cos, sh)
		+ tl.w_sin*sinlobe_int_sigma_t(tl.n_sin, tl.XD_sin, sh)
		+ (1.0-tl.w_cos-tl.w_sin)*iso_int_sigma_t(sh);
}
   
double TrigPhase::int_sigma_ss() const {
	return tl.w_cos*coslobe_int_sigma_ss(tl.n_cos, tl.XD_cos, sh)
		+ tl.w_sin*sinlobe_int_sigma_ss(tl.n_sin, tl.XD_sin, sh)
		+ (1.0-tl.w_cos-tl.w_sin)*iso_int_sigma_ss(sh);
}

float TrigPhase::phase_ss(Vector3d wi, Vector3d wo) const {
	double weight_cos = tl.w_cos*coslobe_sigma_t(tl.n_cos, wi, tl.XD_cos, sh);
	double weight_sin = tl.w_sin*sinlobe_sigma_t(tl.n_sin, wi, tl.XD_sin, sh);
	double weight_iso = (1.0-tl.w_cos-tl.w_sin)*iso_sigma_t(wi, sh);
	double sum = weight_cos+weight_sin+weight_iso;
	weight_cos/=sum;
	weight_sin/=sum;
	weight_iso/=sum;
	return weight_cos*coslobe_phase_ss(tl.n_cos, wi, wo, tl.XD_cos, sh)
		+ weight_sin*sinlobe_phase_ss(tl.n_sin, wi, wo, tl.XD_sin, sh)
		+ weight_iso*iso_phase_ss(wo, sh);
}
	
double TrigPhase::phase_ms(Vector3d wo) const {
	if(sh.m_SA.trace() >= 3.0) {
		assert(0 && "phase_ms is not defined if there is no shadowing");
	}
	return (sigma_t(wo) - sigma_ss(wo))/(int_sigma_t()-int_sigma_ss());
}
	
