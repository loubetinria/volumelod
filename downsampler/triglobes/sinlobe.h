#ifndef SINLOBE_H
#define SINLOBE_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include "trigphase.h"

double sinlobe_sigma_t(int n, Vector3d wi, Vector3d XD, const Shadowing &sh);

double sinlobe_sigma(int n, Vector3d wi, Vector3d XD);

double sinlobe_sigma_ss(int n, Vector3d wi, Vector3d XD, const Shadowing &sh);

double sinlobe_int_sigma_t(int n, Vector3d XD, const Shadowing &sh);

double sinlobe_int_sigma_ss(int n, Vector3d XD, const Shadowing &sh);

double sinlobe_phase_ss(int n, Vector3d wi, Vector3d wo,
						Vector3d XD, const Shadowing &sh);

double sinlobe_D(int n, Vector3d w, Vector3d XD);
	
double sinlobe_phase_ms(int n, Vector3d wo, Vector3d XD, const Shadowing &sh);

Vector3d sinlobe_sample_visible(int n, Vector3d wi, Vector3d XD,
								int &nb_it,
								Sampler &sampler);

double sinlobe_pdf_sigma(int n, Vector3d w, Vector3d XD);

Vector3d sinlobe_sample_sigma(int n, Vector3d XD,
							  Sampler &sampler);

Vector3d sinlobe_sample_ms(int n, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler);

Vector3d sinlobe_sample_ss(int n, Vector3d wi, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler);
	

#endif // SINLOBE_H
