#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <Eigen/Core>
#include <Eigen/Eigenvalues> 

#include "sinlobe.h"
#include "../utils/utils.h"
#include "trigphase.h"
#include "matrixsinlobe.h"
#include "coslobe.h"

double sinlobe_sigma_t(int n, Vector3d wi, Vector3d XD, const Shadowing &sh) {
	return sinlobe_sigma(n, wi, XD)*sh.shadowing(wi);
}


double sinlobe_sigma(int n, Vector3d wi, Vector3d XD) {
	assert(n>0 && n<=20);
	
	// dx = (D.wi)
	double dDI2 = XD.dot(wi);
	dDI2*=dDI2;
	// sum of (n-1) term starting from 2 (3, 4, 5, ..)
	int start_id_array = (n-1)*(4+n-2)/2;

	double cur = 1;
	double pol = 0;
	for (int i = 0; i <= n; i++) {
		pol += sin_att[start_id_array+i]*cur;
		cur *= dDI2;
	}

	// also multiply by shadowing
	return pol;
}


double sinlobe_sigma_ss(int n, Vector3d wi, Vector3d XD, const Shadowing &sh) {
	assert(n>0 && n<=20);

	double dDI = XD.dot(wi);
	double dDI2 = dDI*dDI;
	double shAI = sh.shadowing(wi);
	double shAD = sh.shadowing(XD);
	double sum_ai = sh.m_SA.trace();

	double res = 0;
	// boucle sur lignes de M1
	double cur_dDI = 1;
	for (int l = 0; l < n+1; l++) {
		res += cur_dDI*sin_ss_M1[n-1][3*l]*shAI;
		res += cur_dDI*sin_ss_M1[n-1][3*l+1]*shAD;
		res += cur_dDI*sin_ss_M1[n-1][3*l+2]*(sum_ai);
		cur_dDI*=dDI2;
	}

	double aux = wi.dot(sh.m_SA*(XD)); 

	cur_dDI = dDI*aux;
	for (int l = 0; l < n; l++) {
		res += cur_dDI*sin_ss_M2[n-1][l];
		cur_dDI*=dDI2;
	}
	return shAI*res; // add const
}



double sinlobe_int_sigma_t(int n, Vector3d XD, const Shadowing &sh) {
	double aux = sh.shadowing(XD);
	double res = (3*n+4)*(sh.m_SA.trace())-n*aux;
	return res *= M_PI/(8*n+12);
}

double sinlobe_int_sigma_ss(int n, Vector3d XD, const Shadowing &sh) {

	double XD_SA_XD = XD.dot(sh.m_SA * XD);
	double res = 45*n*(n-1)*XD_SA_XD*XD_SA_XD;
	double D_SA_SA_D = XD.dot(sh.m_SA * sh.m_SA * XD);
	res += 10*n*(1-5*n)*D_SA_SA_D;
	double SASA_trace = (sh.m_SA*sh.m_SA).trace();
	double SA_trace = sh.m_SA.trace();
	res += -n*(19*n+25)*2.0*(XD_SA_XD*SA_trace - D_SA_SA_D);
	res += (29*n*n+91*n+80)*SASA_trace
		+ (31*n*n+105*n+80)*(SA_trace*SA_trace-SASA_trace);
	res *= M_PI/(192*n*n+768*n+720);
	return res;
}

double sinlobe_phase_ss(int n, Vector3d wi, Vector3d wo, Vector3d XD, const Shadowing &sh) {
	return sh.shadowing(wi)*sh.shadowing(wo)*sinlobe_D(n,(wi+wo)/(wi+wo).norm(),XD)/(4*sinlobe_sigma_ss(n, wi, XD, sh));
}

double sinlobe_D(int n, Vector3d w, Vector3d XD) {
	return std::pow(1-(w.dot(XD))*(w.dot(XD)),n)/sin_D_norm[n-1];
}

double sinlobe_phase_ms(int n, Vector3d wo, Vector3d XD, const Shadowing &sh) {
	return (sinlobe_sigma_t(n, wo, XD, sh)-sinlobe_sigma_ss(n, wo, XD, sh))/(sinlobe_int_sigma_t(n, XD, sh)-sinlobe_int_sigma_ss(n, XD, sh));
}

Vector3d sinlobe_sample_visible(int n, Vector3d wi, Vector3d XD,
								int &nb_it,
								Sampler &sampler) {
  
	// find ortho to XD
	Vector3d X,Y;
	buildOrthonormalBasis(X, Y, XD);
 	X /= X.norm();
	Y /= Y.norm();

	Matrix3d world_to_local;
	world_to_local << X(0), X(1), X(2),
		Y(0), Y(1), Y(2),
		XD(0), XD(1), XD(2);

	double total_projected_area = sinlobe_sigma(n, wi, XD);
	double sum_projected_area = 0;
	
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double U1 = uniform_dist(sampler);
	Vector3d sampled_axis;

	// for tests
	Vector3d axis_local_0(1, 0, 0);
	Vector3d axis_world_0 = world_to_local.transpose() * axis_local_0;
	assert_almost_eq(axis_world_0.norm(), 1.0, 0.0000001, DEBUG_INFO);
	assert_almost_eq(axis_world_0.dot(XD), 0.0, 0.0000001, DEBUG_INFO);
	
	for (int i = 0; i < 2*n; i++) {
		Vector3d axis_local(std::cos(i*M_PI/(2.0*n)), std::sin(i*M_PI/(2.0*n)), 0);
		Vector3d axis_world = world_to_local.transpose() * axis_local;
		assert_almost_eq(axis_world.dot(axis_world_0), std::cos(i*M_PI/(2.0*n)), 0.0000001, DEBUG_INFO);
		assert_almost_eq(axis_world.dot(XD), 0.0, 0.0000001, DEBUG_INFO);
		assert_almost_eq(axis_world.norm(), 1.0, 0.0000000001, DEBUG_INFO);

		sum_projected_area += coslobe_sigma(n, wi, axis_world)/(2*n);
		if (sum_projected_area >= U1 * total_projected_area) {
			sampled_axis = axis_world;
			break;
		}
		assert(i != 2*n-1);
	}


	return coslobe_sample_visible(n, wi, sampled_axis, nb_it, sampler);
}

double sinlobe_pdf_sigma(int n, Vector3d w, Vector3d XD) {
	return sinlobe_sigma(n, w, XD)/M_PI;
}



Vector3d sinlobe_sample_sigma(int n, Vector3d XD,
							  Sampler &sampler) {
	// sin = sum of cos lobes
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double U1 = uniform_dist(sampler);

	Vector3d res_local = coslobe_sample_sigma(n,
											  Vector3d(std::cos(U1*2*M_PI),
													   std::sin(U1*2*M_PI),
													   0.0),
											  sampler);
	Vector3d X,Y;
	Matrix3d world_to_local;
	buildOrthonormalBasis(X, Y, XD);
	world_to_local << X(0), X(1), X(2),
		Y(0), Y(1), Y(2),
		XD(0), XD(1), XD(2);
	
	Vector3d res = world_to_local.transpose()*res_local;
	return res/res.norm();
}

Vector3d sinlobe_sample_ms(int n, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler) {

	assert(sh.hasEigenv()
		   && "this implementation of sample_ms needs eigenvalues of SA");
	if (!(sh.m_SA.trace() < 3.0)) {
		std::cout << "sh.m_SA.trace() " << sh.m_SA.trace() << std::endl;
	}
	assert(sh.m_SA.trace() < 3.0);
	Vector3d eg_val = sh.getEigenvalues();
	double Amin = std::min(std::min(eg_val(0),eg_val(1)),eg_val(2));

	std::uniform_real_distribution<double> uniform_dist(0, 1);

	int nb_it = 1;
	while (true) {
	
		double U = uniform_dist(sampler);
		Vector3d w = sinlobe_sample_sigma(n, XD, sampler);
		
		double P = (sinlobe_sigma_t(n,w,XD,sh) - sinlobe_sigma_ss(n,w,XD,sh))
			/(sh.max()*(1.0-Amin)*sinlobe_sigma(n,w,XD));

		if (U < P) {
			return w;
		}
		nb_it++;
	}
}

Vector3d sinlobe_sample_ss(int n, Vector3d wi, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler) {

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	int nb_it_info = 0;
	int nb_it = 1;
	while (true) {
		Vector3d wo = sinlobe_sample_visible(n, wi, XD, nb_it_info, sampler);
		double U = uniform_dist(sampler);
		double P = sh.shadowing(wo) / sh.max();
		if (U < P) {
			return wo;
		}
		nb_it++;
	}
}
