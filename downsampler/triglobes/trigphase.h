#ifndef TRIGPHASE_H
#define TRIGPHASE_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include "../utils/utils.h"
#include <Eigen/Eigenvalues> // solver
#include <iostream>
#include "../utils/shadowing.h"

struct TrigLobe {
	/// Used by triglobe based phase function, Distribution
	int n_cos, n_sin;
	Vector3d XD_cos, XD_sin;
	double w_cos, w_sin;

	TrigLobe() {
		n_cos = 1;
		n_sin = 1;
		XD_cos = Vector3d(1,0,0);
		XD_sin = Vector3d(0,0,1);
		w_cos = 0;
		w_sin = 0;
	}
	
    TrigLobe(int n_cos, int n_sin,
			 Vector3d XD_cos, Vector3d XD_sin,
			 double w_cos, double w_sin)
	: n_cos(n_cos), n_sin(n_sin), XD_cos(XD_cos),
		XD_sin(XD_sin), w_cos(w_cos), w_sin(w_sin) {
	}

	std::string toString() const {
		std::string s = "    tl[       w[" + to_string(w_cos) + "," + to_string(w_sin) + "]\n"
			          + "              n[" + to_string(n_cos) + "," + to_string(n_sin) + "]\n"
			          + "         XD_cos[" + to_string(XD_cos) + "]\n"
			          + "         XD_sin[" + to_string(XD_sin) + "]     ]\n";
		return s;
	}
	
};
	

class TrigPhase {
  public:

	TrigPhase();

    TrigPhase(TrigLobe tl, Shadowing sh) : tl(tl), sh(sh) {};

	Array3d eval(Vector3d wi, Vector3d wo, Array3d albedo_ss, Array3d albedo_ms) const;
	
	double pdf(Vector3d wi, Vector3d wo, Array3d albedo_ss, Array3d albedo_ms) const;
	
	Vector3d sample(Vector3d wi, Array3d albedo_ss, Array3d albedo_ms, Array3d &s,
					double &pdf, Sampler &sampler) const;

	double sigma_t(Vector3d wi) const;
	
	Array3d sigma_s(Vector3d wi, Array3d albedo_ss, Array3d albedo_ms) const;

	Vector3d sample_ss(Vector3d wi, Sampler &sampler) const;
	
	double sigma_ss(Vector3d wi) const;

	double int_sigma_t() const;
	
	
	double int_sigma_ss() const;

	Vector3d sample_ms(Sampler &sampler) const;

	
  private:

	

	// COMBINED LOBES

	float phase_ss(Vector3d wi, Vector3d wo) const;
	
	double phase_ms(Vector3d wo) const;

	TrigLobe tl;
	Shadowing sh;

};

#endif // TRIGPHASE_H
