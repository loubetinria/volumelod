#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>   // std::string, std::to_string
#include <Eigen/Core>
#include <Eigen/Eigenvalues> 

#include "coslobe.h"
#include "../utils/utils.h"
#include "matrixcoslobe.h"

#include "../utils/brent.h"
#include "../utils/utils.h"

// Set precision of numerical invertion using Brent's method
#define BRENT_TOL 0.01 

double coslobe_sigma_t(int n, Vector3d wi, Vector3d XD, const Shadowing &sh) {
	return coslobe_sigma(n, wi, XD)*sh.shadowing(wi);
}


double coslobe_sigma(int n, Vector3d wi, Vector3d XD) {
	assert(n>0 && n<=20);
	
	// dx = (D.wi)
	double dDI2 = XD.dot(wi);
	dDI2*=dDI2;
	// sum of (n-1) term starting from 2 (3, 4, 5, ..)
	int start_id_array = (n-1)*(4+n-2)/2;

	double cur = 1;
	double pol = 0;
	for (int i = 0; i <= n; i++) {
		pol += cos_att[start_id_array+i]*cur;
		cur *= dDI2;
	}

	// also multiply with shadowing
	return pol;
}


double coslobe_sigma_ss(int n, Vector3d wi, Vector3d XD, const Shadowing &sh) {
	assert(n>0 && n<=20);

	double dDI = XD.dot(wi);
	double dDI2 = dDI*dDI;
	double shAI = sh.shadowing(wi);
	double shAD = sh.shadowing(XD);
	double sum_ai = sh.m_SA.trace();

	double res = 0;
	// loop on rows of M1
	double cur_dDI = 1;
	for (int l = 0; l < n+1; l++) {
		res += cur_dDI*cos_ss_M1[n-1][3*l]*shAI;
		res += cur_dDI*cos_ss_M1[n-1][3*l+1]*shAD;
		res += cur_dDI*cos_ss_M1[n-1][3*l+2]*(sum_ai);
		cur_dDI*=dDI2;
	}

	// loop on M2
	double aux = wi.dot(sh.m_SA*(XD));
	cur_dDI = dDI*aux;
	for (int l = 0; l < n; l++) {
		res += cur_dDI*cos_ss_M2[n-1][l];
		cur_dDI*=dDI2;
	}
	return shAI*res; // add const
}

double coslobe_int_sigma_t(int n, Vector3d XD, const Shadowing &sh) {
	double aux = sh.shadowing(XD);
	return M_PI*(n*aux+(sh.m_SA.trace())*(n+2))/(4*n+6);
}

double coslobe_int_sigma_ss(int n, Vector3d XD, const Shadowing &sh) {
	Vector3d SA_XD = sh.m_SA * XD;
	double XD_SA_XD = XD.dot(sh.m_SA * XD);
	double res = 15*n*(n-1)*XD_SA_XD*XD_SA_XD;
	double D_SA_SA_D = XD.dot(sh.m_SA * sh.m_SA * XD);
	assert_almost_eq(D_SA_SA_D, SA_XD.dot(SA_XD), 0.00001, DEBUG_INFO);
	res += -10*n*(n-2)*D_SA_SA_D;
	double SASA_trace = (sh.m_SA*sh.m_SA).trace();
	double SA_trace = sh.m_SA.trace();
	res += n*(n+10)*2.0*(XD_SA_XD*SA_trace - D_SA_SA_D);
	res += (3*n*n+7*n+10)*SASA_trace
		+ (n*n+5*n+10)*(SA_trace*SA_trace-SASA_trace); 
	res *= M_PI/(24*n*n+96*n+90);
	return res;
}

double coslobe_phase_ss(int n, Vector3d wi, Vector3d wo, Vector3d XD, const Shadowing &sh) {
	return sh.shadowing(wi)*sh.shadowing(wo)*coslobe_D(n,(wi+wo)/(wi+wo).norm(),XD)/(4*coslobe_sigma_ss(n, wi, XD, sh));
}

double coslobe_D(int n, Vector3d w, Vector3d XD) {
	return std::pow(w.dot(XD),2*n)*(2*n+1)/(4*M_PI);
}

double coslobe_phase_ms(int n, Vector3d wo, Vector3d XD, const Shadowing &sh) {
	return (coslobe_sigma_t(n, wo, XD, sh)-coslobe_sigma_ss(n, wo, XD, sh))/(coslobe_int_sigma_t(n, XD, sh)-coslobe_int_sigma_ss(n, XD, sh));
}

Vector3d coslobe_sample_visible(int n, Vector3d wi, Vector3d XD,
								int &nb_it,
								Sampler &sampler) {
	
	// rejection sampling : sample with squared
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	Vector3d wo;
	nb_it = 1;
	while (true) {
		coslobe_sample_squared(n, wi, XD, wo, sampler);
		double U = uniform_dist(sampler);
		Vector3d wh = wi+wo;
		wh /= wh.norm();
		if (U < wi.dot(wh)/(wi.dot(wh)*wi.dot(wh)+0.25)) {
			return wo;
		}
		nb_it++;
	}
}


double coslobe_pdf_squared(int n, Vector3d wi, Vector3d XD,
						   Vector3d wm) {
	double aux = wm.dot(wi);
	double aux2 = XD.dot(wi);
	double norm =(8.0*aux2*aux2*n+2.0*n+7.0)/(16.0*n+24.0);
	return std::pow(wm.dot(XD),2.0*n)*(aux*aux+0.25)*(2*n+1)/(4*M_PI*norm*4*std::abs(aux));
}

void coslobe_sample_squared(int n, Vector3d wi, Vector3d XD,
							Vector3d &wo,
							Sampler &sampler) {
	
	double theta, phi;
	
	double costi2 = wi.dot(XD);
	double ti = std::acos(std::min(1.0,std::abs(costi2)));
	costi2 *= costi2;

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double U1 = uniform_dist(sampler);
	
	auto pdf_phi = [](double x, double costi2_, double, int n, double U){
		return -U +(     -(3*costi2_-1)*(std::pow(std::cos(x),2.0*n+3)-1.0)*(2.0*n+1.0)
						 + 0.5*(2*costi2_-3)*(std::pow(std::cos(x),2.0*n+1)-1.0)*(2.0*n+3.0))/(8*costi2_*n+2*n+7);
	};
	
	phi = brents_fun(pdf_phi, costi2, 0.0, n, U1,
					 0.0, M_PI, BRENT_TOL, 1000);
	
	assert_almost_eq(pdf_phi(0, costi2, 0.0, n, U1), -U1, BRENT_TOL*2.0, DEBUG_INFO);	
	assert_almost_eq(pdf_phi(M_PI, costi2, 0.0, n, U1), 1-U1, BRENT_TOL*2.0, DEBUG_INFO);	
	assert_almost_eq(pdf_phi(phi, costi2, 0.0, n, U1), 0, BRENT_TOL*2.0, DEBUG_INFO);	
	assert(phi >= 0.0 && phi <= M_PI && "Check phi domain");
	double U2 = uniform_dist(sampler);
	auto pdf_theta = [](double x, double ti_, double phi_, int n, double U){
		double sinti = std::sin(ti_);
		double costi = std::cos(ti_);
		double cost = std::cos(x);
		double sint = std::sin(x);
		double sinphi = std::sin(phi_);
		double cosphi = std::cos(phi_);

		return -U+(2*sinti*sinti*cost*sint*sinphi*sinphi+2*sinti*sinti*sinphi*sinphi*x+8*sinti*sint*sinphi*costi*cosphi+4*costi*costi*cosphi*cosphi*x+x)/(M_PI*(6*costi*costi*cosphi*cosphi-2*costi*costi-2*cosphi*cosphi+3));
	};
	theta = brents_fun(pdf_theta, ti, phi, n, U2,
					   0.0, M_PI, BRENT_TOL, 1000);


	
	assert_almost_eq(pdf_theta(0, ti, phi, n, U2), -U2, BRENT_TOL*2.0, DEBUG_INFO);	
	assert_almost_eq(pdf_theta(M_PI, ti, phi, n, U2), 1-U2, BRENT_TOL*2.0, DEBUG_INFO);	
	assert_almost_eq(pdf_theta(theta,ti,phi,n,U2), 0, BRENT_TOL*2.0, DEBUG_INFO);	

	if (wi.dot(XD) < 0)  {
		XD *= -1.0;
	}
	
	Matrix3d world_to_local, world_to_local_2;
	Vector3d X, Y, X_2, Y_2;
	if (wi.dot(XD) > 0.9999 || wi.dot(XD) < -0.9999) {
		buildOrthonormalBasis(X, Y, XD);
	} else {
		X = wi - wi.dot(XD)*XD;
		assert(X.norm() != 0.0 && "coslobe_sample_squared: bad");
		X /= X.norm();
		Y = -X.cross(XD);
	}

	world_to_local << X(0), X(1), X(2),
		Y(0), Y(1), Y(2),
		XD(0), XD(1), XD(2);
		
	Vector3d wm = world_to_local.transpose()
		*Vector3d(std::cos(theta)*std::sin(phi),
				  std::sin(theta)*std::sin(phi),
				  std::cos(phi));
	
	assert_almost_eq(wm.norm(), 1.0, 0.0001, DEBUG_INFO);	

	wo = -wi + 2*wm*wi.dot(wm);
	wo /= wo.norm(); // else, normalization is not accurate
}

Vector3d coslobe_sample_sigma(int n, Vector3d XD,
							  Sampler &sampler) {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	Vector3d m = coslobe_sample_D(n, XD, sampler);

	// sample a cos lobe.
	double U1 = uniform_dist(sampler);
	double theta = 2*M_PI*U1;
	double U2 = uniform_dist(sampler);
	double phi = std::asin(std::sqrt(U2));
	
	Vector3d X,Y;
	buildOrthonormalBasis(X, Y, m); // not a problem if X and Y are not
	// consistant, because theta in uniformly sampled
	X /= X.norm();
	Y /= Y.norm();
	Matrix3d world_to_local;
	world_to_local << X(0), X(1), X(2),
		Y(0), Y(1), Y(2),
		m(0), m(1), m(2);
	Vector3d res = world_to_local.transpose()*
		Vector3d(std::cos(theta)*std::sin(phi),
				 std::sin(theta)*std::sin(phi),
				 std::cos(phi));
	return res/res.norm();

}


double coslobe_pdf_sigma(int n, Vector3d w, Vector3d XD) {
	return coslobe_sigma(n, w, XD)/M_PI;
}

Vector3d coslobe_sample_D(int n, Vector3d XD,
						  Sampler &sampler) {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double U1 = uniform_dist(sampler);
	double theta = 2*M_PI*U1;
	double phi;
	double U2 = uniform_dist(sampler);
	if (U2 < 0.5) {
		phi = std::acos(pow(1-2.0*U2,1.0/(2.0*n+1.0)));
	} else {
		phi = M_PI-std::acos(pow(2.0*U2-1.0,1.0/(2.0*n+1.0)));
	}

	// back in world space
	Vector3d X,Y;
	buildOrthonormalBasis(X, Y, XD); // not a problem if X and Y are not
	// consistant, because theta in uniformly sampled on 0..2Pi
	X /= X.norm();
	Y /= Y.norm();
	Matrix3d world_to_local;
	world_to_local << X(0), X(1), X(2),
		Y(0), Y(1), Y(2),
		XD(0), XD(1), XD(2);
	Vector3d res = world_to_local.transpose()*
		Vector3d(std::cos(theta)*std::sin(phi),
				 std::sin(theta)*std::sin(phi),
				 std::cos(phi));
	return res/res.norm();
}

Vector3d coslobe_sample_ms(int n, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler) {

	assert(sh.hasEigenv()
		   && "This implementation of sample_ms needs eigenvalues of SA");

	if (!(sh.m_SA.trace() < 3.0)) {
		std::cout << "sh.m_SA.trace() " << sh.m_SA.trace() << std::endl;
	}
	
	assert(sh.m_SA.trace() < 3.0);
	Vector3d eg_val = sh.getEigenvalues();
	double Amin = std::min(std::min(eg_val(0),eg_val(1)),eg_val(2));
	
	std::uniform_real_distribution<double> uniform_dist(0, 1);

	int nb_it = 1;
	while (true) {
	
		double U = uniform_dist(sampler);
		Vector3d w = coslobe_sample_sigma(n, XD, sampler);
		
		double P = (coslobe_sigma_t(n,w,XD,sh) - coslobe_sigma_ss(n,w,XD,sh))
			/(sh.max()*(1.0-Amin)*coslobe_sigma(n,w,XD));

		if (U < P) {
			return w;
		}
		nb_it++;
	}
}

Vector3d coslobe_sample_ss(int n, Vector3d wi, Vector3d XD,
						   const Shadowing &sh,
						   Sampler &sampler) {

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	Vector3d wo;
	int nb_it = 1;
	while (true) {
		coslobe_sample_squared(n, wi, XD, wo, sampler);
		double U = uniform_dist(sampler);
		Vector3d wh = wi+wo;
		wh /= wh.norm();
		double dot_wi_wh = wi.dot(wh);
		double A_max = sh.max();
		double P = sh.shadowing(wo)*dot_wi_wh/(A_max*(dot_wi_wh*dot_wi_wh+0.25));
		if (U < P) {
			return wo;
		}
		nb_it++;
	}
}
