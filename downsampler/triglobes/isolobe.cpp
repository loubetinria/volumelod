#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <Eigen/Core>
#include <Eigen/Eigenvalues> 

#include "../utils/utils.h"
#include "isolobe.h"
#include "coslobe.h"

double iso_int_sigma_t(const Shadowing &sh) {
	return M_PI*(sh.m_SA.trace())/3.0;
}
	
double iso_int_sigma_ss(const Shadowing &sh) {
	double aux = sh.m_SA.trace();
	return M_PI*aux*aux/9.0;
}


double iso_sigma_t(Vector3d wi, const Shadowing &sh) { // useful ?
	return 0.25 * sh.shadowing(wi);
}
	
double iso_sigma_ss(Vector3d wi, const Shadowing &sh) { // useful ?
	return sh.shadowing(wi)*sh.m_SA.trace()/12.0;
}

double iso_phase_ss(Vector3d wo, const Shadowing &sh) {
	return sh.shadowing(wo)/(sh.m_SA.trace()*4.0*M_PI/3.0);
}

double iso_phase_ms(Vector3d wo, const Shadowing &sh) {
	return sh.shadowing(wo)/(sh.m_SA.trace()*4.0*M_PI/3.0);
}

Vector3d iso_sample_ss(const Shadowing &sh, Sampler &sampler) {

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	double U1 = uniform_dist(sampler)*sh.m_SA.trace();
	assert(sh.hasEigenv()
		   && "This implementation of iso_sample_ss needs eigenvalues");
	Vector3d eigenvalues = sh.getEigenvalues();
	double a1 = eigenvalues(0);
	double a2 = eigenvalues(1);
	//double a3 = eigenvalues(2);
	Vector3d XA1 = sh.getXA1();
	Vector3d XA2 = sh.getXA2();
	Vector3d XA3 = sh.getXA3();
	
	Vector3d axis; 
	if (U1 < a1) {
		axis = XA1;
	} else if (U1 < a1 + a2) {
		axis = XA2;
	} else {
		axis = XA3;
	}
		
	return coslobe_sample_D(1, axis, sampler);
}
	
Vector3d iso_sample_ms(const Shadowing &sh, Sampler &sampler) {
	return iso_sample_ss(sh, sampler);
}
