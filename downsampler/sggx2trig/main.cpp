#include "../utils/utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <string>     // std::string, std::to_string
#include "../sggx/sggx.h"
#include "../triglobes/coslobe.h"
#include "../triglobes/sinlobe.h"
#include <iomanip>
#include <boost/thread.hpp>

#define N_THETA 60
#define N_PHI 60

#define N_SGGX_RES 20

double evalError(double *data1, double *data2) {
	double error = 0.0;
	for (int i = 0; i < N_THETA; i++) {
		for (int j = 0; j < N_PHI; j++) {
			double phi = double(j)/N_PHI*M_PI;
			double diff = data1[i*N_PHI+j] - data2[i*N_PHI+j];
			error += diff*diff*std::sin(phi);
		}
	}
	return error;
}

double integrateSphere(double *data) {
	double sum = 0;
	double norm = 0;
	for (int i = 0; i < N_THETA; i++) {
		for (int j = 0; j < N_PHI; j++) {
			double phi = double(j)/N_PHI*M_PI;
			sum += data[i*N_PHI+j] * std::sin(phi);
			norm += std::sin(phi);
		}
	}
	return sum / norm * 4.0 * M_PI;
}

void convert(double s1, double s2, double s3,
			 double *norm,
			 int *best_n_cos, int *best_n_sin,
			 double *best_w_cos, double *best_w_sin) {

	//std::cout << "Converting SGGX(" << s1 << " " << s2 << " " << s3 << ")" << std::endl;

	
	assert(s1 >= s2);
	assert(s2 >= s3);
	assert(s3 > 0);
	assert(s1 <= 1);

	double D_SGGX[N_THETA*N_PHI];
	double D_cos[N_THETA*N_PHI];

	SGGX sggx(Eigen::Vector3d(s1, s2, s3), Eigen::Vector3d(0.0,0.0,0.0));

	std::random_device r;
	std::default_random_engine sampler(r());
	//double _norm = sggx.evalNorm(sampler);
	double _norm = sggx.evalIntSigma(sampler);
	//std::cout << "Norm: " << norm << std::endl; 

	
	/*
	// fill D_SGGX
	for (int i = 0; i < N_THETA; i++) {
		for (int j = 0; j < N_PHI; j++) {
			double theta = double(i)/N_THETA*M_PI;
			double phi = double(j)/N_PHI*M_PI;
			Eigen::Vector3d w(std::cos(theta)*std::sin(phi),
							  std::sin(theta)*std::sin(phi),
							  std::cos(phi));
			D_SGGX[i*N_PHI+j] = sggx.D(w)/_norm;
		}
	}
	
	//std::cout << "Integration check: " << integrateSphere(D_SGGX) << std::endl; 

	double _best_w_cos = -1;
	double _best_w_sin = -1;
	int _best_n_cos = -1;
	int _best_n_sin = -1;
	
	CosLobe cosLobe;
	SinLobe sinLobe;
	Eigen::Vector3d XD_cos(1.0,0.0,0.0);
	Eigen::Vector3d XD_sin(0.0,0.0,1.0);

	double error_cur = std::pow(42,42);
	
	for (int n_cos = 1; n_cos <= 20; n_cos++) {
		for (int n_sin = 1; n_sin <= 20; n_sin++) {
			for (double w_cos = 0; w_cos <= 1.0; w_cos += 0.1) {
				for (double w_sin = 0; w_sin <= (1-w_cos); w_sin += 0.1) {
					
					// compute D_cos
					for (int i = 0; i < N_THETA; i++) {
						for (int j = 0; j < N_PHI; j++) {

							double theta = double(i)/N_THETA*M_PI;
							double phi = double(j)/N_PHI*M_PI;
							Eigen::Vector3d w(std::cos(theta)*std::sin(phi),
											  std::sin(theta)*std::sin(phi),
											  std::cos(phi));
								
							D_cos[i*N_PHI+j] =  w_cos*cosLobe.D(n_cos, w, XD_cos);
							D_cos[i*N_PHI+j] += w_sin*sinLobe.D(n_sin, w, XD_sin);
							D_cos[i*N_PHI+j] += (1-w_cos-w_sin)/(4.0*M_PI);
						}
					}

					
					//std::cout << "Integration check: " << integrateSphere(D_cos) << std::endl; 
					//assert_almost_eq(integrateSphere(D_cos), 1.0, 0.001);
					
					// compute diff
					double error = evalError(D_cos, D_SGGX);
					if (error < error_cur) {
						error_cur = error;
						_best_w_cos = w_cos;
						_best_w_sin = w_sin;
						_best_n_cos = n_cos;
						_best_n_sin = n_sin;
					}
				}
			}
		}


		//std::cout << "Converted! SGGX(" << s1 << " " << s2 << " " << s3 << ")" << std::endl;
	}

	*best_w_cos = _best_w_cos;
	*best_w_sin = _best_w_sin;
	*best_n_cos = _best_n_cos;
	*best_n_sin = _best_n_sin;
	*/
	best_w_cos = 0;
	best_w_sin = 0;
	best_n_cos = 0;
	best_n_sin = 0;
	*norm = _norm;

	/*
	std::cout << "Converting SGGX(" << s1 << " " << s2 << " " << s3 << "): "
			  << best_w_cos << " "
			  << best_w_sin << " "
			  << best_n_cos << " "
			  << best_n_sin << " "
			  << "(norm=" << norm << ")\n";
		return 0;
	// print comp
	// compute D_cos
	int n_theta = 20;
	int n_phi = 10;
	std::cout << "\n sggx: " << std::endl; 
	for (int i = 0; i < n_theta; i++) {
		for (int j = 0; j < n_phi; j++) {
			double theta = double(i)/n_theta*M_PI*2.0;
			double phi = double(j)/n_phi*M_PI;
			Eigen::Vector3d w(std::cos(theta)*std::sin(phi),
							  std::sin(theta)*std::sin(phi),
							  std::cos(phi));
			double sggx_val = sggx.D(w)/norm;
			std::cout << std::setw(10) <<  std::setprecision(3) << sggx_val << " ";
		}
		std::cout << "\n";
	}
	std::cout << "\n sggx: " << std::endl; 
	for (int i = 0; i < n_theta; i++) {
		for (int j = 0; j < n_phi; j++) {
			double theta = double(i)/n_theta*M_PI*2.0;
			double phi = double(j)/n_phi*M_PI;
			Eigen::Vector3d w(std::cos(theta)*std::sin(phi),
							  std::sin(theta)*std::sin(phi),
							  std::cos(phi));
			double trig_val = best_w_cos*cosLobe.D(best_n_cos, w, XD_cos);
			trig_val += best_w_sin*sinLobe.D(best_n_sin, w, XD_sin);
			trig_val += (1-best_w_cos-best_w_sin)/(4.0*M_PI);
			std::cout << std::setw(10) <<  std::setprecision(3) << trig_val << " ";

		}
		std::cout << "\n";
	}
	*/
}		
	
void print_int(int *data) {
	for (int i = 0; i < N_SGGX_RES; i++) {
		for (int j = 0; j < N_SGGX_RES; j++) {
			std::cout << std::setw(10) << data[j+i*N_SGGX_RES] << " ";
		}
		std::cout << std::endl;
	}
}

void print_double(double *data) {
	for (int i = 0; i < N_SGGX_RES; i++) {
		for (int j = 0; j < N_SGGX_RES; j++) {
			std::cout << std::setw(10) << data[j+i*N_SGGX_RES] << " ";
		}
		std::cout << std::endl;
	}
}

int main(int argc, char *argv[])
{

	double norms[N_SGGX_RES*N_SGGX_RES];
	int best_n_cos[N_SGGX_RES*N_SGGX_RES];
	int best_n_sin[N_SGGX_RES*N_SGGX_RES];
	double best_w_cos[N_SGGX_RES*N_SGGX_RES];
	double best_w_sin[N_SGGX_RES*N_SGGX_RES];

	for (int i = 0; i < N_SGGX_RES*N_SGGX_RES; i++) {
		norms[i] = -1;
		best_n_cos[i] = -1;
		best_n_sin[i] = -1;
		best_w_cos[i] = -1;
		best_w_sin[i] = -1;
	}
	
	// values : 1/N_SGGX_RES .. 1
	
	int finished_jobs = 0;
	int nb_jobs = N_SGGX_RES*(N_SGGX_RES+1)/2;
	std::cout << "nb jobs " << nb_jobs << std::endl;

	int cur_job = 1;
	int nthreads = int(boost::thread::hardware_concurrency());
	boost::thread_group group;

	int current_S2_id = 0;
	int current_S3_id = 0;
	
	while (finished_jobs != nb_jobs) {

		int job_set = std::min(nb_jobs-finished_jobs, nthreads);
		std::cout << "start job " << cur_job << " to " << cur_job + job_set -1 << std::endl;
		for(int j = 0; j < job_set; j++) {
			// job number finished_jobs +j
			double s1 = 1.0;
			double s2 = (current_S2_id+1.0)/N_SGGX_RES;
			double s3 = (current_S3_id+1.0)/N_SGGX_RES;
			int id = current_S3_id+current_S2_id*N_SGGX_RES;
			group.create_thread(boost::bind(&convert,
											s1, s2, s3,
											&norms[id],
											&best_n_cos[id], &best_n_sin[id],
											&best_w_cos[id], &best_w_sin[id]));
			
			current_S3_id++;
			cur_job++;
			if (current_S3_id > current_S2_id) {
				current_S3_id = 0;
				current_S2_id++;
			}
		}
		group.join_all();
		finished_jobs += job_set;
	}

	std::cout << "norms:" << std::endl;
	print_double(norms);

	std::cout << "best_n_cos:" << std::endl;
	print_int(best_n_cos);

	std::cout << "best_n_sin:" << std::endl;
	print_int(best_n_sin);

	std::cout << "best_w_cos:" << std::endl;
	print_double(best_w_cos);

	std::cout << "best_w_sin:" << std::endl;
	print_double(best_w_sin);

	
	return 0;
}
