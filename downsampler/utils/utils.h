#ifndef UTILS_H
#define UTILS_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include <functional>
#include <random>

#define DEBUG_INFO std::to_string(__LINE__),__FILE__,__func__

typedef Eigen::Vector3d Vector3d;
typedef Eigen::Vector3i Vector3i;
typedef Eigen::Array3i Array3i;
typedef Eigen::Array3d Array3d;
typedef Eigen::Matrix3d Matrix3d;

typedef std::default_random_engine Sampler;

std::string to_string(int i);
std::string to_string(double f);
std::string to_string(Vector3i v);
std::string to_string(Vector3d v);
std::string to_string(Array3i v);
std::string to_string(Array3d v);
std::string to_string(Matrix3d m);

inline bool is_equal(Array3i A, Array3i B) {
	return (A==B).count() == 3;
}

inline bool is_equal(Array3d A, Array3d B) {
	return (A==B).count() == 3;
}

void assert_almost_eq(double f1, double f2, double eps,
					  std::string line = "",
					  std::string file = "",
					  std::string func = "");

Vector3d sampleSphere(Sampler &sampler);
Vector3d sampleSphere(double &theta, double &phi, Sampler &sampler);

double gamma_int(int n);
double gamma_half(int n);

void buildOrthonormalBasis(Vector3d& omega_1,
						   Vector3d& omega_2,
						   const Vector3d& omega_3);

void test_chi_square(std::function<double(Vector3d)> pdf_f,
					 std::function<Vector3d(Sampler &)> sample_f);

// Make a matrix from column vectors
Matrix3d makeMatrix(Vector3d v1, Vector3d v2,Vector3d v3);

// Make a matrix from column vectors
Matrix3d makeMatrix(double a1, double a2, double a3);

void sort(Vector3d &eg_val, Matrix3d &eg_vec);

void swap(double &a, double &b);

int getSGGX2TrigId(double S2, double S3, int sggx2trig_res);

inline double safe_sqrt(double value) {
	return std::sqrt(std::max(0.0, value));
}

inline double dot(Array3d a, Array3d b) {
	return a(0)*b(0)+a(1)*b(1)+a(2)*b(2);
}

#endif // UTILS_H
