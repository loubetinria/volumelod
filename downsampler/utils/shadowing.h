#ifndef SHADOWING_H
#define SHADOWING_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include "../utils/utils.h"
#include <Eigen/Eigenvalues> // solver
#include <iostream>

class Shadowing {

  public:
	// by default, has only SA */
	// if eigenvectors and values are computed: has_eigenv = true
	
	Shadowing();
	
	Shadowing(double a1, double a2, double a3,
			  Vector3d XA1, Vector3d XA2, Vector3d XA3);

	Shadowing(const Vector3d &diag, const Vector3d &tri);

	Shadowing(const Matrix3d &SA);

	
	inline double shadowing(const Vector3d &w) const { return w.dot(m_SA*w); }

	std::string toString() const;
  
	void toSigmaR(Vector3d &sigma, Vector3d &r) const;
	
	inline bool hasEigenv() const {return m_has_eigenv;};
	inline Vector3d getEigenvalues() const {
		assert(m_has_eigenv);
		return Vector3d(m_a1, m_a2, m_a3);
	};
	inline Vector3d getXA1() const {
		assert(m_has_eigenv);
		return m_XA1;
	};
	inline Vector3d getXA2() const {
		assert(m_has_eigenv);
		return m_XA2;
	};
	inline Vector3d getXA3() const {
		assert(m_has_eigenv);
		return m_XA3;
	};
	inline double max() const {
		assert(m_has_eigenv);
		return std::max(m_a1, std::max(m_a2, m_a3));
	};
	
  Matrix3d m_SA;

  protected:
	bool m_has_eigenv;
	double m_a1, m_a2, m_a3;
	Vector3d m_XA1, m_XA2, m_XA3;
  
};
#endif // SHADOWING_H
