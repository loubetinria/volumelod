#ifndef BREND_H
#define BREND_H

#include <iostream>
#include <cmath>
#include <algorithm>
#include <functional>
#include <ctime>

double brents_fun(std::function<double(double, double, double, int, double)> f, double c1, double c2, int n, double U, double lower_bound, double upper_bound, double TOL, double MAX_ITER);

double brents_fun(std::function<double(double)> f, double lower_bound, double upper_bound, double TOL, double MAX_ITER);


#endif // BREND_H
