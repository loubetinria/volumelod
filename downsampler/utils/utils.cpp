#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <Eigen/Core>
#include <iomanip>

#include "utils.h"

std::string to_string(int i) {
	std::stringstream ss;
	ss << i;
	return ss.str();
}

std::string to_string(double f) {
	std::stringstream ss;
	ss << f;
	return ss.str();
}

std::string to_string(Eigen::Vector3i v) {
	std::string s = "[" + to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2)) + "]";
	return s;
}

std::string to_string(Matrix3d m) {
	std::string s = "[" + to_string(m(0,0)) + ", " + to_string(m(0,1)) + ", " + to_string(m(0,2)) + ";"
		+ " " + to_string(m(1,0)) + ", " + to_string(m(1,1)) + ", " + to_string(m(1,2)) + ";"
		+ " " + to_string(m(2,0)) + ", " + to_string(m(2,1)) + ", " + to_string(m(2,2)) + "]";
	return s;
}

std::string to_string(Eigen::Vector3d v) {
	std::string s = "[" + to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2)) + "]";
	return s;
}

std::string to_string(Eigen::Array3i v) {
	std::string s = "[" + to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2)) + "]";
	return s;
}

std::string to_string(Eigen::Array3d v) {
	std::string s = "[" + to_string(v(0)) + "," + to_string(v(1)) + "," + to_string(v(2)) + "]";
	return s;
}

void assert_almost_eq(double f1, double f2, double eps,
					  std::string line,
					  std::string file,
					  std::string func) {
	if (std::abs(f1-f2) > eps) {
		std::cout << "assert_almost_eq: " << f1 << " != " << f2
				  << " (eps=" << eps << ")" << std::endl;
		std::cout << "file: " << file << std::endl;
		std::cout << "line: " << line << std::endl;
		std::cout << "func: " << func << std::endl;
		assert(std::abs(f1-f2) <= eps);
	}
}

void assert_almost_eq(int f1, int f2, int eps) {
	if (std::abs(f1-f2) > eps) {
		std::cout << "assert_almost_eq: " << f1 << " != " << f2
				  << " (eps=" << eps << ")" << std::endl;
		assert(std::abs(f1-f2) <= eps);
	}
}

Eigen::Vector3d sampleSphere(std::default_random_engine &sampler) {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double x = uniform_dist(sampler);
	double y = uniform_dist(sampler);
	double z = 1.0f - 2.0f * y;
	double r = std::sqrt(1.0f - z*z);
	double sinTheta = std::sin(2.0f * M_PI * x);
	double cosTheta  = std::cos(2.0f * M_PI * x);
	return Eigen::Vector3d(r * cosTheta, r * sinTheta, z);
}

Eigen::Vector3d sampleSphere(double &theta, double &phi,
							 std::default_random_engine &sampler) {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	double x = uniform_dist(sampler);
	double y = uniform_dist(sampler);
	double z = 1.0f - 2.0f * y;
	double r = std::sqrt(1.0f - z*z);
	double sinTheta = std::sin(2.0f * M_PI * x);
	double cosTheta  = std::cos(2.0f * M_PI * x);
	theta = 2.0f * M_PI * x;
	phi = std::acos(z);
	return Eigen::Vector3d(r * cosTheta, r * sinTheta, z);
}

double gamma_int(int n) {
	return std::tgamma(n);
}

double gamma_half(int n) {
	return std::tgamma(n+0.5);
}

void buildOrthonormalBasis(Eigen::Vector3d& omega_1,
						   Eigen::Vector3d& omega_2,
						   const Eigen::Vector3d& omega_3) {
    if(omega_3(2) < -0.9999999) {
        omega_1 = Eigen::Vector3d ( 0.0 , -1.0 , 0.0 );
        omega_2 = Eigen::Vector3d ( -1.0 , 0.0 , 0.0 );
    } else {
        const double a = 1.0 /(1.0 + omega_3(2));
        const double b = -omega_3(0)*omega_3(1)*a ;
        omega_1 = Eigen::Vector3d (1.0 - omega_3(0)*omega_3(0)*a , b , -omega_3(0) );
        omega_2 = Eigen::Vector3d (b , 1.0 - omega_3(1)*omega_3(1)*a , -omega_3(1) );
    }
}

void test_chi_square(std::function<double(Eigen::Vector3d)> pdf_f,
					 std::function<Eigen::Vector3d(std::default_random_engine &)> sample_f) {

	// array
	// first = theta, between i*2*Pi/N_theta and (i+1)*2*Pi/N_theta
	// second = phi, between i*Pi/N_phi and (i+1)*Pi/N_phi
	int N_theta = 20;
	int N_phi = 10;
	double expected_density[N_theta][N_phi];
	int expected_density_samples[N_theta][N_phi];
	double sampled_density[N_theta][N_phi];

	// init
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			expected_density[i][j] = 0;
			expected_density_samples[i][j] = 0;
			sampled_density[i][j] = 0;
		}
	}
	
	std::random_device r;
	std::default_random_engine sampler(r());

	// integration of the expected density
	int nb_samples_integral = 1000000;
	for (int i = 0; i < nb_samples_integral; i++) {
		double theta, phi;
		Eigen::Vector3d w = sampleSphere(theta, phi, sampler);
		int id_theta = std::floor(theta/(2*M_PI)*N_theta);
		int id_phi = std::floor(phi/(M_PI)*N_phi);
		if (!(id_theta >= 0 && id_theta < N_theta) || !(id_phi >= 0 && id_phi < N_phi)) {
			std::cout << "id_theta " << id_theta << std::endl;
			std::cout << "id_phi " << id_phi << std::endl;
			std::cout << "theta " << theta << std::endl;
			std::cout << "phi " << phi << std::endl;
		} 
		assert(id_theta >= 0 && id_theta < N_theta);
		assert(id_phi >= 0 && id_phi < N_phi);
		expected_density_samples[id_theta][id_phi] += 1;
		expected_density[id_theta][id_phi] += pdf_f(w);
	}

	// sampling
	int nb_samples = 100000;
	for (int i = 0; i < nb_samples; i++) {
		Eigen::Vector3d	wo = sample_f(sampler);
		double phi = std::acos(wo(2));
		double theta = std::atan2(wo(1),wo(0));
		if (theta < 0) {
			theta += 2*M_PI;
		}
		assert_almost_eq(wo(0), std::cos(theta)*std::sin(phi), 0.0001, DEBUG_INFO);	
		assert_almost_eq(wo(1), std::sin(theta)*std::sin(phi), 0.0001, DEBUG_INFO);	
		assert_almost_eq(wo(2), std::cos(phi), 0.0001, DEBUG_INFO);
		int id_theta = std::floor(theta/(2*M_PI)*N_theta);
		int id_phi = std::floor(phi/(M_PI)*N_phi);
		if (!(id_theta >= 0 && id_theta < N_theta) || !(id_phi >= 0 && id_phi < N_phi)) {
			std::cout << "id_theta " << id_theta << std::endl;
			std::cout << "id_phi " << id_phi << std::endl;
			std::cout << "theta " << theta << std::endl;
			std::cout << "phi " << phi << std::endl;
		} 
		assert(id_theta >= 0 && id_theta < N_theta);
		assert(id_phi >= 0 && id_phi < N_phi);
		sampled_density[id_theta][id_phi] += 1;
	}

	// normalize
	double sum_expected_density = 0;
	double sum_sampled_density = 0;
	
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			// expected density = mean value of the density in the area
			// proba : x area
			if (expected_density_samples[i][j] > 0) {
				expected_density[i][j] /= expected_density_samples[i][j];
				double area = (2*M_PI/N_theta)*(std::cos(j*M_PI/N_phi)-std::cos((j+1)*M_PI/N_phi));
				if (expected_density[i][j] < 0 || area < 0) {
					std::cout << "area " << area << std::endl;
					std::cout << "expected_density[i][j] " << expected_density[i][j] << std::endl;

				}
				expected_density[i][j] *= area;
			}
			// estimation of the pdf = samples/nb_samples
			sampled_density[i][j] /= nb_samples;
			sum_expected_density += expected_density[i][j];
			sum_sampled_density += sampled_density[i][j];
		}
	}
#if 0
	std::cout << "sum_expected_density " << sum_expected_density << std::endl;
	std::cout << "sum_sampled_density " << sum_sampled_density << std::endl;

	//compare
	std::cout << "Expected density\n" << std::endl;
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			std::cout << std::setw(10) <<  std::setprecision(3) << expected_density[i][j] << " ";
		}
		std::cout << "\n";
	}
	std::cout << "measured density from sampling\n" << std::endl;
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			std::cout << std::setw(10) <<  std::setprecision(3) << sampled_density[i][j] << " ";
		}
		std::cout << "\n";
		}
#endif
	
	// if eff < 5, density to 0 and add to neighbour	
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			if (sampled_density[i][j] * nb_samples < 5) {
				// copying to neighbour
				if (j+1 < N_phi) {
					sampled_density[i][j+1] += sampled_density[i][j];
					sampled_density[i][j] = 0;
					expected_density[i][j+1] += expected_density[i][j];
					expected_density[i][j] = 0;
				} else {
					if (i+1 < N_theta) {
						sampled_density[i+1][0] += sampled_density[i][j];
						sampled_density[i][j] = 0;
						expected_density[i+1][0] += expected_density[i][j];
						expected_density[i][j] = 0;
					} else {
						// loop for finding the first convenient
						// case, meaning with expected_density > 0
						int non_void_i = 0, non_void_j = 0;
						bool found = false;
						for (int ii = 0; ii < N_theta && !found; ii++) {
							for (int jj = 0; jj < N_phi && !found; jj++) {
								if (expected_density[ii][jj] > 0) {
									found = true;
									non_void_i = ii;
									non_void_j = jj;
								}
							}
						}
						sampled_density[non_void_i][non_void_j] += sampled_density[i][j];
						sampled_density[i][j] = 0;
						expected_density[non_void_i][non_void_j] += expected_density[i][j];
						expected_density[i][j] = 0;
					}
				}
			}
		}
	}
	
	// Khi2 test
	double T = 0.0;
	for (int i = 0; i < N_theta; i++) {
		for (int j = 0; j < N_phi; j++) {
			if ((sampled_density[i][j] * nb_samples < 5)
				&& (expected_density[i][j] != 0)) {
				std::cout << "Warning, effectif = "
						  << sampled_density[i][j] * nb_samples << std::endl;
			}
			if (sampled_density[i][j] > 0) {
				T += nb_samples*(expected_density[i][j]-sampled_density[i][j])*(expected_density[i][j]-sampled_density[i][j])/expected_density[i][j];
			}
		}
	}

	double chi2inv_inv_095_199 = 232.9118;
	if (T >= chi2inv_inv_095_199) {
		std::cout << "REJECT CHI-SQUARE TEST: " << std::setw(4) <<  std::setprecision(3) << T << " " << chi2inv_inv_095_199 << std::endl;
	} else {
		std::cout << "accept chi-square test: " << std::setw(4) <<  std::setprecision(3) << T << " " << chi2inv_inv_095_199 << std::endl;
	}
}

// Make a matrix from column vectors
Eigen::Matrix3d makeMatrix(Eigen::Vector3d v1, Eigen::Vector3d v2,Eigen::Vector3d v3) {
	Eigen::Matrix3d res;
	res << v1(0), v2(0), v3(0),
		v1(1), v2(1), v3(1),
		v1(2), v2(2), v3(2);
	return res;
}

// Make a matrix from column vectors
Eigen::Matrix3d makeMatrix(double a1, double a2, double a3) {
	Eigen::Matrix3d res;
	res << a1, 0, 0, 0, a2, 0, 0, 0, a3;
	return res;
}

void swap(double &a, double &b) {
	double _a = a;
	a = b;
	b = _a;
}


// sort eigen values by increasing order, and swap corresponding column eigen
// vector in matrix eg_vec.
void sort(Vector3d &eg_val, Matrix3d &eg_vec) {
	if (eg_val(0) > eg_val(1)) {
		swap(eg_val(0),eg_val(1));
		eg_vec.col(0).swap(eg_vec.col(1));
	}
	if (eg_val(0) > eg_val(2)) {
		swap(eg_val(0),eg_val(2));
		eg_vec.col(0).swap(eg_vec.col(2));
	}
	if (eg_val(1) > eg_val(2)) {
		swap(eg_val(1),eg_val(2));
		eg_vec.col(1).swap(eg_vec.col(2));
	}
	assert(eg_val(0) <= eg_val(1));
	assert(eg_val(1) <= eg_val(2));
}

int getSGGX2TrigId(double s2, double s3, int sggx2trig_res) {
	// mapping from (0,1) to 0..CONVERT_SGGX_RES, and avoid 0
	int id_S2 = std::max(1.0,std::round(s2*sggx2trig_res));
	int id_S3 = std::max(1.0,std::round(s3*sggx2trig_res));	
	// find id in convert arrays: 
	return (id_S2-1)*id_S2/2+id_S3-1;
}
