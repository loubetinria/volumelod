#ifndef BOX3_H
#define BOX3_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "ray.h"
#include "utils.h"

#include <Eigen/Core>


/*
 * From https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
 */
class Box3 {
public:
	Box3(Array3d vmin, Array3d vmax) {
		bounds[0] = vmin;
		bounds[1] = vmax;
	}
	Array3d bounds[2];
	bool intersect(const Ray &r, double &tmin, double &tmax) const 
	{ 
		double tymin, tymax, tzmin, tzmax; 
		tmin = (bounds[r.sign[0]](0) - r.o(0)) * r.invd(0); 
		tmax = (bounds[1-r.sign[0]](0) - r.o(0)) * r.invd(0); 
		tymin = (bounds[r.sign[1]](1) - r.o(1)) * r.invd(1); 
		tymax = (bounds[1-r.sign[1]](1) - r.o(1)) * r.invd(1); 
 
		if ((tmin > tymax) || (tymin > tmax)) 
			return false; 
		if (tymin > tmin) 
			tmin = tymin; 
		if (tymax < tmax) 
			tmax = tymax; 
 
		tzmin = (bounds[r.sign[2]](2) - r.o(2)) * r.invd(2); 
		tzmax = (bounds[1-r.sign[2]](2) - r.o(2)) * r.invd(2); 
 
		if ((tmin > tzmax) || (tzmin > tmax)) 
			return false; 
		if (tzmin > tmin) 
			tmin = tzmin; 
		if (tzmax < tmax) 
			tmax = tzmax; 

		if (tmin > tmax) {
			std::cout << "Box3::intersect - Warning, tmin > tmax, TODO more robust" << std::endl;
			return false;
		}
		
		return true; 
	} 

	// sample axis aligned ray
	Ray sampleRay(Sampler &sampler, bool randomizeDir) const {
		
		std::uniform_real_distribution<double> uniform_dist(0, 1);
		double a, b;
		do {
			a = uniform_dist(sampler);
		} while( a == 0 || a == 1 );

		do {
			b = uniform_dist(sampler);
		} while( b == 0 || b == 1 );
		
		double face = uniform_dist(sampler)*6.0;

		Vector3d o;
		Vector3d d;

		switch(int(std::floor(face))) {
			case 0 :
				o = Vector3d(bounds[0](0),
							 a*bounds[0](1) + (1.0-a)*bounds[1](1),
							 b*bounds[0](2) + (1.0-b)*bounds[1](2));
				d = Vector3d(1,0,0);
				break;
			case 1 :
				o = Vector3d(bounds[1](0),
							 a*bounds[0](1) + (1.0-a)*bounds[1](1),
							 b*bounds[0](2) + (1.0-b)*bounds[1](2));
				d = Vector3d(-1,0,0);
				break;
			case 2 :
				o = Vector3d(a*bounds[0](0) + (1.0-a)*bounds[1](0),
							 bounds[0](1),
							 b*bounds[0](2) + (1.0-b)*bounds[1](2));
				d = Vector3d(0,1,0);
				break;
			case 3 :
				o = Vector3d(a*bounds[0](0) + (1.0-a)*bounds[1](0),
							 bounds[1](1),
							 b*bounds[0](2) + (1.0-b)*bounds[1](2));
				d = Vector3d(0,-1,0);
				break;
			case 4 :
				o = Vector3d(a*bounds[0](0) + (1.0-a)*bounds[1](0),
							 b*bounds[0](1) + (1.0-b)*bounds[1](1),
							 bounds[0](2));
				d = Vector3d(0,0,1);
				break;
			default :
				o = Vector3d(a*bounds[0](0) + (1.0-a)*bounds[1](0),
							 b*bounds[0](1) + (1.0-b)*bounds[1](1),
							 bounds[1](2));
				d = Vector3d(0,0,-1);
		}

		// Random direction
		if (randomizeDir) {
			Vector3d dir;
			do {
				dir = sampleSphere(sampler);
			} while( d.dot(dir) == 0 );
		
			if (d.dot(dir) < 0) {
				d = -dir;
			} else {
				d = dir;
			}

			Ray r(o, d, (bounds[1](2)-bounds[0](2))*std::sqrt(3));
			double tmin, tmax;
			bool success = intersect(r, tmin, tmax);
			assert(success);
			r.tmax = tmax;
			return r;
			
		} else {
			Ray r(o, d, (bounds[1](0)-bounds[0](0)));
			return r;
		}
	}
	
}; 



#endif // BOX3_H
