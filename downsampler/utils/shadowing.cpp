#include "shadowing.h"

Shadowing::Shadowing() {
	m_a1 = 1;
	m_a2 = 1;
	m_a3 = 1;
	m_XA1 = Vector3d(1,0,0);
	m_XA2 = Vector3d(0,1,0);
	m_XA3 = Vector3d(0,0,1);
	m_SA = makeMatrix(m_XA1, m_XA2, m_XA3);
	m_has_eigenv = true;
}

Shadowing::Shadowing(double a1, double a2, double a3,
					 Vector3d XA1, Vector3d XA2, Vector3d XA3)
	: m_a1(a1), m_a2(a2), m_a3(a3), m_XA1(XA1), m_XA2(XA2), m_XA3(XA3) {
	
	assert(a1 > 0 && a1 <= 1);
	assert(a2 > 0 && a2 <= 1);
	assert(a3 > 0 && a3 <= 1);
	assert_almost_eq(XA1.norm(), 1.0, 0.0001);
	assert_almost_eq(XA2.norm(), 1.0, 0.0001);
	assert_almost_eq(XA3.norm(), 1.0, 0.0001);
	assert_almost_eq(XA1.dot(XA2), 0.0, 0.0001);
	assert_almost_eq(XA2.dot(XA3), 0.0, 0.0001);
	assert_almost_eq(XA1.dot(XA3), 0.0, 0.0001);
		
	Matrix3d D = makeMatrix(m_a1, m_a2, m_a3);
	Matrix3d B = makeMatrix(m_XA1, m_XA2, m_XA3);
	Matrix3d Bt = B.transpose();
	m_SA = B*D*Bt;
	m_has_eigenv = true;
}

Shadowing::Shadowing(const Vector3d &diag, const Vector3d &tri)  {
	m_SA << diag(0), tri(0),  tri(1),
		tri(0),  diag(1), tri(2),
		tri(1),  tri(2),  diag(2);

	// debug
	assert(diag(0) > 0 && diag(0) <= 1.0);
	assert(diag(1) > 0 && diag(1) <= 1.0);
	assert(diag(2) > 0 && diag(2) <= 1.0);

	// get eigenvalues
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es;
	es.compute(m_SA);
	Vector3d eg_val = es.eigenvalues(); // not sorted
	Matrix3d eg_vec = es.eigenvectors();

	// debug
	if (eg_val(2)<= 0.0
		|| eg_val(1) <= 0.0
		|| eg_val(0) <= 0.0) {
		std::cout << "DEBUG: diag " << to_string(diag) << std::endl;
		std::cout << "DEBUG: tri  " << to_string(tri) << std::endl;
		std::cout << "DEBUG: eg   " << to_string(eg_val) << std::endl;
	}
		
	assert(eg_val(0) > 0.0);
	assert(eg_val(1) > 0.0);
	assert(eg_val(2) > 0.0);

	m_a1 = eg_val(0);
	m_a2 = eg_val(1);
	m_a3 = eg_val(2);
	m_XA1 = eg_vec.col(0);
	m_XA2 = eg_vec.col(1);
	m_XA3 = eg_vec.col(2);

	// debug
	if (std::abs(m_XA1.dot(m_XA2)) > 0.001
		|| std::abs(m_XA1.dot(m_XA3)) > 0.001
		|| std::abs(m_XA2.dot(m_XA3)) > 0.001) {
		std::cout << "DEBUG: diag " << to_string(diag) << std::endl;
		std::cout << "DEBUG: tri  " << to_string(tri) << std::endl;
		std::cout << "DEBUG: eg   " << to_string(eg_val) << std::endl;
		std::cout << "DEBUG: XA1   " << to_string(m_XA1) << std::endl;
		std::cout << "DEBUG: XA2   " << to_string(m_XA2) << std::endl;
		std::cout << "DEBUG: XA3   " << to_string(m_XA3) << std::endl;
	}
		
	assert_almost_eq(m_XA1.norm(), 1.0, 0.0001);
	assert_almost_eq(m_XA2.norm(), 1.0, 0.0001);
	assert_almost_eq(m_XA3.norm(), 1.0, 0.0001);
	assert_almost_eq(m_XA1.dot(m_XA2), 0.0, 0.0001);
	assert_almost_eq(m_XA2.dot(m_XA3), 0.0, 0.0001);
	assert_almost_eq(m_XA1.dot(m_XA3), 0.0, 0.0001);
		

	m_has_eigenv = true;
}

Shadowing::Shadowing(const Matrix3d &SA) :
	Shadowing(Vector3d(SA(0,0), SA(1,1), SA(2,2)),
			  Vector3d(SA(0,1), SA(0,2), SA(1,2)) ) {
	
}

std::string Shadowing::toString() const {
	std::string s = "sh[  SA" + to_string(m_SA) + "\n"
		          + "     egv" + to_string(Vector3d(m_a1, m_a2, m_a3)) + "  ]\n";
	return s;
}

void Shadowing::toSigmaR(Vector3d &sigma, Vector3d &r) const {
	sigma = Eigen::Vector3d(std::sqrt(m_SA(0,0)),
							std::sqrt(m_SA(1,1)),
							std::sqrt(m_SA(2,2)));
	r = Eigen::Vector3d(m_SA(0,1) / (sigma(0)*sigma(1)),
						m_SA(0,2) / (sigma(0)*sigma(2)),
						m_SA(1,2) / (sigma(1)*sigma(2)));
}
