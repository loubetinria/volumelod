#ifndef RAY_H
#define RAY_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <Eigen/Core>

/*
 * From https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
 */
class Ray {
  public:
	
  Ray(const Eigen::Vector3d &orig, const Eigen::Vector3d &dir, double tmax)
	  : o(orig), d(dir), tmax(tmax) {
		invd = Eigen::Vector3d(1.0/d(0),1.0/d(1),1.0/d(2)); // yes, can give -inf or +inf
		sign[0] = (invd(0) < 0);
		sign[1] = (invd(1) < 0);
		sign[2] = (invd(2) < 0);
		tcur = 0.0;
		last_voxel = Eigen::Vector3i(-1,-1,-1);
	}

	Ray() {
		Ray(Eigen::Vector3d(0.0,0.0,0.0),
			Eigen::Vector3d(1.0,1.0,1.0),
			0.0);
		//assert("Not implemented" && 0);
	}
	
	Eigen::Vector3d o,d;       // ray origin and dir 
	Eigen::Vector3d invd; 
    int sign[3];
	double tmax;
	double tcur;
	Eigen::Vector3i last_voxel;
}; 

#endif // RAY_H
