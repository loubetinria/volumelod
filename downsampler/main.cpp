#include <iostream>
#include <string>
#include <iostream>

#include "volume/volume.h"
#include "volume/sggx_volume.h"
#include "volume/trig_volume.h"
#include "triglobes/coslobe.h"
#include "triglobes/sinlobe.h"
#include "sggx/sggx.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "utils/utils.h"

#define NB_SAMPLES_SH 10

/* 
 * argv[1] = directory with input .mtsvol : density, SGGX, albedo
 * argv[2] = output directory
 */
int main(int argc, char *argv[])
{

	if (argc != 3) {
		std::cout << "Usage: Downsampling input_dir/ output_dir/" << std::endl;
		std::cout << "input_dir must contain the following files:" << std::endl;
		std::cout << "  - albedo_0.mtsvol" << std::endl;
		std::cout << "  - density_0.mtsvol" << std::endl;
		std::cout << "  - sggx_sigma_0.mtsvol" << std::endl;
		std::cout << "  - sggx_r_0.mtsvol" << std::endl;
		exit(0);
	}

	// for time measurments
	using namespace boost::posix_time;
	typedef ptime Time;
	typedef time_duration TimeD;

    std::string::size_type sz;
    std::string src(argv[1]);
    std::string dest(argv[2]);
	
	SGGXVolume *volume_current = new SGGXVolume(src);

	// convert SGGX to Trigonometric lobes
	// volume_current->downsample(0, dest, 0, EMeanOcclusion, true);

	
	for (int i = 5; i <= 5; i+=1) { // Level of Detail
		
		int nbSpl = NB_SAMPLES_SH*std::pow(2,2*i);
		std::cout << "Samples: "<< nbSpl << std::endl;

		Time t0_EShadowing(microsec_clock::local_time());

		// Our method, called Aniso in the paper
		volume_current->downsample(i, dest, nbSpl, EShadowing, true);

		Time t1_EShadowing(microsec_clock::local_time());
		TimeD dt_EShadowing = t1_EShadowing - t0_EShadowing;
		std::cout << "Time (s): " << float(dt_EShadowing.total_seconds()) << std::endl; 

		// Our method with simplified model (isotropic self-shadowing)
		// volume_current->downsample(i, dest, nbSpl, EShadowingIso, true);

		// This is method "Transp" of the paper, it preserves transparency
		// volume_current->downsample(i, dest, nbSpl, EMeanOcclusion, true);

		// Naive linear downsampling
		// volume_current->downsample(i, dest, nbSpl, ENaive, true);

		// Unmaintained methods for SGGX output volumes
		//volume_current->downsample(i, dest, nbSpl, EShadowingSGGX, true);
		//volume_current->downsample(i, dest, nbSpl, EShadowingSGGXOpt, true);
		
	}

	delete volume_current;
	
}
