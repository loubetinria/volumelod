#ifndef SGGX_H
#define SGGX_H

#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>
#include "../triglobes/trigphase.h"


class SGGX {
public:
	
    Vector3d m_diag;
    Vector3d m_tri;

    SGGX() {
        m_diag = Vector3d(0,0,0);
        m_tri = Vector3d(0,0,0);
    }

    SGGX(Vector3d d, Vector3d t) {
        m_diag = d;
        m_tri = t;
    }

    SGGX& operator+=(const SGGX &other)
    {
        m_diag += other.m_diag;
        m_tri += other.m_tri;
        return *this;
    }

    SGGX& operator/=(double f)
    {
        m_diag /= f;
        m_tri /= f;
        return *this;
    }

    SGGX& operator*=(double f)
    {
        m_diag *= f;
        m_tri *= f;
        return *this;
    }

	void normalize();
	void toSigmaR(Vector3d &sigma, Vector3d &r) const;
    double projectedArea(Vector3d view) const;
	
	double D(Vector3d wm,
			 double S_xx, double S_yy, double S_zz,
			 double S_xy, double S_xz, double S_yz) const;

	double D(Vector3d wm) const;

	double evalNorm(Sampler &sampler) const;
	double evalIntSigma(Sampler &sampler) const;

	void print_eigenv() const;

	
	double sigma(Vector3d view) const;

	double sigma(Vector3d wi,
				 double S_xx, double S_yy, double S_zz,
				 double S_xy, double S_xz, double S_yz) const;
	
	void buildOrthonormalBasis(Vector3d& omega_1,
							   Vector3d& omega_2,
							   const Vector3d& omega_3) const;

	Vector3d sample_VNDF(const Vector3d wi,
								const double S_xx, const double S_yy, const double S_zz,
								const double S_xy, const double S_xz, const double S_yz,
								const double U1, const double U2) const;

	Vector3d sample_specular(const Vector3d wi,
									const double S_xx, const double S_yy, const double S_zz,
									const double S_xy, const double S_xz, const double S_yz,
									const double U1, const double U2) const;

	double eval_specular(Vector3d wi, Vector3d wo,
						 const double S_xx, const double S_yy, const double S_zz,
						 const double S_xy, const double S_xz, const double S_yz) const;

	double eval_specular(Vector3d wi, Vector3d wo) const;

	Vector3d sample_specular(const Vector3d wi,
									const double U1, const double U2) const;

	double sample(const Vector3d &wi,
				  Vector3d &wo,
				  double &pdf,
				  Sampler &sampler) const;

	double pdf(const Vector3d &wi,
			   const Vector3d &wo,
			   Sampler &sampler) const;

	TrigLobe toTrigLobe() const;
	Matrix3d toMatrix() const;

private:



};

inline SGGX operator*(SGGX lhs, double rhs)
{
  lhs *= rhs;
  return lhs;
}

inline SGGX operator*(double lhs, SGGX rhs)
{
  rhs *= lhs;
  return rhs;
}

inline SGGX operator/(SGGX lhs, double rhs)
{
  lhs /= rhs;
  return lhs;
}



#endif // SGGX_H
