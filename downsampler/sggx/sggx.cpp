#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <Eigen/Core>
#include <Eigen/Eigenvalues> 

#include "sggx.h"
#include "../utils/utils.h"
#include "../utils/sggx2trig.h"

// Numeric evaluation of the norm of SGGX dist.
#define NB_SAMPLES_NORM 10000000

// This is based on the code provided by Heitz et al.

double SGGX::projectedArea(Eigen::Vector3d view) const {
    return std::sqrt((toMatrix() * view).dot(view));
}

double SGGX::sigma(Eigen::Vector3d view) const {
    return projectedArea(view);
}

double SGGX::sigma(Eigen::Vector3d wi,
				   double S_xx, double S_yy, double S_zz,
				   double S_xy, double S_xz, double S_yz) const {
    const double sigma_squared = wi(0)*wi(0)*S_xx + wi(1)*wi(1)*S_yy + wi(2)*wi(2)*S_zz
		+ 2.0f * (wi(0)*wi(1)*S_xy + wi(0)*wi(2)*S_xz + wi(1)*wi(2)*S_yz);

    return (sigma_squared > 0.0) ? std::sqrt(sigma_squared) : 0.0; // conditional to avoid numerical errors
}


Eigen::Matrix3d SGGX::toMatrix() const {
    Eigen::Matrix3d res;
    res << m_diag[0], m_tri[0], m_tri[1],
		m_tri[0], m_diag[1], m_tri[2],
		m_tri[1], m_tri[2], m_diag[2];
    return res;
}

void SGGX::normalize() {
	Eigen::Matrix3d sggx = toMatrix();

	Eigen::EigenSolver<Eigen::Matrix3d> es(sggx);
	Eigen::EigenSolver<Eigen::Matrix3d>::EigenvalueType eg;
	eg = es.eigenvalues();
	assert(eg(0).imag() == 0.0);
	assert(eg(1).imag() == 0.0);
	assert(eg(2).imag() == 0.0);
	assert(eg(0).real() > 0.0);
	assert(eg(1).real() > 0.0);
	assert(eg(2).real() > 0.0);
	double biggest = std::max(eg(0).real(),
							  std::max(eg(1).real(),
									   eg(2).real()));

    // divide by the biggest eigen value:
    m_tri /= biggest;
    m_diag /= biggest;
}

void SGGX::toSigmaR(Eigen::Vector3d &sigma, Eigen::Vector3d &r) const {
	sigma = Eigen::Vector3d(std::sqrt(m_diag[0]),
							std::sqrt(m_diag[1]),
							std::sqrt(m_diag[2]));
	r = Eigen::Vector3d(m_tri[0] / (sigma(0)*sigma(1)),
						m_tri[1] / (sigma(0)*sigma(2)),
						m_tri[2] / (sigma(1)*sigma(2)));
}


// Eval normalization factor of distribution
// Note: this is just a random sampling, could
// be much more efficient
double SGGX::evalNorm(std::default_random_engine &sampler) const {
    double sum = 0.0;
    for (int i = 0; i < NB_SAMPLES_NORM; i++) {
        Eigen::Vector3d dir = sampleSphere(sampler);
        sum += D(dir);
    }
    double res = sum / NB_SAMPLES_NORM * 4.0 * M_PI;
    return res;
}

// Eval normalization factor of distribution
// Note: this is just a random sampling, could
// be much more efficient
double SGGX::evalIntSigma(std::default_random_engine &sampler) const {
    double sum = 0.0;
    for (int i = 0; i < NB_SAMPLES_NORM; i++) {
        Eigen::Vector3d dir = sampleSphere(sampler);
        sum += sigma(dir);
    }
    double res = sum / NB_SAMPLES_NORM * 4.0 * M_PI;
    return res;
}


double SGGX::D(Eigen::Vector3d wm) const {
    return D(wm, m_diag(0), m_diag(1), m_diag(2),
             m_tri(0),  m_tri(1),  m_tri(2));
}

double SGGX::D(Eigen::Vector3d wm,
			   double S_xx, double S_yy, double S_zz,
			   double S_xy, double S_xz, double S_yz) const {
    const double detS = S_xx*S_yy*S_zz - S_xx*S_yz*S_yz - S_yy*S_xz*S_xz - S_zz*S_xy*S_xy + 2.0f*S_xy*S_xz*S_yz;

    const double den = wm(0)*wm(0)*(S_yy*S_zz-S_yz*S_yz) + wm(1)*wm(1)*(S_xx*S_zz-S_xz*S_xz) + wm(2)*wm(2)*(S_xx*S_yy-S_xy*S_xy)
		+ 2.0f*(wm(0)*wm(1)*(S_xz*S_yz-S_zz*S_xy) + wm(0)*wm(2)*(S_xy*S_yz-S_yy*S_xz) + wm(1)*wm(2)*(S_xy*S_xz-S_xx*S_yz));
    if (den == 0)
        return 0.0;

	double sqrt_detS = std::sqrt(std::abs(detS));
	// avoid the use of pow( , 1.5), too expensive
    const double D = std::abs(detS)*sqrt_detS / (M_PI*den*den);
    return D;
}

double SGGX::pdf(const Eigen::Vector3d &wi,
				 const Eigen::Vector3d &wo,
				 std::default_random_engine &sampler) const {
	double pdf =  eval_specular(wi, wo);
	return pdf;
}

double SGGX::sample(const Eigen::Vector3d &wi,
					Eigen::Vector3d &wo,
					double &pdf,
					std::default_random_engine &sampler) const {

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	wo = sample_specular(wi, uniform_dist(sampler), uniform_dist(sampler));
	pdf = eval_specular(wi, wo);
	// return phase / pdf
	return 1.0;
}

double SGGX::eval_specular(Eigen::Vector3d wi, Eigen::Vector3d wo) const {
    return eval_specular(wi, wo,
                         m_diag(0), m_diag(1), m_diag(2), m_tri(0), m_tri(1), m_tri(2));
}

Eigen::Vector3d SGGX::sample_specular(const Eigen::Vector3d wi, const double U1, const double U2) const {
    return sample_specular(wi,
                           m_diag(0), m_diag(1), m_diag(2), m_tri(0), m_tri(1), m_tri(2),
                           U1, U2);
}


double SGGX::eval_specular(Eigen::Vector3d wi, Eigen::Vector3d wo,
						   const double S_xx, const double S_yy, const double S_zz,
						   const double S_xy, const double S_xz, const double S_yz) const {
    Eigen::Vector3d wh = (wi + wo) / (wi + wo).norm();
    return 0.25f * D(wh, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz) / sigma(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz);
}

/// Here wi is assumed to be -ray.dir
Eigen::Vector3d SGGX::sample_specular(const Eigen::Vector3d wi,
									  const double S_xx, const double S_yy, const double S_zz,
									  const double S_xy, const double S_xz, const double S_yz,
									  const double U1, const double U2) const {

    // sample VNDF
    const Eigen::Vector3d wm = sample_VNDF(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz, U1, U2);
	
    // specular reflection
    const Eigen::Vector3d wo = -wi + 2.0 * wm * wm.dot(wi);
    return wo;
}


Eigen::Vector3d SGGX::sample_VNDF(const Eigen::Vector3d wi,
								  const double S_xx, const double S_yy, const double S_zz,
								  const double S_xy, const double S_xz, const double S_yz,
								  const double U1, const double U2) const {

    // generate sample (u, v, w)
    const double r = std::sqrt(U1);
    const double phi = 2.0*M_PI*U2;
    const double u = r*std::cos(phi);
    const double v= r*std::sin(phi);
    const double w = std::sqrt(1.0 - u*u - v*v);

    // build orthonormal basis
    Eigen::Vector3d wk, wj;
    buildOrthonormalBasis(wk, wj, wi);

    // project S in this basis
    const double S_kk = wk(0)*wk(0)*S_xx + wk(1)*wk(1)*S_yy + wk(2)*wk(2)*S_zz
		+ 2.0 * (wk(0)*wk(1)*S_xy + wk(0)*wk(2)*S_xz + wk(1)*wk(2)*S_yz);

    const double S_jj = wj(0)*wj(0)*S_xx + wj(1)*wj(1)*S_yy + wj(2)*wj(2)*S_zz
		+ 2.0 * (wj(0)*wj(1)*S_xy + wj(0)*wj(2)*S_xz + wj(1)*wj(2)*S_yz);

    const double S_ii = wi(0)*wi(0)*S_xx + wi(1)*wi(1)*S_yy + wi(2)*wi(2)*S_zz
		+ 2.0 * (wi(0)*wi(1)*S_xy + wi(0)*wi(2)*S_xz + wi(1)*wi(2)*S_yz);

    const double S_kj = wk(0)*wj(0)*S_xx + wk(1)*wj(1)*S_yy + wk(2)*wj(2)*S_zz
		+ (wk(0)*wj(1) + wk(1)*wj(0))*S_xy
		+ (wk(0)*wj(2) + wk(2)*wj(0))*S_xz
		+ (wk(1)*wj(2) + wk(2)*wj(1))*S_yz;

    const double S_ki = wk(0)*wi(0)*S_xx + wk(1)*wi(1)*S_yy + wk(2)*wi(2)*S_zz
		+ (wk(0)*wi(1) + wk(1)*wi(0))*S_xy + (wk(0)*wi(2) + wk(2)*wi(0))*S_xz + (wk(1)*wi(2) + wk(2)*wi(1))*S_yz;

    const double S_ji = wj(0)*wi(0)*S_xx + wj(1)*wi(1)*S_yy + wj(2)*wi(2)*S_zz
		+ (wj(0)*wi(1) + wj(1)*wi(0))*S_xy
		+ (wj(0)*wi(2) + wj(2)*wi(0))*S_xz
		+ (wj(1)*wi(2) + wj(2)*wi(1))*S_yz;

    // compute normal
    double sqrtDetSkji = std::sqrt(std::abs(S_kk*S_jj*S_ii - S_kj*S_kj*S_ii - S_ki*S_ki*S_jj - S_ji*S_ji*S_kk + 2.0*S_kj*S_ki*S_ji));
    double inv_sqrtS_ii = 1.0 / std::sqrt(S_ii);
    if (S_ii == 0) {
        //std::cout << "problem here" << std::endl;
        Eigen::Vector3d r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }
    double tmp = std::sqrt(S_jj*S_ii-S_ji*S_ji);
    if (tmp == 0) {
        //std::cout << "problem here" << std::endl;
        Eigen::Vector3d r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }
    Eigen::Vector3d Mk(sqrtDetSkji/tmp, 0.0, 0.0);
    Eigen::Vector3d Mj(-inv_sqrtS_ii*(S_ki*S_ji-S_kj*S_ii)/tmp , inv_sqrtS_ii*tmp, 0);
    Eigen::Vector3d Mi(inv_sqrtS_ii*S_ki, inv_sqrtS_ii*S_ji, inv_sqrtS_ii*S_ii);
    Eigen::Vector3d test = u*Mk+v*Mj+w*Mi;
    if (test.norm() == 0) {
        //std::cout << "problem here" << std::endl;
        Eigen::Vector3d r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }

    Eigen::Vector3d wm_kji = (u*Mk+v*Mj+w*Mi) / (u*Mk+v*Mj+w*Mi).norm();

    // rotate back to world basis
    Eigen::Vector3d res = wm_kji(0) * wk + wm_kji(1) * wj + wm_kji(2) * wi;

    return res/res.norm(); // add norm to get more precise

}

void SGGX::buildOrthonormalBasis(Eigen::Vector3d& omega_1, Eigen::Vector3d& omega_2, const Eigen::Vector3d& omega_3) const {
    if(omega_3(2) < -0.9999999) {
        omega_1 = Eigen::Vector3d ( 0.0 , -1.0 , 0.0 );
        omega_2 = Eigen::Vector3d ( -1.0 , 0.0 , 0.0 );
    } else {
        const double a = 1.0 /(1.0 + omega_3(2));
        const double b = -omega_3(0)*omega_3(1)*a ;
        omega_1 = Eigen::Vector3d (1.0 - omega_3(0)*omega_3(0)*a , b , -omega_3(0) );
        omega_2 = Eigen::Vector3d (b , 1.0 - omega_3(1)*omega_3(1)*a , -omega_3(1) );
    }
}

TrigLobe SGGX::toTrigLobe() const {

	// get SGGX eigenvalues
	Eigen::Matrix3d sggx = toMatrix();
	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es;
	es.compute(sggx);
	Vector3d eg_val = es.eigenvalues(); // not sorted
	Matrix3d eg_vec = es.eigenvectors(); 
	assert(eg_val(0) > 0.0);
	assert(eg_val(1) > 0.0);
	assert(eg_val(2) > 0.0);
	sort(eg_val, eg_vec); // increasing order
	eg_val /= eg_val(2); // normalize, just in case;

	int sggx2trig_id = getSGGX2TrigId(eg_val(1), eg_val(0), SGGX2TRIG_RES);

	//std::cout << "Eigenvalues: " << eg_val << std::endl;
	//std::cout << "main axis: " << eg_vec.col(2) << std::endl;
		
	// axis: need eigenvectors!
	// axis for cos is axis of highest eigen value
	// axis for sin is axis of smallest eigen value
	
	TrigLobe trigLobe(sggx2trig_n_cos[sggx2trig_id],
					  sggx2trig_n_sin[sggx2trig_id],
					  eg_vec.col(2), // cos axis
					  eg_vec.col(0), // sin axis
					  sggx2trig_w_cos[sggx2trig_id],
					  sggx2trig_w_sin[sggx2trig_id]);
	
    return trigLobe;
}

void SGGX::print_eigenv() const {
	Matrix3d m = toMatrix();
	Eigen::SelfAdjointEigenSolver<Matrix3d> es;
	es.compute(m);
	Vector3d eg_val = es.eigenvalues(); // not sorted
	//Matrix3d eg_vec = es.eigenvectors(); 
	std::cout << eg_val(0) << " " << eg_val(1) << " " << eg_val(2)  << std::endl;
}
