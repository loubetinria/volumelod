#include "sggx_volume.h"
#include "volume_io.h"
#include "../utils/utils.h"
#include "../triglobes/trigphase.h"
#include "../utils/shadowing.h"
#include <boost/thread.hpp>

#include <cmath> // isnan

// in estimateShadowing
#define MAX_THREADS 32

SGGXSHVolume::SGGXSHVolume() : Volume() {
	m_S1 = NULL;
    m_S1 = NULL;
    m_sh = NULL;
	m_albedo_ss = NULL;
	m_albedo_ms = NULL;
}

SGGXSHVolume::SGGXSHVolume(const SGGXVolume *sggxVol, int level,
						   std::string dest,
						   std::string suffix) : Volume() {

	double factor = std::pow(2.0, level);
	Array3i sggxVol_grid = sggxVol->getGrid();
	m_grid(0) = int(std::ceil(double(sggxVol_grid(0))/factor));
	m_grid(1) = int(std::ceil(double(sggxVol_grid(1))/factor));
	m_grid(2) = int(std::ceil(double(sggxVol_grid(2))/factor));
	m_lod = sggxVol->getLoD()+level;
	
	// divide voxel size by pow
	Array3d new_vs = sggxVol->getVoxelSize() * factor;		
	std::cout << "new_vs " << to_string(new_vs) << std::endl;
	m_min = sggxVol->getMin();
	m_max = m_min;
	m_max(0) += new_vs(0)*m_grid(0);
	m_max(1) += new_vs(1)*m_grid(1);
	m_max(2) += new_vs(2)*m_grid(2);

	m_vsize = getVoxelSize();
	std::cout << "m_vsize " << to_string(m_vsize) << std::endl;

	std::cout << "Downsampling ("<< suffix <<", rec) volume " << to_string(sggxVol_grid)
			  << " to LoD " << sggxVol->getLoD()+level << std::endl;
	
	std::cout << "New bbox: " << to_string(m_min)
			  << " " << to_string(m_max) << std::endl;

	std::cout << "New size: " << to_string(m_grid) << std::endl;
	
	// set paths
	setPaths(dest, suffix);
	
	// alloc volumes
	int new_size = m_grid(0) * m_grid(1) * m_grid(2);
	
	m_density = new float[new_size];

	m_S1 = new float[new_size*3];
	m_S2 = new float[new_size*3];
	m_sh = new float[new_size];
	m_albedo_ss = new float[new_size*3];
	m_albedo_ms = new float[new_size*3];

	// init new volume with 0 in order to avoid
	// error when estimating the scattering in the new volume
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				int id_new = getId(x, y, z);
				m_density[id_new] = 0;

				m_S1[id_new*3] = 1;
				m_S1[id_new*3+1] = 1;
				m_S1[id_new*3+2] = 1;

				m_S2[id_new*3] = 0;
				m_S2[id_new*3+1] = 0;
				m_S2[id_new*3+2] = 0;
				
				m_sh[id_new] = 1;
				m_albedo_ss[id_new*3] = 1;
				m_albedo_ss[id_new*3+1] = 1;
				m_albedo_ss[id_new*3+2] = 1;

				m_albedo_ms[id_new*3] = 1;
				m_albedo_ms[id_new*3+1] = 1;
				m_albedo_ms[id_new*3+2] = 1;
			}
		}
	}
}


SGGXSHVolume::~SGGXSHVolume() {

	if (m_S1 != NULL)
		delete [] m_S1;

	if (m_S2 != NULL)
		delete [] m_S2;
	
	if (m_sh != NULL)
		delete [] m_sh;

	if (m_albedo_ss != NULL)
		delete [] m_albedo_ss;

	if (m_albedo_ms != NULL)
		delete [] m_albedo_ms;
}

SGGX SGGXSHVolume::getSGGX(int id) const {
	assert_goodId(id);
	SGGX res(Vector3d(m_S1[id*3],
					  m_S1[id*3+1],
					  m_S1[id*3+2]),
			 Vector3d(m_S2[id*3],
					  m_S2[id*3+1],
					  m_S2[id*3+2]));
	return res;
}

void SGGXSHVolume::writeSGGX(int id, const SGGX &sggx) {
	m_S1[id*3] = sggx.m_diag(0);
	m_S1[id*3+1] = sggx.m_diag(1);
	m_S1[id*3+2] = sggx.m_diag(2);
	m_S2[id*3] = sggx.m_tri(0);
	m_S2[id*3+1] = sggx.m_tri(1);
	m_S2[id*3+2] = sggx.m_tri(2);
}

/*
 *  Return attenuation coefficient ( = density x sggx.sigma )
 */
double SGGXSHVolume::getSigma_t(int id, const Vector3d &w) const {
	assert_goodId(id);
	double density = double(m_density[id]);
	if (density > 0) {
		SGGX sggx = getSGGX(id);
		density *= sggx.sigma(w);
	}
	return density;
}

/*
 *  Return attenuation coefficient ( = density x sggx.sigma )
 */

Array3d SGGXSHVolume::getSigma_ss(int id, const Vector3d &w) const {
	return getSigma_t(id, w)*getAlbedo_ss(id);
}

//
// Sample SGGX phase function in the voxel id
// Assume that sampling is perfect (it is the case for SGGX)
//
Vector3d SGGXSHVolume::samplePhase_ss(int id, const Vector3d &wi, Sampler &sampler) const {
	SGGX sggx = getSGGX(id);
	Vector3d wo;
	double pdf;
	sggx.sample(wi, wo, pdf, sampler);
	return wo;
}

Vector3d SGGXSHVolume::samplePhase_full(int id, const Vector3d &wi, Sampler &sampler) const {
	double sh = getShadowing(id);
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	if (uniform_dist(sampler) < sh) {
		SGGX sggx = getSGGX(id);
		Vector3d wo;
		double pdf;
		sggx.sample(wi, wo, pdf, sampler);
		return wo;
	} else {
		return sampleSphere(sampler); // todo: should be accurate ms phase for sggx
	}
}

void SGGXSHVolume::write() {

	// paths are supposed to be set

	// carefull: density should be max 1.0
	float max_density = 0.0;
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				int id = getId(x, y, z);
				max_density = std::max(max_density, m_density[id]);
			}
		}
	}
	std::cout << "MAX DENSITY = " << max_density << std::endl;
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				m_density[getId(x, y, z)] /= max_density;
			}
		}
	}

	
	std::cout << "Writing " << m_density_path << std::endl;
	writeFloatFile(m_density_path.c_str(), &m_density,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_albedo_ss_path << std::endl;
	writeFloat3File(m_albedo_ss_path.c_str(), &m_albedo_ss,
					m_grid, m_min, m_max);

	std::cout << "Writing " << m_albedo_ms_path << std::endl;
	writeFloat3File(m_albedo_ms_path.c_str(), &m_albedo_ms,
					m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_S1_path << std::endl;
	writeFloat3File(m_S1_path.c_str(), &m_S1,
					m_grid, m_min, m_max);

	std::cout << "Writing " << m_S2_path << std::endl;
	writeFloat3File(m_S2_path.c_str(), &m_S2,
					m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_sh_path << std::endl;
	writeFloatFile(m_sh_path.c_str(), &m_sh,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_cdf_path << std::endl;
	float *cdf = new float[m_grid(0) * m_grid(1) * m_grid(2)];
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				cdf[getId(x, y, z)] = (m_density[getId(x, y, z)] > 0) ? 1.0 : 0.0;
			}
		}
	}

	writeFloatFile(m_cdf_path.c_str(), &cdf,
				   m_grid, m_min, m_max);

	delete [] cdf;
}

void SGGXSHVolume::writeShadowing(int id, double sh) {
	assert_goodId(id);
	m_sh[id] = sh;
}

double SGGXSHVolume::getShadowing(int id) const {
	assert_goodId(id);
	return m_sh[id];
}

void SGGXSHVolume::setPaths(std::string dest, std::string sufix) {
	std::string s_end = "_" + to_string(m_lod) + "_" + sufix + ".mtsvol";
	m_density_path = dest + "density" + s_end;
	m_S1_path = dest + "S1" + s_end;
	m_S2_path = dest + "S2" + s_end;
	m_sh_path = dest + "sh" + s_end;
	m_albedo_ss_path = dest + "albedo_ss" + s_end;
	m_albedo_ms_path = dest + "albedo_ms" + s_end;
	m_cdf_path = dest + "cdf" + s_end;

}


void SGGXSHVolume::writeVoidVoxel(int id) {
	assert_goodId(id);
	writeDensity(id, 0);
	writeAlbedo_ss(id, Array3d(0,0,0));
	writeAlbedo_ms(id, Array3d(0,0,0));
	writeSGGX(id, SGGX(Vector3d(1,1,1), Vector3d(0,0,0)));
	writeShadowing(id, 1);
}
