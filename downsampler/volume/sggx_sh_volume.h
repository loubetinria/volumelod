#ifndef SGGX_SH_VOLUME_H
#define SGGX_SH_VOLUME_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <Eigen/Core>

#include "../sggx/sggx.h"
#include "../utils/ray.h"
#include "../utils/box3.h"
#include "volume.h"
#include "trig_volume.h"
#include "sggx_volume.h"

class TrigVolume;
class SGGXVolume;

class SGGXSHVolume : public Volume {

    friend class VolumeTest;
	
  public:
	// Constructor
	SGGXSHVolume();
	SGGXSHVolume(const SGGXVolume *sggxVol, int level,
				 std::string dest,
				 std::string suffix);
	
	// Destructor
	~SGGXSHVolume();

	double getSigma_t(int id, const Vector3d &w) const;

	Array3d getSigma_ss(int id, const Vector3d &w) const;

	Vector3d samplePhase_ss(int id, const Vector3d &wi, Sampler &sampler) const;
	
	Vector3d samplePhase_full(int id, const Vector3d &wi, Sampler &sampler) const;

	inline Array3d getAlbedo_ss(int id) const {
		assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
		return Array3d(m_albedo_ss[id*3],
					   m_albedo_ss[id*3+1],
					   m_albedo_ss[id*3+2]);
	}

	inline void writeAlbedo_ss(int id, Array3d a) {
		assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
		m_albedo_ss[id*3] = a(0);
		m_albedo_ss[id*3+1] = a(1);
		m_albedo_ss[id*3+2] = a(2);
	};
	
	inline void writeAlbedo_ms(int id, Array3d a) {
		assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
		m_albedo_ms[id*3] = a(0);
		m_albedo_ms[id*3+1] = a(1);
		m_albedo_ms[id*3+2] = a(2);
	};

	SGGX getSGGX(int id) const;

	void writeSGGX(int id, const SGGX &sggx);

	double getShadowing(int id) const;

	void writeShadowing(int id, double sh);

	void writeTrigLobe(int id, const TrigLobe &tl) {
		assert(0 && "not implemented");
	}
	
	void writeShadowing(int id, const Shadowing &sh) {
		assert(0 && "not implemented");
	}

	bool estimateMultScat(const Box3 &box, Sampler &sampler,
						  Array3d &mult_scatt,
						  int nb_samples,
						  int max_loop) const {
		assert(0 && "not implemented");
	}

	void writeVoidVoxel(int id);
	
	void setPaths(std::string dest, std::string sufix);
	
	void write();
	
  protected:
		
	// additional data for SGGX
	std::string m_S1_path;
    std::string m_S2_path;
	std::string m_sh_path;
	std::string m_cdf_path;
	std::string m_albedo_ss_path;
	std::string m_albedo_ms_path;

    float *m_S1;
    float *m_S2;
    float *m_sh;
	float *m_albedo_ss;
    float *m_albedo_ms;

};



#endif // SGGX_SH_VOLUME_H
