#include "trig_volume.h"
#include "volume_io.h"

#include <Eigen/Dense> // cross product

TrigVolume::TrigVolume(const SGGXVolume *sggxVol, int level,
					   std::string dest,
					   std::string suffix) : Volume() {

	double factor = std::pow(2.0, level);
	Array3i sggxVol_grid = sggxVol->getGrid();
	m_grid(0) = int(std::ceil(double(sggxVol_grid(0))/factor));
	m_grid(1) = int(std::ceil(double(sggxVol_grid(1))/factor));
	m_grid(2) = int(std::ceil(double(sggxVol_grid(2))/factor));
	m_lod = sggxVol->getLoD()+level;
	
	// divide voxel size by pow
	Array3d new_vs = sggxVol->getVoxelSize() * factor;		
	std::cout << "new_vs " << to_string(new_vs) << std::endl;
	m_min = sggxVol->getMin();
	m_max = m_min;
	m_max(0) += new_vs(0)*m_grid(0);
	m_max(1) += new_vs(1)*m_grid(1);
	m_max(2) += new_vs(2)*m_grid(2);

	m_vsize = getVoxelSize();
	std::cout << "m_vsize " << to_string(m_vsize) << std::endl;

	std::cout << "Downsampling ("<< suffix <<", rec) volume " << to_string(sggxVol_grid)
			  << " to LoD " << sggxVol->getLoD()+level << std::endl;
	
	std::cout << "New bbox: " << to_string(m_min)
			  << " " << to_string(m_max) << std::endl;

	std::cout << "New size: " << to_string(m_grid) << std::endl;
	
	// set paths
	setPaths(dest, suffix);
	
	// alloc volumes
	int new_size = m_grid(0) * m_grid(1) * m_grid(2);
	
	m_density = new float[new_size];
	m_albedo_ss = new unsigned char[new_size*3];

	m_tl_cos = new unsigned char[new_size*3];
	m_tl_sin = new unsigned char[new_size*3];
	m_tl_w = new unsigned char[new_size*3];
	m_sh_sigma = new unsigned char[new_size*3];
	m_sh_r = new unsigned char[new_size*3];
	m_albedo_ms = new unsigned char[new_size*3];

	// init new volume with 0 in order to avoid
	// error when estimating the scattering in the new volume
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				int id_new = getId(x, y, z);

				m_density[id_new] = 0;
				m_albedo_ss[id_new*3] = 0;
				m_albedo_ss[id_new*3+1] = 0;
				m_albedo_ss[id_new*3+2] = 0;

				m_tl_cos[id_new*3] = 127;
				m_tl_cos[id_new*3+1] = 127;
				m_tl_cos[id_new*3+2] = 1;

				m_tl_sin[id_new*3] = 127;
				m_tl_sin[id_new*3+1] = 127;
				m_tl_sin[id_new*3+2] = 1;
				
				m_tl_w[id_new*3] = 0;
				m_tl_w[id_new*3+1] = 0;
				m_tl_w[id_new*3+2] = 0;
	
				m_sh_sigma[id_new*3] = 255;
				m_sh_sigma[id_new*3+1] = 255;
				m_sh_sigma[id_new*3+2] = 255;

				m_sh_r[id_new*3] = 127;
				m_sh_r[id_new*3+1] = 127;
				m_sh_r[id_new*3+2] = 127;

				m_albedo_ms[id_new*3] = 0;
				m_albedo_ms[id_new*3+1] = 0;
				m_albedo_ms[id_new*3+2] = 0;
			}
		}
	}
}



TrigVolume::~TrigVolume() {
	if (m_density != NULL)
		delete [] m_density;

	if (m_albedo_ss != NULL)
		delete [] m_albedo_ss;
	
	if (m_tl_cos != NULL)
		delete [] m_tl_cos;

	if (m_tl_sin != NULL)
		delete [] m_tl_sin;

	if (m_tl_w != NULL)
		delete [] m_tl_w;

	if (m_sh_sigma != NULL)
		delete [] m_sh_sigma;

	if (m_sh_r != NULL)
		delete [] m_sh_r;

	if (m_albedo_ms != NULL)
		delete [] m_albedo_ms;
}

double TrigVolume::getSigma_t(int id, const Vector3d &w) const {
	double density = double(m_density[id]);
	if (density > 0) {
		TrigLobe tl = getTrigLobe(id);
		Shadowing sh = getShadowing(id);
		TrigPhase tp(tl, sh);
		density*= tp.sigma_t(w);
	}
	return density;
}

/**
 * @warning give only the "local single scattering coefficient" (without local ms)
 */
Array3d TrigVolume::getSigma_ss(int id, const Vector3d &w) const {
	double density = double(m_density[id]);
	Array3d res(density, density, density);
	if (density > 0) {
		TrigLobe tl = getTrigLobe(id);
		Shadowing sh = getShadowing(id);
		TrigPhase tp(tl, sh);
		Array3d ssalbedo = getAlbedo_ss(id);
		//res*=tp.sigma_s(w, ssalbedo);
		res *= ssalbedo*tp.sigma_ss(w);
		
		if (std::isnan(res[0])) {
			std::cout << "Debug density: " << density << std::endl;
			std::cout << "Debug sh: \n" << sh.toString() << std::endl;
			std::cout << "Debug tl: \n" << tl.toString() << std::endl;
			std::cout << "Debug ssalbedo: " << to_string(ssalbedo) << std::endl;
			std::cout << "Debug w: " << to_string(w) << std::endl;
		}
	}


	assert(!std::isnan(res[0]));
	assert(!std::isnan(res[1]));
	assert(!std::isnan(res[2]));

	return res;
}

void TrigVolume::setPaths(std::string dest, std::string sufix) {
	std::string s_end = "_" + to_string(m_lod) + "_" + sufix + ".mtsvol";
	m_density_path = dest + "density" + s_end;
	m_albedo_ss_path = dest + "albedo_ss" + s_end;
	m_tl_cos_path = dest + "tl_cos" + s_end;
	m_tl_sin_path = dest + "tl_sin" + s_end;
	m_tl_w_path = dest + "tl_w" + s_end;
	m_sh_sigma_path = dest + "sh_sigma" + s_end;
	m_sh_r_path = dest + "sh_r" + s_end;
	m_albedo_ms_path = dest + "albedo_ms" + s_end;
}

void TrigVolume::write() {

	// paths are supposed to be set
		
	std::cout << "Writing " << m_density_path << std::endl;
	writeFloatFile(m_density_path.c_str(), &m_density,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_albedo_ss_path << std::endl;
	writeChar3File(m_albedo_ss_path.c_str(), &m_albedo_ss,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_tl_cos_path << std::endl;
	writeChar3File(m_tl_cos_path.c_str(), &m_tl_cos,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_tl_sin_path << std::endl;
	writeChar3File(m_tl_sin_path.c_str(), &m_tl_sin,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_tl_w_path << std::endl;
	writeChar3File(m_tl_w_path.c_str(), &m_tl_w,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_sh_sigma_path << std::endl;
	writeChar3File(m_sh_sigma_path.c_str(), &m_sh_sigma,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_sh_r_path << std::endl;
	writeChar3File(m_sh_r_path.c_str(), &m_sh_r,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_albedo_ms_path << std::endl;
	writeChar3File(m_albedo_ms_path.c_str(), &m_albedo_ms,
				   m_grid, m_min, m_max);
}

/*
 *  Sample SGGX phase function in the voxel id
 *  Assume that sampling is perfect (it is the case for SGGX)
 */
Vector3d TrigVolume::samplePhase_ss(int id, const Vector3d &wi, Sampler &sampler) const {
	TrigLobe tl = getTrigLobe(id);
	Shadowing sh = getShadowing(id);
	TrigPhase tp(tl, sh);
	//Array3d ssalbedo = getSpecular(id);
	//Array3d rgb_pdf; // RGB phase value divided by pdf
	//double pdf;
	//return tp.sample(wi, ssalbedo, rgb_pdf, pdf, sampler);
	return tp.sample_ss(wi, sampler);
}

/*
 * Sample either ss or ms lobe depending of local shadowing,
 * without importance sampling with albedos (ms albedo not known yet)
 */
Vector3d TrigVolume::samplePhase_full(int id, const Vector3d &wi, Sampler &sampler) const {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	TrigLobe tl = getTrigLobe(id);
	Shadowing sh = getShadowing(id);
	TrigPhase tp(tl, sh);
	double proba_ss = tp.sigma_ss(wi) / tp.sigma_t(wi);
	if (uniform_dist(sampler) <= proba_ss) {
		return tp.sample_ss(wi, sampler);
	} else {
		if (sh.m_SA.trace() >= 3)
			return tp.sample_ss(wi, sampler); // no ms lobe if no sh
		return tp.sample_ms(sampler);
	}
}

Array3d TrigVolume::getAlbedo_full(int id, const Vector3d &wi) const {
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	TrigLobe tl = getTrigLobe(id);
	Shadowing sh = getShadowing(id);
	TrigPhase tp(tl, sh);
	double proba_ss = tp.sigma_ss(wi) / tp.sigma_t(wi);
	return getAlbedo_ss(id)*proba_ss +(1-proba_ss)*getAlbedo_ms(id);
}

TrigLobe TrigVolume::getTrigLobe(int id) const {
	assert_goodId(id);

	if (m_density[id] == 0.0) {
		std::cout << "Warning: reading data where density is 0" << std::endl;
		return TrigLobe();
	}
	
	TrigLobe tl;

	tl.XD_cos(0) = double(m_tl_cos[id*3])/255.0*2.0-1.0;
	tl.XD_cos(1) = double(m_tl_cos[id*3+1])/255.0*2.0-1.0;

	// debug
	if (tl.XD_cos(0)*tl.XD_cos(0)+tl.XD_cos(1)*tl.XD_cos(1) > 1.04) {
		std::cout << "TrigVolume::getTrigLobe: cos data is suspicious!" << std::endl;
		std::cout << double(m_tl_cos[id*3]) << std::endl;
		std::cout << double(m_tl_cos[id*3+1]) << std::endl;
		std::cout << tl.XD_cos(0) << std::endl;
		std::cout << tl.XD_cos(1) << std::endl;
	}
	
	//assert(tl.XD_cos(0)*tl.XD_cos(0)+tl.XD_cos(1)*tl.XD_cos(1) <= 1.0);
	tl.XD_cos(2) = safe_sqrt(1.0-tl.XD_cos(0)*tl.XD_cos(0)-tl.XD_cos(1)*tl.XD_cos(1));
	tl.XD_cos /= tl.XD_cos.norm();

	tl.XD_sin(0) = double(m_tl_sin[id*3])/255.0*2.0-1.0;
	tl.XD_sin(1) = double(m_tl_sin[id*3+1])/255.0*2.0-1.0;
	// debug
	if (tl.XD_sin(0)*tl.XD_sin(0)+tl.XD_sin(1)*tl.XD_sin(1) > 1.04) {
		std::cout << "TrigVolume::getTrigLobe: sin data is suspicious!" << std::endl;
		std::cout << int(m_tl_sin[id*3]) << std::endl;
		std::cout << int(m_tl_sin[id*3+1]) << std::endl;
		std::cout << tl.XD_sin(0) << std::endl;
		std::cout << tl.XD_sin(1) << std::endl;
	}
	//assert(tl.XD_sin(0)*tl.XD_sin(0)+tl.XD_sin(1)*tl.XD_sin(1) <= 1.000001);
	tl.XD_sin(2) = safe_sqrt(1.0-tl.XD_sin(0)*tl.XD_sin(0)-tl.XD_sin(1)*tl.XD_sin(1));
	tl.XD_sin /= tl.XD_sin.norm();
	
	assert_almost_eq(tl.XD_cos.norm(), 1.0, 0.00001, DEBUG_INFO);
	assert_almost_eq(tl.XD_sin.norm(), 1.0, 0.00001, DEBUG_INFO);
	
	tl.n_cos = m_tl_cos[id*3+2];
	tl.n_sin = m_tl_sin[id*3+2];

	assert(tl.n_cos >= 1 && tl.n_cos <= 20);
	assert(tl.n_sin >= 1 && tl.n_sin <= 20);

	tl.w_cos = double(m_tl_w[id*3])/255.0;
	tl.w_sin = double(m_tl_w[id*3+1])/255.0;
	
	assert(tl.w_cos >= 0.0 && tl.w_cos <= 1.0);
	assert(tl.w_sin >= 0.0 && tl.w_sin <= 1.0);
	assert(tl.w_cos + tl.w_sin <= 1.0);

	return tl;
}

Shadowing TrigVolume::getShadowing(int id) const {
	assert_goodId(id);

	if (m_density[id] == 0.0) {
		std::cout << "Warning: reading data where density is 0" << std::endl;
		return Shadowing();
	}
	
	double sigma_x = double(m_sh_sigma[id*3])/255.0;
	double sigma_y = double(m_sh_sigma[id*3+1])/255.0;
	double sigma_z = double(m_sh_sigma[id*3+2])/255.0;

	// debug
	assert(sigma_x > 0 && sigma_x <= 1.0);
	assert(sigma_y > 0 && sigma_y <= 1.0);
	assert(sigma_z > 0 && sigma_z <= 1.0);
	
	
	double r_xy = (double(m_sh_r[id*3])/255.0*2.0-1.0);
	double r_xz = (double(m_sh_r[id*3+1])/255.0*2.0-1.0);
	double r_yz = (double(m_sh_r[id*3+2])/255.0*2.0-1.0);
	Shadowing sh(Vector3d(sigma_x*sigma_x,
						  sigma_y*sigma_y,
						  sigma_z*sigma_z),
				 Vector3d(r_xy*sigma_x*sigma_y,
						  r_xz*sigma_x*sigma_z,
						  r_yz*sigma_y*sigma_z));
	return sh;
}

void TrigVolume::writeTrigLobe(int id, const TrigLobe &tl) {
	assert_goodId(id);

	
	Vector3d XD_cos = tl.XD_cos;
	Vector3d XD_sin = tl.XD_sin;
	
	if (XD_cos(2) < 0)
		XD_cos *= -1;
	if (XD_sin(2) < 0)
		XD_sin *= -1;

	assert_almost_eq(XD_cos.norm(), 1.0, 0.0001, DEBUG_INFO);
	assert_almost_eq(XD_sin.norm(), 1.0, 0.0001, DEBUG_INFO);
	
	m_tl_cos[id*3] = (unsigned char)((XD_cos(0)*0.5+0.5)*255);
	m_tl_cos[id*3+1] = (unsigned char)((XD_cos(1)*0.5+0.5)*255);
	m_tl_cos[id*3+2] = (unsigned char)(tl.n_cos);
	
	m_tl_sin[id*3] = (unsigned char)((XD_sin(0)*0.5+0.5)*255);
	m_tl_sin[id*3+1] = (unsigned char)((XD_sin(1)*0.5+0.5)*255);
	m_tl_sin[id*3+2] = (unsigned char)(tl.n_sin);

	m_tl_w[id*3] = (unsigned char)(tl.w_cos*255);
	m_tl_w[id*3+1] = (unsigned char)(tl.w_sin*255);
	//m_tl_w[id*3+2] = (unsigned char)(0); // useless
}



void TrigVolume::writeShadowing(int id, const Shadowing &sh) {
	assert_goodId(id);
	
	Eigen::Vector3d sigma, r;
	sh.toSigmaR(sigma, r);

	// debug
	assert(sigma(0) > 0 && sigma(0) <= 1.0);
	assert(sigma(1) > 0 && sigma(1) <= 1.0);
	assert(sigma(2) > 0 && sigma(2) <= 1.0);

	m_sh_sigma[id*3] = (unsigned char)(sigma(0)*255.0);
	m_sh_sigma[id*3+1] = (unsigned char)(sigma(1)*255.0);
	m_sh_sigma[id*3+2] = (unsigned char)(sigma(2)*255.0);
	m_sh_r[id*3] = (unsigned char)((r(0)*0.5+0.5)*255.0);
	m_sh_r[id*3+1] = (unsigned char)((r(1)*0.5+0.5)*255.0);
	m_sh_r[id*3+2] = (unsigned char)((r(2)*0.5+0.5)*255.0);
}

void TrigVolume::writeVoidVoxel(int id) {
	assert_goodId(id);
	writeDensity(id, 0);
	writeAlbedo_ss(id, Array3d(0,0,0));
	writeAlbedo_ms(id, Array3d(0,0,0));
	writeShadowing(id, Shadowing());
	writeTrigLobe(id, TrigLobe());
}

/**
 * Estimate the mean ms albedo (= ray throughput) in the volume. This 
 * implementation casts rays and accumulates throughputs.
 */
bool TrigVolume::estimateMultScat(const Box3 &box, Sampler &sampler,
								  Array3d &mult_scatt,
								  int nb_samples,
								  int max_loop) const {
	
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	mult_scatt = Array3d(0.0,0.0,0.0);
	int n_samples_ms = 0;
	
	for (int i = 0; i < nb_samples; i++) {
		
		Ray r = box.sampleRay(sampler, false);
		Vector3d dir = r.d;
		Vector3d position;
		Array3d val_pdf_scattering;
		
		// Sample a scattering even in the box, along ray r, fails if there is
		// no density along the ray.
		int id;
		bool success = sampleScattering(r, sampler, position, id,
										val_pdf_scattering,
										false /* use attenuation only */);

		// As we sampled scattering using attenuation, sample weight should be 1
		if (!success) {
			continue;
		}
		
		assert(success);
		assert(m_density[id] > 0);

		// Once we get the first intersection, loop:
		//  - phase sampling
		//  - transparency sampling (leaving or not the box)
		//  - sample new scattering position

		Array3d throughput = getAlbedo_full(id, -dir);
		double tmin, tmax;
		double transp;

		int nb_bounces = 1;		
		while (nb_bounces < max_loop) {
			Vector3d wo = samplePhase_full(id, -dir, sampler);
			r = Ray(position,
					wo,
					(Vector3d(box.bounds[1]-box.bounds[0])).norm());
			box.intersect(r, tmin, tmax);
			r.tmax = tmax;
			
			// Get transparency in the box
			integrate(r, transp);
			double sample_t = uniform_dist(sampler);
			if (sample_t < transp) {
				// Leave the box
				mult_scatt += throughput;
				n_samples_ms++;
				break;
			} else {
				// Sample new position
				bool sc = sampleScattering(r, sampler, position, id,
										   val_pdf_scattering,
										   false /* use attenuation only */);

				assert(sc);
				
				// Set new direction
				throughput *= getAlbedo_full(id, -dir);
				dir = wo;
				nb_bounces++;
			}
		}
		
		if (nb_bounces == max_loop) {
			// If max_loop is reached, take the current throughput
			mult_scatt += throughput;
			n_samples_ms++;
		}
	}
	   
	if (n_samples_ms > 0) {
		mult_scatt /= n_samples_ms;
		return true;
	}
	return false;
}
