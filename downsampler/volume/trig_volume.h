#ifndef TRIG_VOLUME_H
#define TRIG_VOLUME_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <Eigen/Core>

#include "../sggx/sggx.h"
#include "../utils/ray.h"
#include "../utils/box3.h"
#include "../triglobes/trigphase.h"
#include "sggx_volume.h"
#include "../utils/shadowing.h"

class VolumeTest;
class SGGXVolume;

class TrigVolume : public Volume {

    friend class VolumeTest;
	
  public:
	// Constructor
	TrigVolume(const SGGXVolume *sggxVol, int level,
			   std::string dest,
			   std::string suffix);

	// Destructor
	~TrigVolume();

	double getSigma_t(int id, const Vector3d &w) const;
 
	Array3d getSigma_ss(int id, const Vector3d &w) const;
	
	Vector3d samplePhase_ss(int id, const Vector3d &wi, Sampler &sampler) const;
	
	Vector3d samplePhase_full(int id, const Vector3d &wi, Sampler &sampler) const;

	inline Array3d getAlbedo_ss(int id) const {
		return Array3d(m_albedo_ss[id*3]/255.0,
					   m_albedo_ss[id*3+1]/255.0,
					   m_albedo_ss[id*3+2]/255.0);
	};

	inline void writeAlbedo_ss(int id, Array3d a) {
		m_albedo_ss[id*3] = (unsigned)(a(0)*255);
		m_albedo_ss[id*3+1] = (unsigned)(a(1)*255);
		m_albedo_ss[id*3+2] = (unsigned)(a(2)*255);
	};


	inline Array3d getAlbedo_ms(int id) const {
		return Array3d(m_albedo_ms[id*3]/255.0,
					   m_albedo_ms[id*3+1]/255.0,
					   m_albedo_ms[id*3+2]/255.0);
	};

	inline void writeAlbedo_ms(int id, Array3d a) {
		m_albedo_ms[id*3] = (unsigned)(a(0)*255);
		m_albedo_ms[id*3+1] = (unsigned)(a(1)*255);
		m_albedo_ms[id*3+2] = (unsigned)(a(2)*255);
	};

	Array3d getAlbedo_full(int id, const Vector3d &wi) const;
	
	TrigLobe getTrigLobe(int id) const;
	
	Shadowing getShadowing(int id) const;
	
	void writeTrigLobe(int id, const TrigLobe &tl);
	
	void writeShadowing(int id, const Shadowing &sh);

	void writeShadowing(int id, double sh) {
		assert(0 && "not implemented");
	}
	
	void writeSGGX(int id, const SGGX &sggx) {
		assert(0 && "not implemented");
	}

	void writeVoidVoxel(int id);
	
	void setPaths(std::string dest, std::string sufix);

	void write();

	bool estimateMultScat(const Box3 &box, Sampler &sampler,
						  Array3d &mult_scatt,
						  int nb_samples,
						  int max_loop) const;
	
  protected:

	std::string m_tl_cos_path;
	std::string m_tl_sin_path;
	std::string m_tl_w_path;
	std::string m_sh_sigma_path;
	std::string m_sh_r_path;
	std::string m_albedo_ss_path;
	std::string m_albedo_ms_path;

	unsigned char *m_tl_cos;
	unsigned char *m_tl_sin;
	unsigned char *m_tl_w;
	
	unsigned char *m_sh_sigma;
    unsigned char *m_sh_r;

	unsigned char *m_albedo_ss;
	unsigned char *m_albedo_ms;
};



#endif // TRIG_VOLUME_H
