#ifndef SGGX_VOLUME_H
#define SGGX_VOLUME_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <Eigen/Core>

#include "../sggx/sggx.h"
#include "../utils/ray.h"
#include "../utils/box3.h"
#include "volume.h"
#include "trig_volume.h"
#include "sggx_sh_volume.h"


class TrigVolume;
class SGGXSHVolume;

class SGGXVolume : public Volume {

    friend class VolumeTest;
	
  public:

	SGGXVolume();
	
	/**
	 * Init a SGGX Volume from a source dir.
	 * Reads input files for albedo, density and sggx distribution.
	 * @param src source dir containing input volume data
	 */ 
	SGGXVolume(std::string src);

	/**
	 * Create an empty SGGX Volume for downsampling, given
	 * an input SGGX Volume.
	 * @param sggxVol input SGGX Volume
	 * @param level downsampling level
	 * @param dest output directory for downsampled files
	 * @param suffix suffix for downsampled files
	 */ 
	SGGXVolume(const SGGXVolume *sggxVol, int level,
			   std::string dest, std::string suffix);
	
	// Destructor
	~SGGXVolume();

	// for tests
	void getCopyData(double *density,
					 unsigned char *diffuse,
					 unsigned char *albedo,
					 unsigned char *sggx_sigma,
					 unsigned char *sggx_r,
					 unsigned char *sggx_sigma_shadowing,
					 unsigned char *sggx_r_shadowing,
					 Eigen::Array3i &grid,
					 int &lod,
					 Eigen::Array3d &min,
					 Eigen::Array3d &max) const;

	Volume* downsample(int level, std::string dest,
					   int nb_samples_sh,
					   EMethod method,
					   bool freeResult) const;

	void downsample_task(int level, int x_begin, int x_end,
						 Volume* new_volume, int nb_samples_sh,
						 EMethod method) const;

	double getSigma_t(int id, const Vector3d &w) const;

	
	Array3d getSigma_ss(int id, const Vector3d &w) const;

   
	Vector3d samplePhase_ss(int id, const Vector3d &wi,
							Sampler &sampler) const;
	
	Vector3d samplePhase_full(int id, const Vector3d &wi,
							  Sampler &sampler) const;

	inline Array3d getAlbedo_ss(int id) const {
		assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
		return Array3d(double(m_albedo[id*3])/255.0,
					   double(m_albedo[id*3+1])/255.0,
					   double(m_albedo[id*3+2])/255.0);
	};
	
	inline void writeAlbedo_ss(int id, Array3d a) {
		assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
		m_albedo[id*3] = (unsigned char)(a(0)*255.0);
		m_albedo[id*3+1] = (unsigned char)(a(1)*255.0);
		m_albedo[id*3+2] = (unsigned char)(a(2)*255.0);
	};

	inline void writeAlbedo_ms(int id, Array3d a) {
		assert(0 && "not implemented");
	};

	void writeTrigLobe(int id, const TrigLobe &tl) {
		assert(0 && "not implemented");
	};
	
	void writeShadowing(int id, const Shadowing &sh) {
		assert(0 && "not implemented");
	};

	void writeShadowing(int id, double sh) {
		assert(0 && "not implemented");
	};

	

	SGGX getSGGX(int id) const;

	void writeSGGX(int id, const SGGX &sggx);
	
	/**
	 * Estimate effective multiple scattering albedo with
	 * brute force local ray casting.
	 */
	bool estimateMultScat(const Box3 &box, Sampler &sampler,
						  Array3d &mult_scatt,
						  int nb_samples,
						  int max_loop) const;

	void writeVoidVoxel(int id);

	
	void write();

	
  protected:
	
	void setPaths(std::string dest, std::string sufix);


	
	/**
	 * Get linearly prefiltered values of a cube of sub-voxels. 
	 * Provide prefiltered albedos, SGGX distributions and
	 * density, as well as transparency along X, Y and Z axes.
	 * @warning doesn't return a normalized SGGX (for perfs and accuracy,
	 * it is better to do it after the recursive downsampling).
	 * @param level lod level (0 = input resolution)
	 * @return struct with prefiltered values
	 */
	Voxel downsampleVoxel(int x, int y, int z, int level) const;
	
	std::string m_albedo_path;
	std::string m_sggx_sigma_path;
    std::string m_sggx_r_path;

    unsigned char *m_albedo;	
    unsigned char *m_sggx_sigma;
    unsigned char *m_sggx_r;

};



#endif // SGGX_VOLUME_H
