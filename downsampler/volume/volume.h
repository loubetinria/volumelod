#ifndef VOLUME_H
#define VOLUME_H

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <Eigen/Core>

#include "../sggx/sggx.h"
#include "../utils/ray.h"
#include "../utils/box3.h"


enum EMethod {
    EShadowing,
    EShadowingIso,
    EShadowingOpt,
    EMeanOcclusion,
	ENaive,
	EShadowingSGGX,
	EShadowingSGGXOpt
};


struct Voxel {
	double density;
	int non_empty_voxels;
	Array3d transp;
	Array3d albedo;
	SGGX sggx;
};

struct Segment {
	double dist;
	double sigma_t;
	Array3d sigma_s;
	int id;
Segment(double dist, double sigma_t, Array3d sigma_s, int id)
: dist(dist), sigma_t(sigma_t), sigma_s(sigma_s), id(id) {};

};

class VolumeTest;

class Volume {

    friend class VolumeTest;
	
  public:
	
	// Constructor
	Volume();

	// Destructor
	virtual ~Volume();	

	inline Array3i getGrid() const {return m_grid;};
   
	inline int getLoD() const {return m_lod;};
	
	inline Array3d getMin() const {return m_min;};
	
	inline Array3d getMax() const {return m_max;};
	
	inline Array3d getVSize() const {return m_vsize;};

	int getId(int x, int y, int z) const;
	
	int getId(const Array3i &xyz) const;

	bool getVoxelId(Array3d pos, Array3i &next_voxel_id) const;

	Array3d getVoxelSize() const;
	
	inline void writeDensity(int id, float value) { m_density[id] = value;};

	// pure virtual getters and setters
	
	///  Return attenuation coefficient ( = density x sggx.sigma )
	virtual double getSigma_t(int id, const Vector3d &w) const = 0;

	virtual Array3d getSigma_ss(int id, const Vector3d &w) const = 0;

	virtual Vector3d samplePhase_ss(int id, const Vector3d &wi,
									Sampler &sampler) const = 0;
	
	virtual Vector3d samplePhase_full(int id, const Vector3d &wi,
									  Sampler &sampler) const = 0;

	virtual Array3d getAlbedo_ss(int id) const = 0;
	
	virtual void writeAlbedo_ss(int id, Array3d a) = 0;
	
	virtual void writeAlbedo_ms(int id, Array3d a) = 0;

	virtual void writeTrigLobe(int id, const TrigLobe &tl) = 0;
	
	virtual void writeShadowing(int id, const Shadowing &sh) = 0;
	
	virtual void writeShadowing(int id, double sh) = 0;

	virtual void writeSGGX(int id, const SGGX &sggx) = 0;
	
	virtual bool estimateMultScat(const Box3 &box, Sampler &sampler,
								  Array3d &mult_scatt,
								  int nb_samples,
								  int max_loop) const = 0;

	virtual void writeVoidVoxel(int id) = 0;
	
	virtual void write() = 0;

	// estimations
	
	bool estimateSingleScat(const Box3 &box, Sampler &sampler,
							Array3d &single_scatt,
							Array3d &mean_ss_albedo,
							int nb_samples_sh_estimate,
							bool includeShadowing,
							bool randomizeDir) const;

	bool estimateScatProb(const Box3 &box, Sampler &sampler,
						  std::vector<double> &scat_prob,
						  int nb_samples) const;
	
	/*
	 * Return true if sampling is successful, and val_pdf which is the amount
	 * of scattering at the sampled position (attenuation x scattering) divided
	 * by the pdf of the sampling method.
	 * val_pdf should be 1 for media with uniform single_s (ie. uniform albedo).
	 */
	bool sampleScattering(Ray &ray,
						  Sampler &sampler,
						  Vector3d &position,
						  int &id,
						  Array3d &val_pdf,
						  bool singleScattering) const;
	
	bool integrate(Ray &ray, double &transp) const;
	
	bool rayCubeIntersect(Array3i voxel_id, Ray &ray,
						  double &d, Array3i &next_voxel_id) const;


	
  protected:

	void assert_goodId(int id) const;
	
	// Base data
	std::string m_src;
	std::string m_density_path;

	float *m_density;

	Array3i m_grid;
	int m_lod;
	Array3d m_min;
	Array3d m_max;
	Array3d m_vsize;
};



#endif // VOLUME_H
