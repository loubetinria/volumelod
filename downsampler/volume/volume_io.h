#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>

#include <iostream>

#include <Eigen/Core>

void readFloatFile(const char * file, float **data,
				   Eigen::Array3i &grid,
                   Eigen::Array3d &min,
                   Eigen::Array3d &max);


void readChar3File(const char * file, unsigned char **data,
				   Eigen::Array3i &grid,
				   Eigen::Array3d &min,
				   Eigen::Array3d &max);

void writeChar3File(const char * file, unsigned char **data,
					const Eigen::Array3i &grid,
                    const Eigen::Array3d &min,
                    const Eigen::Array3d &max);

void writeFloatFile(const char * file, float **data,
					const Eigen::Array3i &grid,
                    const Eigen::Array3d &min,
                    const Eigen::Array3d &max);

void writeFloat3File(const char * file, float **data,
					 const Eigen::Array3i &grid,
					 const Eigen::Array3d &min,
					 const Eigen::Array3d &max);

