#include "sggx_volume.h"
#include "sggx_sh_volume.h"
#include "volume_io.h"
#include "../utils/utils.h"
#include "../triglobes/trigphase.h"
#include "../utils/shadowing.h"
#include "../utils/brent.h"

#include <boost/thread.hpp>

#include <cmath> // isnan

// in estimateShadowing
#define MAX_THREADS 32

/**
 * Helper for optimizing multiple scattering albedo.
 * @param x multiple scattering albedo in the downsampled volume
 * @param ss single local scattering albedo in the downsampled volume
 * @param sh estimation of mean local shadowing in the downsampled volume
 * @param target target macroscopic multiple scattering albedo
 * @param vec probability of leaving the downsampled volume for each number of 
 * bounces.
 * @return an estimation of the error on multiple scattering in the downsampled 
 * volume for x
 */ 
double compute_ms(double x, double ss, double sh, double target,
				  const std::vector<double> &vec) {

	// Approximate local albedo (single scattering + local multiple scattering)
	double albedo = sh*ss+(1-sh)*x;

	double res = 0;
	double cur = albedo;

	// estimate multiple scattering in the downsampled volume from
	// probability of each number of bounces
	for (unsigned int i = 0; i < vec.size(); i++) {
		res += cur * vec[i];
		cur *= albedo;
	}
	return res-target;
}


SGGXVolume::SGGXVolume() : Volume() {
	m_sggx_sigma = NULL;
	m_sggx_r = NULL;
}

SGGXVolume::SGGXVolume(std::string src) {

	std::cout << "Creating SGGX volume from src " << src << std::endl;
	
	// Init
	m_src = src;
	m_density_path = src + "density_0.mtsvol";
	m_albedo_path = src + "albedo_0.mtsvol";
	m_sggx_sigma_path = src + "sggx_sigma_0.mtsvol";
	m_sggx_r_path = src + "sggx_r_0.mtsvol";
	
	m_density = NULL;
	m_albedo = NULL;
	m_sggx_sigma = NULL;
	m_sggx_r = NULL;
	m_lod = 0;

	// Read data

    /* DENSITY */
	std::cout << "    reading " << m_density_path << std::endl;
	readFloatFile(m_density_path.c_str(), &m_density, m_grid, m_min, m_max);
	std::cout << "    density grid : " << to_string(m_grid) << std::endl;
	std::cout << "    density box min: " << to_string(m_min) << std::endl;
	std::cout << "    density box max: " << to_string(m_max) << std::endl;

	m_vsize = getVoxelSize();
	
	Array3i grid;
	Array3d min, max;
	
    /* ALBEDO */
  	std::cout << "    reading " << m_albedo_path << std::endl;
	readChar3File(m_albedo_path.c_str(), &m_albedo, grid, min, max);
	std::cout << "    albedo grid : " << to_string(grid) << std::endl;
	std::cout << "    density box min: " << to_string(min) << std::endl;
	std::cout << "    density box max: " << to_string(max) << std::endl;

	assert("check grid resolution"
		   && is_equal(grid, m_grid) && is_equal(min, m_min) && is_equal(max, m_max));
	
    /* SGGX */
	std::cout << "    reading " << m_sggx_sigma_path << std::endl;
	readChar3File(m_sggx_sigma_path.c_str(), &m_sggx_sigma, grid, min, max);
	std::cout << "    sggx sigma grid : " << to_string(grid) << std::endl;
	std::cout << "    density box min: " << to_string(min) << std::endl;
	std::cout << "    density box max: " << to_string(max) << std::endl;

	assert("check grid resolution"
		   && is_equal(grid, m_grid) && is_equal(min, m_min) && is_equal(max, m_max));
	
	std::cout << "    reading " << m_sggx_r_path << std::endl;
	readChar3File(m_sggx_r_path.c_str(), &m_sggx_r, grid, min, max);
	std::cout << "    sggx r grid : " << to_string(grid) << std::endl;
	std::cout << "    density box min: " << to_string(min) << std::endl;
	std::cout << "    density box max: " << to_string(max) << std::endl;

	assert("check grid resolution"
		   && is_equal(grid, m_grid) && is_equal(min, m_min) && is_equal(max, m_max));
	
	std::cout << "SGGXVolume OK!" << std::endl;
}

SGGXVolume::SGGXVolume(const SGGXVolume *sggxVol, int level,
					   std::string dest,
					   std::string suffix) : Volume() {

	double factor = std::pow(2.0, level);
	Array3i sggxVol_grid = sggxVol->getGrid();
	m_grid(0) = int(std::ceil(double(sggxVol_grid(0))/factor));
	m_grid(1) = int(std::ceil(double(sggxVol_grid(1))/factor));
	m_grid(2) = int(std::ceil(double(sggxVol_grid(2))/factor));
	m_lod = sggxVol->getLoD()+level;
	
	// divide voxel size by pow
	Array3d new_vs = sggxVol->getVoxelSize() * factor;		
	std::cout << "new_vs " << to_string(new_vs) << std::endl;
	m_min = sggxVol->getMin();
	m_max = m_min;
	m_max(0) += new_vs(0)*m_grid(0);
	m_max(1) += new_vs(1)*m_grid(1);
	m_max(2) += new_vs(2)*m_grid(2);

	m_vsize = getVoxelSize();
	std::cout << "m_vsize " << to_string(m_vsize) << std::endl;

	std::cout << "Downsampling ("<< suffix <<", rec) volume " << to_string(sggxVol_grid)
			  << " to LoD " << sggxVol->getLoD()+level << std::endl;
	
	std::cout << "New bbox: " << to_string(m_min)
			  << " " << to_string(m_max) << std::endl;

	std::cout << "New size: " << to_string(m_grid) << std::endl;
	
	// set paths
	setPaths(dest, suffix);
	
	// alloc volumes
	int new_size = m_grid(0) * m_grid(1) * m_grid(2);
	
	m_density = new float[new_size];
	m_albedo = new unsigned char[new_size*3];

	m_sggx_sigma = new unsigned char[new_size*3];
	m_sggx_r = new unsigned char[new_size*3];
	
	// init new volume with 0 in order to avoid
	// error when estimating the scattering in the new volume
	for (int x = 0; x < m_grid(0); x++) {
		for (int y = 0; y < m_grid(1); y++) {
			for (int z = 0; z < m_grid(2); z++) {
				int id_new = getId(x, y, z);

				m_density[id_new] = 0;
				m_albedo[id_new*3] = 0;
				m_albedo[id_new*3+1] = 0;
				m_albedo[id_new*3+2] = 0;

				m_sggx_sigma[id_new*3] = 255;
				m_sggx_sigma[id_new*3+1] = 255;
				m_sggx_sigma[id_new*3+2] = 255;

				m_sggx_r[id_new*3] = 127;
				m_sggx_r[id_new*3+1] = 127;
				m_sggx_r[id_new*3+2] = 127;
			}
		}
	}
}

void SGGXVolume::setPaths(std::string dest, std::string sufix) {
	std::string s_end = "_" + to_string(m_lod) + "_" + sufix + ".mtsvol";
	m_density_path = dest + "density" + s_end;
	m_albedo_path = dest + "albedo" + s_end;
	m_sggx_sigma_path = dest + "sggx_sigma" + s_end;
	m_sggx_r_path = dest + "sggx_r" + s_end;
}


SGGXVolume::~SGGXVolume() {

	if (m_density != NULL)
		delete [] m_density;

	if (m_albedo != NULL)
		delete [] m_albedo;
	
	if (m_sggx_sigma != NULL)
		delete [] m_sggx_sigma;

	if (m_sggx_r != NULL)
		delete [] m_sggx_r;
}

SGGX SGGXVolume::getSGGX(int id) const {
	assert_goodId(id);
	double sigma_x = double(m_sggx_sigma[id*3])/255.0;
	double sigma_y = double(m_sggx_sigma[id*3+1])/255.0;
	double sigma_z = double(m_sggx_sigma[id*3+2])/255.0;
	double r_xy = (double(m_sggx_r[id*3])/255.0*2.0-1.0);
	double r_xz = (double(m_sggx_r[id*3+1])/255.0*2.0-1.0);
	double r_yz = (double(m_sggx_r[id*3+2])/255.0*2.0-1.0);
	SGGX res(Vector3d(sigma_x*sigma_x,
					  sigma_y*sigma_y,
					  sigma_z*sigma_z),
			 Vector3d(r_xy*sigma_x*sigma_y,
					  r_xz*sigma_x*sigma_z,
					  r_yz*sigma_y*sigma_z));
	return res;
}

void SGGXVolume::writeSGGX(int id, const SGGX &sggx) {
	Vector3d sigma, r;
	sggx.toSigmaR(sigma, r);
	m_sggx_sigma[id*3] = (unsigned char)(sigma(0)*255.0);
	m_sggx_sigma[id*3+1] = (unsigned char)(sigma(1)*255.0);
	m_sggx_sigma[id*3+2] = (unsigned char)(sigma(2)*255.0);
	m_sggx_r[id*3] = (unsigned char)((r(0)*0.5+0.5)*255.0);
	m_sggx_r[id*3+1] = (unsigned char)((r(1)*0.5+0.5)*255.0);
	m_sggx_r[id*3+2] = (unsigned char)((r(2)*0.5+0.5)*255.0);
}

double SGGXVolume::getSigma_t(int id, const Vector3d &w) const {
	assert_goodId(id);
	double density = double(m_density[id]);
	if (density > 0) {
		SGGX sggx = getSGGX(id);
		density *= sggx.sigma(w);
	}
	return density;
}

Array3d SGGXVolume::getSigma_ss(int id, const Vector3d &w) const {
	return getSigma_t(id, w)*getAlbedo_ss(id);
}


Voxel SGGXVolume::downsampleVoxel(int x, int y, int z, int level) const {

	// if level 0, just give the data, handle out-of-grid voxels
	if (level == 0) {
		bool inGrid = (x >= 0) && (x < m_grid(0))
			&& (y >= 0) && (y < m_grid(1))
			&& (z >= 0) && (z < m_grid(2));
		
		if (inGrid) {
			int id = getId(x,y,z);
			Voxel voxel;
			voxel.density = m_density[getId(x,y,z)];
			if (voxel.density > 0) {
				voxel.non_empty_voxels = 1;
				// compute transparency for non empty voxels only
				// avoid get sigma if axis aligned: 
				double sigma_x = double(m_sggx_sigma[id*3])/255.0;
				double sigma_y = double(m_sggx_sigma[id*3+1])/255.0;
				double sigma_z = double(m_sggx_sigma[id*3+2])/255.0;
				voxel.transp(0) = std::exp(-m_vsize(0)*sigma_x*voxel.density);
				voxel.transp(1) = std::exp(-m_vsize(1)*sigma_y*voxel.density);
				voxel.transp(2) = std::exp(-m_vsize(2)*sigma_z*voxel.density);
				voxel.sggx = getSGGX(id);				
			} else {
				voxel.non_empty_voxels = 0;
				// void
				voxel.transp(0) = 1.0;
				voxel.transp(1) = 1.0;
				voxel.transp(2) = 1.0;
				voxel.sggx = SGGX(Vector3d(1,1,1), Vector3d(0,0,0));
			}
			voxel.albedo = getAlbedo_ss(id);
			return voxel;
		} else {
			Voxel voxel;
			voxel.density = 0.0;
			voxel.non_empty_voxels = 0;
			voxel.transp = Array3d(1.0,1.0,1.0);
			voxel.albedo = Array3d(0.0,0.0,0.0);
			voxel.sggx = SGGX(Vector3d(1,1,1), Vector3d(0,0,0));
			return voxel;
		}
	}
	
	// get 8 subvoxels and downsample
	// coords: x*2, y*2, z*2
	int x2 = x*2, y2 = y*2, z2 = z*2;
	Voxel voxels[8];
	voxels[0] = downsampleVoxel(x2,   y2,   z2,   level-1);
	voxels[1] = downsampleVoxel(x2+1, y2,   z2,   level-1);
	voxels[2] = downsampleVoxel(x2,   y2+1, z2,   level-1);
	voxels[3] = downsampleVoxel(x2+1, y2+1, z2,   level-1);
	voxels[4] = downsampleVoxel(x2,   y2,   z2+1, level-1);
	voxels[5] = downsampleVoxel(x2+1, y2,   z2+1, level-1);
	voxels[6] = downsampleVoxel(x2,   y2+1, z2+1, level-1);
	voxels[7] = downsampleVoxel(x2+1, y2+1, z2+1, level-1);
	double weights[8];
	double weight_sum = 0;
	for (int i = 0; i < 8; i++) {
		weights[i] = voxels[i].density;
		weight_sum += weights[i];
	}

	Voxel result;
	result.density = 0.0;
	result.albedo = Array3d(0.0,0.0,0.0);
	result.sggx = SGGX(Vector3d(0.0,0.0,0.0),
					   Vector3d(0.0,0.0,0.0));

	result.non_empty_voxels = 0;
	for (int i = 0; i < 8; i++) {
		result.non_empty_voxels += voxels[i].non_empty_voxels;
	}
	
	if (weight_sum > 0) {	
		for (int i = 0; i < 8; i++) {
			weights[i] /= weight_sum;
			result.density  += 0.125*voxels[i].density;
			result.albedo += weights[i]*voxels[i].albedo;
			result.sggx     += weights[i]*voxels[i].sggx;
		}

		// transparency : multiply and average
		result.transp(0) = 0.25*(voxels[0].transp(0)*voxels[1].transp(0)
								 + voxels[2].transp(0)*voxels[3].transp(0)
								 + voxels[4].transp(0)*voxels[5].transp(0)
								 + voxels[6].transp(0)*voxels[7].transp(0));
		result.transp(1) = 0.25*(voxels[0].transp(1)*voxels[2].transp(1)
								 + voxels[1].transp(1)*voxels[3].transp(1)
								 + voxels[4].transp(1)*voxels[6].transp(1)
								 + voxels[5].transp(1)*voxels[7].transp(1));
		result.transp(2) = 0.25*(voxels[0].transp(2)*voxels[4].transp(2)
								 + voxels[1].transp(2)*voxels[5].transp(2)
								 + voxels[2].transp(2)*voxels[6].transp(2)
								 + voxels[3].transp(2)*voxels[7].transp(2));

	} else {
		// void, no density, cannot do mean, set sggx to Id
		// to avoid nans.
		result.transp(0) = 1.0;
		result.transp(1) = 1.0;
		result.transp(2) = 1.0;
		result.sggx = SGGX(Vector3d(1,1,1), Vector3d(0,0,0));
	}

	return result;
}

bool usingSGGX(EMethod method) {
	return (method == EShadowingSGGX
			|| method == EShadowingSGGXOpt
			|| method == ENaive);
}

bool usingAnisotropicSh(EMethod method) {
	return (method == EShadowing || method == EShadowingOpt);
}

bool usingOpt(EMethod method) {
	return (method == EShadowingOpt
			|| method == EShadowingSGGXOpt
			|| method == EShadowing
			|| method == EShadowingIso);
}


void SGGXVolume::downsample_task(int level, int x_begin, int x_end,
								 Volume* new_volume, int nb_samples_sh,
								 EMethod method) const {
	
	std::random_device r;
	Sampler sampler(r());
	
	// Downsampling
	Array3i newGrid = new_volume->getGrid();
	for (int x = x_begin; x <= x_end; x++) {
		for (int y = 0; y < newGrid(1); y++) {
			for (int z = 0; z < newGrid(2); z++) {

				// Get linear prefiltering of sub-voxels and transparency
				Voxel voxel = downsampleVoxel(x, y, z, new_volume->getLoD());

				// normalize pre-filtered SGGX flakes distribution
				// (not before in order to avoid pre-filtering errors)
				if (voxel.density > 0)
					voxel.sggx.normalize(); 

				if (voxel.transp(0) <= 0.f
					|| voxel.transp(1) <= 0.f
					|| voxel.transp(2) <= 0.f) {
					std::cout << "Problem, transparency null, density too high?"
							  << std::endl;
					assert(0);
				}
				
				//
				// 2) set pre-filtered specular albedos
				//

				int id_new = new_volume->getId(x, y, z);

				// albedo
				new_volume->writeAlbedo_ss(id_new, voxel.albedo);	
				
				// density = mean density
				new_volume->writeDensity(id_new, voxel.density);

				if (method != ENaive)
					new_volume->writeAlbedo_ms(id_new, voxel.albedo);

				if (voxel.density == 0.0) {
					new_volume->writeVoidVoxel(id_new);
				} else {

					if (method == ENaive) {
						new_volume->writeSGGX(id_new, voxel.sggx);
						// End of job for naive
						continue;
					}
					
					//
					// 3) compute box and estimate shadowing
					//
					Array3d new_vsize = new_volume->getVSize();
					
					double sigma_target_X = -std::log(voxel.transp(0)) / (new_vsize(0));
					double sigma_target_Y = -std::log(voxel.transp(1)) / (new_vsize(1));
					double sigma_target_Z = -std::log(voxel.transp(2)) / (new_vsize(2));

					// set pre-filtered SGGX (flakes distribution)
					SGGX sggx_flakes_mean = voxel.sggx; // normalized
					//voxel.sggx.print_eigenv();
					
					Vector3d wX(1.0,0.0,0.0);
					Vector3d wY(0.0,1.0,0.0);
					Vector3d wZ(0.0,0.0,1.0);
					double sigma_flakes_X, sigma_flakes_Y, sigma_flakes_Z;

					TrigLobe trigLobe;
					
					if (usingSGGX(method)) {
						new_volume->writeSGGX(id_new, sggx_flakes_mean);
						sigma_flakes_X = sggx_flakes_mean.sigma(wX);
						sigma_flakes_Y = sggx_flakes_mean.sigma(wY);
						sigma_flakes_Z = sggx_flakes_mean.sigma(wZ);
					} else {
						trigLobe = sggx_flakes_mean.toTrigLobe();					
						new_volume->writeTrigLobe(id_new, trigLobe);
			
						// convert to triglobe
						Shadowing no_sh(1, 1, 1,
										Vector3d(1.0,0.0,0.0),
										Vector3d(0.0,1.0,0.0),
										Vector3d(0.0,0.0,1.0));

						TrigPhase trigPhase(trigLobe, no_sh);
					
						// 3) find density so that rho * sigma_flakes >= sigma_target,
						//    in X, Y,
						sigma_flakes_X = trigPhase.sigma_t(wX);
						sigma_flakes_Y = trigPhase.sigma_t(wY);
						sigma_flakes_Z = trigPhase.sigma_t(wZ);
					}

					double rho, sigma_sh_X, sigma_sh_Y, sigma_sh_Z;
					if (usingAnisotropicSh(method)) {
						rho = std::max(sigma_target_X/sigma_flakes_X,
									   std::max(sigma_target_Y/sigma_flakes_Y,
												sigma_target_Z/sigma_flakes_Z));
						sigma_sh_X = sigma_target_X/(rho * sigma_flakes_X);
						sigma_sh_Y = sigma_target_Y/(rho * sigma_flakes_Y);
						sigma_sh_Z = sigma_target_Z/(rho * sigma_flakes_Z);

						// avoid problems due to roundoff errors
						sigma_sh_X = std::min(1.0, sigma_sh_X);
						sigma_sh_Y = std::min(1.0, sigma_sh_Y);
						sigma_sh_Z = std::min(1.0, sigma_sh_Z);
					} else {
						rho = 1.0/3.0*(sigma_target_X/sigma_flakes_X
									   + sigma_target_Y/sigma_flakes_Y
									   + sigma_target_Z/sigma_flakes_Z);
						sigma_sh_X = 1.0;
						sigma_sh_Y = 1.0;
						sigma_sh_Z = 1.0;
					}

					new_volume->writeDensity(id_new, rho);
					Shadowing sh;
					if (!usingSGGX(method)) {
						sh = Shadowing(sigma_sh_X, sigma_sh_Y, sigma_sh_Z,
									   Vector3d(1.0,0.0,0.0),
									   Vector3d(0.0,1.0,0.0),
									   Vector3d(0.0,0.0,1.0));
						new_volume->writeShadowing(id_new, sh);
					}

					if (method == EMeanOcclusion) {
						// nothing else to do
						continue;
					}

					// Set box
					Array3d vmin(new_volume->getMin() + new_vsize*Array3d(x,y,z));
					Array3d vmax = vmin + new_vsize*Array3d(1,1,1);
					Array3d vmax_clamp(std::min(vmax(0), m_max(0)),
									   std::min(vmax(1), m_max(1)),
									   std::min(vmax(2), m_max(2))); // clamp at the old box
					Box3 box(vmin, vmax_clamp);

					// Estimate single scattering in input volume
					
					Array3d sh_estimation;
					Array3d mean_ss_albedo;
					// Estimate mean transparency after single scattering in the input volume
					bool success_sh_estimation = estimateSingleScat(box, sampler,
																	sh_estimation,
																	mean_ss_albedo,
																	nb_samples_sh,
																	true, false);
					// replace linearly filterer albedo, take into account some correlations
					//new_volume->writeAlbedo_ss(id_new, mean_ss_albedo); 

					// estimate single scattering in downsampled volume
					Box3 box2(vmin, vmax);
					Array3d sh_estimation_new;
					Array3d mean_ss_albedo_new;
					// mean transparency after single scattering in the new volume
					bool success_sh_estimation_new =
						new_volume->estimateSingleScat(box2, sampler, sh_estimation_new,
													   mean_ss_albedo_new, nb_samples_sh,
													   true, false);
					
					// Choice: sh estimations are colors
					double A = (success_sh_estimation && success_sh_estimation_new)
						? dot(sh_estimation_new, sh_estimation)/dot(sh_estimation_new, sh_estimation_new)
						: 1.0;

					
					if (A == 0.0 || std::isinf(A) || std::isnan(A)) {
						A = 1.0;						
					}
					
					A = std::min(A, 1.0); // clamp to avoid problems with char representation
					A = std::max(A, 0.1); // clamp to avoid problems?

					Shadowing new_sh;
					
					if (method == EShadowingSGGX || method == EShadowingSGGXOpt) {
						new_volume->writeShadowing(id_new, A);
					} else {
						assert(!usingSGGX(method) && "method here should use trig");
						new_volume->writeDensity(id_new, rho / A);
						Matrix3d new_SA = sh.m_SA * A;					
						new_sh = Shadowing(new_SA);
						new_volume->writeShadowing(id_new, new_sh);
					}
	  
					// we should have the same sigma_t
					// error because of how the triglobe is encoded, or shadowing
					if (usingAnisotropicSh(method)) {
						assert_almost_eq(sigma_target_X,
										 new_volume->getSigma_t(id_new, wX), rho*0.02,
										 DEBUG_INFO);
						assert_almost_eq(sigma_target_Y,
										 new_volume->getSigma_t(id_new, wY), rho*0.02,
										 DEBUG_INFO);
						assert_almost_eq(sigma_target_Z,
										 new_volume->getSigma_t(id_new, wZ), rho*0.02,
										 DEBUG_INFO);
					}
					
					double Pe; // mean probability of LOCAL single scattering (no self-sh)
					if (method == EShadowingSGGX || method == EShadowingSGGXOpt) {
						Pe = A;
					} else {
						TrigPhase tp(trigLobe, new_sh);
						Pe = tp.int_sigma_ss() / tp.int_sigma_t();
					}

					if (!usingOpt(method)) {
						std::cout << "Deprecated" << std::endl;
						assert(0);
					} else {
						// estimate with brute force
						Array3d albedo_ms_opt = voxel.albedo; // default value
						Array3d msEst;
						bool success_ms = estimateMultScat(box, sampler, msEst,
														   nb_samples_sh, 10000);
						if (success_ms) {
							//std::cout << "estimate old: " << to_string(estimated_C) << std::endl;
							std::vector<double> scat_prob;
							bool sp_success = new_volume->estimateScatProb(box2, sampler,
																		   scat_prob,
																		   nb_samples_sh);
							if (sp_success) {
								using std::placeholders::_1;
								std::function<double(double)> f0 = std::bind( &compute_ms, _1,
																			  voxel.albedo(0),
																			  Pe,
																			  msEst(0),
																			  scat_prob);
								std::function<double(double)> f1 = std::bind( &compute_ms, _1,
																			  voxel.albedo(1),
																			  Pe,
																			  msEst(1),
																			  scat_prob);
								std::function<double(double)> f2 = std::bind( &compute_ms, _1,
																			  voxel.albedo(2),
																			  Pe,
																			  msEst(2),
																			  scat_prob);

								double root0 = 0, root1 = 0, root2 = 0;

								if (f0(0)*f0(1) < 0) {
									root0 = brents_fun(f0, 0.0, 1.0, 1e-5, 100);
								} else {
									//std::cout << "could not optimize root 0" << std::endl;
									root0 = 0.05;
								}

							
								if (f1(0)*f1(1) < 0) {
									root1 = brents_fun(f1, 0.0, 1.0, 1e-5, 100);
								} else {
									//std::cout << "could not optimize root 1" << std::endl;
									root1 = 0.05;
								}

							
								if (f2(0)*f2(1) < 0) {
									root2 = brents_fun(f2, 0.0, 1.0, 1e-5, 100);
								} else {
									//std::cout << "could not optimize root 2" << std::endl;
									root2 = 0.05;
								}
							
								albedo_ms_opt(0) = std::max(root0, 0.005);
								albedo_ms_opt(1) = std::max(root1, 0.005);
								albedo_ms_opt(2) = std::max(root2, 0.005);
								new_volume->writeAlbedo_ms(id_new, albedo_ms_opt);

								// evaluation of the solution?
								// compare msEst with the estimated multiple scattering albedo new_volume->estimateMultScat

								//std::cout << "msEst: " << to_string(msEst) << std::endl;
								Array3d msRes;
								new_volume->estimateMultScat(box, sampler, msRes,
															 nb_samples_sh, 10000);
								//std::cout << "msRes: " << to_string(msRes) << std::endl;

								Array3d relError = (msRes-msEst)/msEst;
								//std::cout << "relError: " << to_string(relError) << std::endl;
								//printf("%f %f %f\n", relError(0), relError(1), relError(2));
							} else {
								// std::cout << "SGGXVolume: could not estimate scatt probs."
								// 		  << std::endl
								// 		  << " Too few samples?" << std::endl;
							}
						} else {
							// std::cout << "SGGXVolume: could not estimate mult scatt."
							// 			  << std::endl
							// 			  << " Too few samples?" << std::endl;
						}
						new_volume->writeAlbedo_ms(id_new, albedo_ms_opt);
					}
				}
			}	
		}
	}
	
	std::cout << "End of job: " << x_begin << "-" << x_end << std::endl;
}

Volume* SGGXVolume::downsample(int level, std::string dest,
							   int nb_samples_sh,
							   EMethod method,
							   bool freeResult) const {
	
	TrigVolume* new_volume_tv = NULL;
	SGGXVolume* new_volume_sggx = NULL;
	SGGXSHVolume* new_volume_sggx_sh = NULL;
	Array3i newGrid;
	
	std::string postfix;
	if (method == EShadowing) {
		postfix = "shadowing";
	} else if (method == EShadowingIso) {
		postfix = "iso_shadowing";
	} else if (method == EShadowingOpt) {
		postfix = "opt_shadowing";
	} else if (method == EMeanOcclusion) {
		postfix = "mean_occlusion";
	} else if (method == ENaive) {
		postfix = "naive";
	} else if (method == EShadowingSGGX) {
		postfix = "shadowing_sggx";
	} else if (method == EShadowingSGGXOpt) {
		postfix = "shadowing_sggx_opt";
	} else {
		assert(0 && "unknown method");
	}

	if (!usingSGGX(method)) {
		new_volume_tv = new TrigVolume(this, level, dest, postfix);
		newGrid = new_volume_tv->getGrid();
	} else if (method == ENaive) {
		new_volume_sggx = new SGGXVolume(this, level, dest, postfix);
		newGrid = new_volume_sggx->getGrid();
	} else {
		new_volume_sggx_sh = new SGGXSHVolume(this, level, dest, postfix);
		newGrid = new_volume_sggx_sh->getGrid();
	}

	int nthreads = int(boost::thread::hardware_concurrency());
	nthreads = std::min(nthreads, newGrid(0));
	nthreads = std::min(nthreads, MAX_THREADS);	

	std::cout << "Using " << nthreads << " threads " << std::endl;
	
	int x_by_th = newGrid(0) / nthreads;
	int rest = newGrid(0) - x_by_th*nthreads;

	boost::thread_group group;

	for(int j = 0; j < nthreads; j++) {
		// if j < rest:
		int x_begin = 0, x_end = 0;
		if (j < rest) {
			x_begin = (x_by_th+1)*j;
			x_end = x_begin+x_by_th;
		} else {
			x_begin = x_by_th*j+rest;
			x_end = x_begin+x_by_th-1;
		}
		std::cout << "Thread " << j << " takes " << x_begin << " to " << x_end
				  << " (" << x_end - x_begin+1 << ")"<< std::endl;
		
		if (!usingSGGX(method)) {
			group.create_thread(boost::bind(&SGGXVolume::downsample_task,
											this,
											level, x_begin, x_end,
											(Volume *)new_volume_tv, nb_samples_sh, method));
		} else if (method == ENaive) {
			group.create_thread(boost::bind(&SGGXVolume::downsample_task,
											this,
											level, x_begin, x_end,
											(Volume *)new_volume_sggx, nb_samples_sh, method));
		} else {
			group.create_thread(boost::bind(&SGGXVolume::downsample_task,
											this,
											level, x_begin, x_end,
											(Volume *)new_volume_sggx_sh, nb_samples_sh, method));
		}
    }
    group.join_all();

	std::cout << "Done, new res: " << to_string(newGrid) << std::endl;

	if (!usingSGGX(method)) {
		new_volume_tv->write();
	} else if (method == ENaive) {
		new_volume_sggx->write();
	} else {
		new_volume_sggx_sh->write();
	}
	
	if (freeResult) {
		if (!usingSGGX(method)) {
			delete new_volume_tv;
		} else if (method == ENaive) {
			delete new_volume_sggx;
		} else {
			delete new_volume_sggx_sh;
		}
		return NULL;
	}

	if (!usingSGGX(method)) {
		return new_volume_tv;
	} else if (method == ENaive) {
		return new_volume_sggx;
	} else {
		return new_volume_sggx_sh;
	}
}


/**
 * Sample SGGX phase function in the voxel id.
 * Assume that sampling is perfect (it is the case for SGGX)
 * Assume that wi = -ray.dir!
 */
Vector3d SGGXVolume::samplePhase_ss(int id, const Vector3d &wi, Sampler &sampler) const {
	SGGX sggx = getSGGX(id);
	Vector3d wo;
	double pdf;
	sggx.sample(wi, wo, pdf, sampler);	
	return wo;
}

/**
 * Assume that wi = -ray.dir!
 */
Vector3d SGGXVolume::samplePhase_full(int id, const Vector3d &wi, Sampler &sampler) const {
	return samplePhase_ss(id, wi, sampler);
}



void SGGXVolume::write() {
	// Assume that paths are set
	
	std::cout << "Writing " << m_density_path << std::endl;
	writeFloatFile(m_density_path.c_str(), &m_density,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_albedo_path << std::endl;
	writeChar3File(m_albedo_path.c_str(), &m_albedo,
				   m_grid, m_min, m_max);
	
	std::cout << "Writing " << m_sggx_sigma_path << std::endl;
	writeChar3File(m_sggx_sigma_path.c_str(), &m_sggx_sigma,
				   m_grid, m_min, m_max);

	std::cout << "Writing " << m_sggx_r_path << std::endl;
	writeChar3File(m_sggx_r_path.c_str(), &m_sggx_r,
				   m_grid, m_min, m_max);
}


/**
 * Estimate the mean ms albedo (= ray throughput) in the volume. This 
 * implementation casts rays and accumulates throughputs.
 */
bool  SGGXVolume::estimateMultScat(const Box3 &box, Sampler &sampler,
								   Array3d &mult_scatt,
								   int nb_samples,
								   int max_loop) const {
	
	std::uniform_real_distribution<double> uniform_dist(0, 1);
	
	mult_scatt = Array3d(0.0,0.0,0.0);
	int n_samples_ms = 0;
	
	for (int i = 0; i < nb_samples; i++) {
		
		Ray r = box.sampleRay(sampler, false);
		Vector3d dir = r.d;
		Vector3d position;
		Array3d val_pdf_scattering;
		
		// Sample a scattering even in the box, along ray r, fails if there is
		// no density along the ray.
		int id;
		bool success = sampleScattering(r, sampler, position, id,
										val_pdf_scattering,
										false /* use attenuation only */);

		// As we sampled scattering using attenuation, sample weight should be 1
		if (!success) {
			continue;
		}
		
		assert(success);
		assert(m_density[id] > 0);

		// Once we get the first intersection, loop:
		//  - phase sampling
		//  - transparency sampling (leaving or not the box)
		//  - sample new scattering position

		Array3d throughput = getAlbedo_ss(id);
		double tmin, tmax;
		double transp;

		int nb_bounces = 1;		
		while (nb_bounces < max_loop) {
			Vector3d wo = samplePhase_full(id, -dir, sampler);
			r = Ray(position,
					wo,
					(Vector3d(box.bounds[1]-box.bounds[0])).norm());
			box.intersect(r, tmin, tmax);
			r.tmax = tmax;
			
			// Get transparency in the box
			integrate(r, transp);
			double sample_t = uniform_dist(sampler);
			if (sample_t < transp) {
				// Leave the box
				mult_scatt += throughput;
				n_samples_ms++;
				break;
			} else {
				// Sample new position
				bool sc = sampleScattering(r, sampler, position, id,
										   val_pdf_scattering,
										   false /* use attenuation only */);

				assert(sc);
				
				// Set new direction
				dir = wo;
				throughput *= getAlbedo_ss(id);
				nb_bounces++;
			}
		}
		
		if (nb_bounces == max_loop) {
			// If max_loop is reached, take the current throughput
			mult_scatt += throughput;
			n_samples_ms++;
		}
	}
	   
	if (n_samples_ms > 0) {
		mult_scatt /= n_samples_ms;
		return true;
	}
	return false;
}

void SGGXVolume::writeVoidVoxel(int id) {
	assert_goodId(id);
	writeDensity(id, 0);
	writeAlbedo_ss(id, Array3d(0,0,0));
	writeSGGX(id, SGGX(Vector3d(1,1,1), Vector3d(0,0,0)));
}
