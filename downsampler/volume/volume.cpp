#include "volume.h"
#include "volume_io.h"
#include "../utils/utils.h"

#include <boost/thread.hpp>

#include <cmath> // isnan


Volume::Volume() {
	m_density = NULL;
	m_grid = Array3i(0,0,0); 
	m_lod = -1;
	m_min = Array3d(0.f,0.f,0.f); 
	m_max = Array3d(0.f,0.f,0.f);
	m_vsize = Array3d(0.f,0.f,0.f);
}

Volume::~Volume() {
	
}

void Volume::assert_goodId(int id) const {
	assert(id >= 0 && id < m_grid(0)*m_grid(1)*m_grid(2));
}

int Volume::getId(int x, int y, int z) const {
	return x + y*m_grid(0) + z*m_grid(0)*m_grid(1);
}

int Volume::getId(const Array3i &xyz) const {
	return getId(xyz(0), xyz(1), xyz(2));
}

Array3d Volume::getVoxelSize() const {
	double vsX = (m_max(0)-m_min(0)) / m_grid(0); // voxel size in X
	double vsY = (m_max(1)-m_min(1)) / m_grid(1); // voxel size in Y
	double vsZ = (m_max(2)-m_min(2)) / m_grid(2); // voxel size in Z
	return Array3d(vsX, vsY, vsZ);
}


/**
 * Estimate the probability for a incoming ray
 * to leave the box after n bounces. Doesn't 
 * take into account albedos.
 */
bool  Volume::estimateScatProb(const Box3 &box, Sampler &sampler,
							   std::vector<double> &scat_prob,
							   int nb_samples) const {

	std::uniform_real_distribution<double> uniform_dist(0, 1);
	int n_samples_ms = 0;

	scat_prob.resize(200);
	
	for (int i = 0; i < nb_samples; i++) {
		
		Ray r = box.sampleRay(sampler, false);
		Vector3d dir = r.d;
		Vector3d position;
		Array3d val_pdf_scattering;
		
		// Sample a scattering even in the box, along ray r.
		// Fail if there is no density along the ray
		int id;
		bool success = sampleScattering(r, sampler, position, id, val_pdf_scattering, false);
		//std::cout << "val_pdf_scattering " << to_string(val_pdf_scattering) << std::endl;
		if (!success) {
			continue;
		}
		assert(success);
		assert(m_density[id] > 0);

		// once we have the first intersection, loop:
		// sample phase function, sample transparency, sample scattering.

		double tmin, tmax;
		double transp;

		int nb_bounces = 1;
		int max_loop = 200;
		
		while (nb_bounces < max_loop) {
			Vector3d wo = samplePhase_full(id, -dir, sampler);
			r = Ray(position,
					wo,
					(Vector3d(box.bounds[1]-box.bounds[0])).norm());
			box.intersect(r, tmin, tmax);
			r.tmax = tmax;
			
			// get transparency
			integrate(r, transp);
			double sample_t = uniform_dist(sampler);
			if (sample_t < transp) {
				// leave the volume
				break;
			} else {
				
				//std::cout << "got transparency " << transp << std::endl;
				//std::cout << "got samplet " << sample_t << std::endl;
				// sample new position
				sampleScattering(r, sampler, position, id, val_pdf_scattering, false);
				// set new dir
				dir = wo;
				nb_bounces++;
			}
		}

		nb_bounces = std::min(nb_bounces, int(scat_prob.size()));
		scat_prob[nb_bounces-1] += 1;
		n_samples_ms++;
		//std::cout << nb_bounces << std::endl;

	}
	   
	// normalize
	if (n_samples_ms == 0) {
		return false;
	}
	
	for (unsigned int i = 0; i < scat_prob.size(); i++) {
		scat_prob[i] /= n_samples_ms;
	}
	return true;
	
}


// Cast rays in a box (can contain several voxels)
// return : mean probability of occlusion after single scattering?
bool Volume::estimateSingleScat(const Box3 &box,
								Sampler &sampler,
								Array3d &single_scatt,
								Array3d &mean_ss_albedo,
								int nb_samples_sh_estimate,
								bool includeShadowing,
								bool randomizeDir) const {
	
	// for loop
	int n_samples = 0;
	int n_samples_scatt_failed = 0;
	int n_samples_box_failed = 0;
	int n_samples_integrate_failed = 0;
	Array3d mean_single(0.0, 0.0, 0.0);
	int n_mean_ss = 0;
	mean_ss_albedo = Array3d(0.0, 0.0, 0.0);
	
	for (int i = 0; i < nb_samples_sh_estimate; i++) {
		
		Ray r = box.sampleRay(sampler, randomizeDir);
		Vector3d wi = r.d;
		Vector3d position;
		Array3d val_pdf_scattering;

		// Sample a scattering even in the box, along ray r.
		// Fail if there is no density along the ray
		// val_pdf_scattering takes into account the probability
		// of leaving the medium with direct transmittance
		int id;
		bool success = sampleScattering(r, sampler, position, id,
										val_pdf_scattering, true);
		if (!success) {
			n_samples++;
			n_samples_scatt_failed++;
			continue;
		}
		assert(success);

		assert(m_density[id] > 0);

		mean_ss_albedo += getAlbedo_ss(id);
		n_mean_ss++;

		// Careful! wi = -raydir
		Vector3d wo = samplePhase_ss(id, -wi, sampler); 
		
		r = Ray(position,
				wo,
				(Vector3d(box.bounds[1]-box.bounds[0])).norm());
		double tmin, tmax;
	    success = box.intersect(r, tmin, tmax);
		if (!success || tmax <= 0) {
			n_samples_box_failed++;
			// means that the ray doesn't intersect the box that is sampled.
			continue;
		}
		assert(success);
		if (!(tmax > 0)) {
			std::cout << "Volume::estimateShadowing - Warning, weard tmax " << tmax << std::endl;
			continue;
		}
		assert(tmax > 0);
		//assert(tmin <= 0);
		r.tmax = tmax;
		if (includeShadowing) {
			double transp;
			if (integrate(r, transp)) {	
				mean_single += transp * val_pdf_scattering;
				n_samples++;
			} else {
				std::cout << "integrate_failed" << std::endl;
				n_samples_integrate_failed++;
			}
		} else {
			mean_single += val_pdf_scattering;
			n_samples++;
		}
	}
	
	//std::cout << "valid samples: " << float(n_samples)/NB_LOOP*100 << "%" << std::endl;
	//std::cout << "scatt_failed: " << float(n_samples_scatt_failed)/n_loop*100 << "%" << std::endl;
	//std::cout << "box_failed: " << float(n_samples_box_failed)/n_loop*100 << "%" << std::endl;
	//std::cout << "integrate_failed: " << float(n_samples_integrate_failed)/n_loop*100 << "%" << std::endl;

	if (n_samples == 0) {
		return false;
	}
	
	//std::cout << "estimateShadowing done" << std::endl;

	single_scatt = mean_single / n_samples;
	mean_ss_albedo /= n_mean_ss;
	//std::cout << "fail/samples: " << double(n_samples_scatt_failed)/n_samples << std::endl;

	return true;
}


/*
 * sampling a scattering event along a ray with
 * importance sampling based on both albedo and density
 */
bool Volume::sampleScattering(Ray &ray,
							  Sampler &sampler,
							  Vector3d &position,
							  int &id,
							  Array3d &val_pdf,
							  bool singleScattering) const {
	
	Array3i voxel_id, next_voxel_id;
	bool foundFirst = getVoxelId(ray.o, voxel_id);	
	if (!foundFirst) {		
		Box3 bb(m_min, m_max);
		double tmin, tmax;
		bool success = bb.intersect(ray, tmin, tmax);
		if (!success) {
			std::cout << "Volume::sampleScattering - Warning, ray.o not in voxel "
					  << "and ray doesn't intersect voxel grid?" << std::endl;
			return false;
		}
		//assert(tmin > 0);
		ray.o += tmin * ray.d;
		bool foundFirst_try2 = getVoxelId(ray.o, voxel_id);
		if (!foundFirst_try2) {
			std::cout << "Volume::sampleScattering - Not implemented, "
					  << "ray.o must be in a voxel." << std::endl;
			return false;
		}
	}
	
	std::vector<Segment> segments;
	double dist, sigma_t;
	Array3d sigma_s;

	// Store for each voxel the length of the ray inside the cube,
	// the attenuation coefficient sigma_t and scattering coefficient
	// sigma_s.
	bool has_next = true;
	while (has_next) {
		has_next = rayCubeIntersect(voxel_id, ray, dist, next_voxel_id);
		int v_id = getId(voxel_id); // index of voxel voxel_id	
		sigma_t = getSigma_t(v_id, ray.d);
		if (singleScattering) {
			sigma_s = getSigma_ss(v_id, ray.d);
		} else {
			sigma_s = Array3d(sigma_t, sigma_t, sigma_t);
		}
		segments.push_back(Segment(dist, sigma_t, sigma_s, v_id));
		voxel_id = next_voxel_id;
	} 	
	
	// compute \int sigma_s e^{-\int sigma_t ds}
	// given by: sum of sigma_s/sigma_t x (1 - e^{-sigma_t d}) x transparency before

	// Compute the pdf:
	// for each segment, weight = \int sigma_s e^{\int sigma_t ds}
	// we chose for the pdf sigma_s.mean
	double single_scattering_ray = 0.0; // part qui part en single scat
	double transp_before = 1.0;
	double dist_before = 0.0;
	double scattering_prob[segments.size()];
	double transp_before_cum[segments.size()];
	double dist_before_cum[segments.size()];
	
	for (unsigned int i = 0; i < segments.size(); i++) {
		single_scattering_ray += ((segments[i].sigma_t > 0) ? segments[i].sigma_s.mean()/segments[i].sigma_t : 0.0)
			* (1.0 - std::exp(-segments[i].sigma_t*segments[i].dist))
			* transp_before;
		
		scattering_prob[i] = single_scattering_ray;
		transp_before_cum[i] = transp_before;
		dist_before_cum[i] = dist_before;
		transp_before *= std::exp(-segments[i].sigma_t*segments[i].dist);
		dist_before += segments[i].dist;
	}
		
	// check that single_scattering_ray is a part of energy or proba
	if (!(single_scattering_ray >= 0 && single_scattering_ray <= 1.01)) {
		std::cout << "Volume::sampleScattering - Debug, single_scattering_ray "
				  <<  single_scattering_ray << std::endl;

		for (unsigned int i = 0; i < segments.size(); i++) {
			std::cout << "   dists[i] " <<  segments[i].dist << std::endl;
			std::cout << "   sigma_s[i] " <<  to_string(segments[i].sigma_s)  << std::endl;
			std::cout << "   sigma_t[i] " <<  segments[i].sigma_t  << std::endl;
		}
		
		std::cout << "This is because density is too high, numerical errors."
				  << std::endl;
		return false;
	}

	single_scattering_ray = std::min(single_scattering_ray, 1.0);
	
	assert(single_scattering_ray >= 0 && single_scattering_ray <= 1.0);
	
	if (single_scattering_ray == 0.0) {
		return false;
	}

	// Sample the pdf
    std::uniform_real_distribution<double> uniform_dist(0, single_scattering_ray);
    double sample = uniform_dist(sampler);

	int segment_id = 0;
	// Find proper segment depending on sample value
	for (unsigned int i = 0; i < segments.size(); i++) {
		if (sample < scattering_prob[i])
			break;
		segment_id++;
	}

	assert(segments[segment_id].sigma_t > 0);
	
	double integral_before = (segment_id > 0) ? scattering_prob[segment_id-1] : 0.0;
	
	// s in segment segment_id	
	// equation : sample = integral_before + sigma_s[i]/sigma_t[i] (1 - exp(-s*sigma_t[i]))*transp_before_cum[i];
	// s = -log(1 - (sample - integral_before)/transp_before_cum[i])/sigma_t[i]
	double s = -std::log(1.0-(sample-integral_before)/transp_before_cum[segment_id])/segments[segment_id].sigma_t;

	if (s >  segments[segment_id].dist) {
		float error = std::abs(s - segments[segment_id].dist);
		if (error > 1e-10) {
			std::cout << "Volume::sampleScattering - Warning, s > dist : "
					  << segments[segment_id].dist << " "
					  << s << std::endl;
		}
		s = segments[segment_id].dist;
	}
	s += dist_before_cum[segment_id];

	if (s < 0) {
		std::cout << "s: " << s << std::endl;
		//std::cout << "sum_dist: " << sum_dist << std::endl;
		for (unsigned i = 0; i < segments.size(); i++) {
			std::cout << segments[i].dist  << " " << std::endl;
		}
		std::cout << "sample: " << sample << std::endl;
		//std::cout << "sum_integrated: " << sum_integrated << std::endl;
		//std::cout << "proba_occlusion: " << proba_occlusion << std::endl;
		assert("Big problem" && 0);
	}


	
	position = ray.o + s * ray.d;
	
	// here, value = sigma_s * attenuation = sigma_s * e^{-\int sigma_t ds}
	// and pdf = sigma_s.mean * e^{-\int sigma_t ds}/norm
	// so val_pdf = sigma_s/sigma_s.mean*norm

	id = segments[segment_id].id;
	
	val_pdf = segments[segment_id].sigma_s * single_scattering_ray / segments[segment_id].sigma_s.mean();
	
	return true;
}

bool Volume::integrate(Ray &ray,double &transp) const {

	Array3i voxel_id, next_voxel_id;
	bool foundFirst = getVoxelId(ray.o, voxel_id);
	
	if (!foundFirst) {
		std::cout << "Volume::integrate : not implemented, ray.o must be in a voxel." << std::endl;
		return false;
	}

	double sum_dist = 0.0, dist;

	bool has_next = true;
	while (has_next) {
		has_next = rayCubeIntersect(voxel_id, ray, dist, next_voxel_id);
		sum_dist += dist*getSigma_t(getId(voxel_id), ray.d);
		voxel_id = next_voxel_id;
	}

	transp = std::exp(-sum_dist);
	return true;
}

// return true if there is a next volume.
// voxel_id returns the next's voxel if there is one
bool Volume::rayCubeIntersect(Array3i voxel_id, Ray &ray, double &d,
							  Array3i &next_voxel_id) const {
	Array3d voxel_id_f= voxel_id.cast<double>();
	// compute voxel min et max
	Array3d vmin = m_min + m_vsize*voxel_id_f;
	Array3d vmax = vmin + m_vsize;
	Box3 box(vmin, vmax);
	double tmin = 0.0, tmax = 0.0;

	// parallel to axes, ambiguity that box.intersect cannot solve
	// ask for the ray in the middle of the proper voxel instead
	double aux;
	if (ray.d(0) == 0 && ray.d(1) == 0) {
		tmin = (vmin(2) - ray.o(2))*ray.d(2);
		tmax = (vmax(2) - ray.o(2))*ray.d(2);
	} else if (ray.d(1) == 0 && ray.d(2) == 0) {
		tmin = (vmin(0) - ray.o(0))*ray.d(0);
		tmax = (vmax(0) - ray.o(0))*ray.d(0);
	} else if (ray.d(0) == 0 && ray.d(2) == 0) {
		tmin = (vmin(1) - ray.o(1))*ray.d(1);
		tmax = (vmax(1) - ray.o(1))*ray.d(1);
	} else if (ray.d(0) == 0) {
		assert("not implemented" && ray.o(0) != vmin(0) && ray.o(0) != vmax(0));
	} else if (ray.d(1) == 0) {
		assert("not implemented" && ray.o(1) != vmin(1) && ray.o(1) != vmax(1));
	} else if (ray.d(2) == 0) {
		assert("not implemented" && ray.o(2) != vmin(2) && ray.o(2) != vmax(2));
	} else {
		bool success = box.intersect(ray, tmin, tmax);

		if (!success) {
			d = 0;
			bool has_next = getVoxelId(ray.o + ray.d*m_vsize(0)/100.0, next_voxel_id);
			if (is_equal(next_voxel_id, voxel_id)) {
				//std::cout << "Volume::rayCubeIntersect - Problem, next = cur" << std::endl;
				return false;
			}
			return has_next;
		}
		
		assert(success);
		//std::cout << "tmin et tmax: " << tmin << " " << tmax << std::endl;
	}

	if (tmin > tmax) {
		aux = tmin;
		tmin = tmax;
		tmax = aux;
	}
	
	// check all and compute d and next
	if (tmax < 0) {
		// fix: go to the next voxel
		d = 0;
		bool has_next = getVoxelId(ray.o + ray.d*m_vsize(0)/100.0, next_voxel_id);
		if (is_equal(next_voxel_id, voxel_id)) {
			return false;
			std::cout << "Volume::rayCubeIntersect - Problem, next = cur" << std::endl;
		}
		return has_next;
	}

	assert(tmax >= 0);
	// case tmin < 0 : begining of the ray
	if (tmin < 0) {
		if (ray.tmax > tmax) {
			d = tmax;
			bool has_next = getVoxelId(ray.o + ray.d*(tmax + m_vsize(0)/100.0), next_voxel_id);
			if (is_equal(next_voxel_id, voxel_id)) {
				return false;

				std::cout << to_string(next_voxel_id) << std::endl;
				std::cout << "tmin tmax " << tmin << " " << tmax << std::endl;
				std::cout << "Volume::rayCubeIntersect - Problem, next = cur" << std::endl;
			}
			return has_next;
		} else {
			d = ray.tmax;
			return false;
		}
	} else {
		// tmin >= 0
		if(tmin >= ray.tmax) {
			d = 0;
			return false;
		}
		// ray begin outside : if stop inside if tmax > ray.tmax
		if (ray.tmax > tmax) {
			d = tmax - tmin;
			bool has_next = getVoxelId(ray.o + ray.d*(tmax + m_vsize(0)/10.0), next_voxel_id);
			if (is_equal(next_voxel_id, voxel_id)) {
				return false;
				std::cout << to_string(next_voxel_id) << std::endl;
				std::cout << "tmin tmax " << tmin << " " << tmax << std::endl;
				std::cout << "Volume::rayCubeIntersect - Problem, next = cur" << std::endl;
			}
			return has_next;
		} else {
			d = ray.tmax - tmin;
			return false;
		}
	}
}

bool Volume::getVoxelId(Array3d pos, Array3i &voxel_id) const {
	// pos to grid
	Array3d extend = m_max-m_min;
	Array3d pos_grid = (pos - m_min) / extend * m_grid.cast<double>();
	voxel_id = Array3i(std::floor(pos_grid(0)),
					   std::floor(pos_grid(1)),
					   std::floor(pos_grid(2)));

	// cas particuliers
	if (pos(0) == m_max(0)) {
		voxel_id(0) = m_grid(0)-1;
	}
	if (pos(1) == m_max(1)) {
		voxel_id(1) = m_grid(1)-1;
	}
	if (pos(2) == m_max(2)) {
		voxel_id(2) = m_grid(2)-1;
	}

	bool test = (voxel_id(0) >= 0)
		&& voxel_id(1) >= 0
		&& voxel_id(2) >= 0
		&& (voxel_id(0) < m_grid(0))
		&& (voxel_id(1) < m_grid(1))
		&& (voxel_id(2) < m_grid(2));	
	return test;
}
