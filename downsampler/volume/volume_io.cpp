#ifndef VOLUME_IO_H
#define VOLUME_IO_H


#include "volume_io.h"

void readFloatFile(const char * file, float **data,
				   Eigen::Array3i &grid,
                   Eigen::Array3d &min,
                   Eigen::Array3d &max) {
    FILE *pFile;

    pFile=fopen(file,"rb");
    assert(pFile);

    if (pFile) {

        // read header
        int V, O, L, chanels;
		size_t st;

        st = fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        st = fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        st = fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        st = fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        st = fread(&type, 1, sizeof(int), pFile);
		assert(type == 1);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
		int gridX, gridY, gridZ;
        st = fread(&gridX, 1, sizeof(int), pFile);
        st = fread(&gridY, 1, sizeof(int), pFile);
        st = fread(&gridZ, 1, sizeof(int), pFile);
		grid = Eigen::Array3i(gridX,gridY,gridZ);
		
        //std::cout << "Grid size: " << gridX << " " << gridY << " " << gridZ << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        st = fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 1);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
		float minX, minY, minZ, maxX, maxY, maxZ;
        st = fread(&minX, 1, sizeof(float), pFile);
        st = fread(&minY, 1, sizeof(float), pFile);
        st = fread(&minZ, 1, sizeof(float), pFile);
        st = fread(&maxX, 1, sizeof(float), pFile);
        st = fread(&maxY, 1, sizeof(float), pFile);
        st = fread(&maxZ, 1, sizeof(float), pFile);
		min = Eigen::Array3d(minX,minY,minZ);
		max = Eigen::Array3d(maxX,maxY,maxZ);

        (*data) = new float[gridX*gridY*gridZ];
        st = fread((*data), gridX * gridY * gridZ, sizeof(float), pFile);
		st++; // just avoid warnings;

    }
    fclose(pFile);
}


void readChar3File(const char * file, unsigned char **data,
				   Eigen::Array3i &grid,
				   Eigen::Array3d &min,
				   Eigen::Array3d &max) {
    FILE *pFile;

    pFile=fopen(file,"rb");
    assert(pFile);

    if (pFile) {

        // read header
        int V, O, L, chanels;
		size_t st;
        st = fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        st = fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        st = fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        st = fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        st = fread(&type, 1, sizeof(int), pFile);
		assert(type == 3);
		
        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
		int gridX, gridY, gridZ;
        st = fread(&gridX, 1, sizeof(int), pFile);
        st = fread(&gridY, 1, sizeof(int), pFile);
        st = fread(&gridZ, 1, sizeof(int), pFile);
		grid= Eigen::Array3i(gridX,gridY,gridZ);

        //std::cout << "Grid size: " << grid_size << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        st = fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 3);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
		float minX, minY, minZ, maxX, maxY, maxZ;
        st = fread(&minX, 1, sizeof(float), pFile);
        st = fread(&minY, 1, sizeof(float), pFile);
        st = fread(&minZ, 1, sizeof(float), pFile);
        st = fread(&maxX, 1, sizeof(float), pFile);
        st = fread(&maxY, 1, sizeof(float), pFile);
        st = fread(&maxZ, 1, sizeof(float), pFile);
		min = Eigen::Array3d(minX,minY,minZ);
		max = Eigen::Array3d(maxX,maxY,maxZ);
		
        (*data) = new unsigned char[gridX*gridY*gridZ*3];
        st = fread((*data), gridX * gridY * gridZ * 3, sizeof(unsigned char), pFile);
		st++; // just avoid warnings;

    }
    fclose(pFile);
}

void writeChar3File(const char * file, unsigned char **data,
					const Eigen::Array3i &grid,
                    const Eigen::Array3d &min,
                    const Eigen::Array3d &max) {
    FILE *pFile;

    std::remove(file);

    pFile=fopen(file,"a");
    assert(pFile);

    if (pFile) {

        // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
        int V = 86, O = 79, L = 76;
        fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version = 3;
        fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type = 3; //uint8
        fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
		int gridX = grid(0), gridY = grid(1), gridZ = grid(2);
        fwrite(reinterpret_cast<const char*>(&gridX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridZ), 1, sizeof(int), pFile);

        int chanels = 3;
        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
		float minX = min(0), minY = min(1), minZ = min(2),
			maxX = max(0), maxY = max(1), maxZ = max(2);
		fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);

		
        fwrite(reinterpret_cast<const char*>(*data),    gridX * gridY * gridZ * 3, sizeof(char), pFile);

    }
    fclose(pFile);
}

void writeFloatFile(const char * file, float **data,
					const Eigen::Array3i &grid,
                    const Eigen::Array3d &min,
                    const Eigen::Array3d &max) {
    FILE *pFile;

    std::remove(file);

    pFile=fopen(file,"a");
    assert(pFile);

    if (pFile) {

        // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
        int V = 86, O = 79, L = 76;
        fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version = 3;
        fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type = 1; //float32
        fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
		int gridX = grid(0), gridY = grid(1), gridZ = grid(2);
        fwrite(reinterpret_cast<const char*>(&gridX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridZ), 1, sizeof(int), pFile);
		
        int chanels = 1;
        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
		float minX = min(0), minY = min(1), minZ = min(2),
			maxX = max(0), maxY = max(1), maxZ = max(2);
        fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(*data), gridX*gridY*gridZ, sizeof(float), pFile);

    }
    fclose(pFile);
}

void writeFloat3File(const char * file, float **data,
					 const Eigen::Array3i &grid,
					 const Eigen::Array3d &min,
					 const Eigen::Array3d &max) {
    FILE *pFile;

    std::remove(file);

    pFile=fopen(file,"a");
    assert(pFile);

    if (pFile) {

        // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
        int V = 86, O = 79, L = 76;
        fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version = 3;
        fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type = 1; //float32
        fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
		int gridX = grid(0), gridY = grid(1), gridZ = grid(2);
        fwrite(reinterpret_cast<const char*>(&gridX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridZ), 1, sizeof(int), pFile);
		
        int chanels = 3;
        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
		float minX = min(0), minY = min(1), minZ = min(2),
			maxX = max(0), maxY = max(1), maxZ = max(2);
        fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(*data), gridX*gridY*gridZ*3, sizeof(float), pFile);

    }
    fclose(pFile);
}


#endif // VOLUME_IO_H
