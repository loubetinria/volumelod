/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/core/sggx.h>

MTS_NAMESPACE_BEGIN

SGGX::SGGX(Vector3f d, Vector3f t) {
    m_diag = d;
    m_tri = t;
    if (d.length() == 0.0 && t.length() == 0.0) {
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
    }
}

SGGX::SGGX(Vector3f normal, float roughness) {
    fromSurface(normal,roughness, roughness);
}

SGGX::SGGX(Spectrum sigma, Spectrum r) {
    fromSigmaR(sigma, r);
}

SGGX::SGGX(Matrix3x3 matrix) {
    fromMatrix(matrix);
}

void SGGX::fromSurface(Vector3f normal, float roughnessU, float roughnessV) {
    if (roughnessU < 0)
        Log(EError, "Got roughnessU < 0!");

    if (roughnessV < 0)
        Log(EError, "Got roughnessV < 0!");

    if ( !(normal.length() > 0.99 && normal.length()< 1.01)) {
        std::cout << normal.toString() << std::endl;
        Log(EError, "Got not normalized normal!");
    }

    Vector3f tangent, bitangent;
    coordinateSystem(normalize(normal), tangent, bitangent);

    Matrix3x3 base(tangent,bitangent,normal);
    if (roughnessU < MIN_ROUGHNESS)
        roughnessU = MIN_ROUGHNESS;
    if (roughnessV < MIN_ROUGHNESS)
        roughnessV = MIN_ROUGHNESS;

    Matrix3x3 eigenvalues(roughnessV, 0.0, 0.0,
                          0.0, roughnessU, 0.0,
                          0.0, 0.0, 1.0);
    Matrix3x3 base_t;
    base.transpose(base_t); // base_t is the target
    fromMatrix(base * eigenvalues * base_t);
}

void SGGX::fromSigmaR(Spectrum sigma, Spectrum r) {

    Float sigma_x, sigma_y, sigma_z, r_xy, r_xz, r_yz;
    sigma.toLinearRGB(sigma_x,sigma_y,sigma_z);
    r.toLinearRGB(r_xy,r_xz,r_yz);

    // Rescale r between -1 and 1
    r_xy = r_xy*2.0 - 1.0;
    r_xz = r_xz*2.0 - 1.0;
    r_yz = r_yz*2.0 - 1.0;
    Float Sxy = r_xy*sigma_x*sigma_y;
    Float Sxz = r_xz*sigma_x*sigma_z;
    Float Syz = r_yz*sigma_y*sigma_z;
    Float Sxx = sigma_x*sigma_x;
    Float Syy = sigma_y*sigma_y;
    Float Szz = sigma_z*sigma_z;
    m_diag = Vector3f(Sxx,Syy,Szz);
    m_tri = Vector3f(Sxy,Sxz,Syz);
    if (testIsNull()) {
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
    }
}

void SGGX::fromSigmaR(Vector3f sigma, Vector3f r) {

    Float sigma_x, sigma_y, sigma_z, r_xy, r_xz, r_yz;
    sigma_x = sigma[0];
    sigma_y = sigma[1];
    sigma_z = sigma[2];
    r_xy = r[0];
    r_xz = r[1];
    r_yz = r[2];

    // Rescale r between -1 and 1
    r_xy = r_xy*2.0 - 1.0;
    r_xz = r_xz*2.0 - 1.0;
    r_yz = r_yz*2.0 - 1.0;
    Float Sxy = r_xy*sigma_x*sigma_y;
    Float Sxz = r_xz*sigma_x*sigma_z;
    Float Syz = r_yz*sigma_y*sigma_z;
    Float Sxx = sigma_x*sigma_x;
    Float Syy = sigma_y*sigma_y;
    Float Szz = sigma_z*sigma_z;

    // Because of numerical instabilities
    Sxx = Sxx*0.98 + 0.02;
    Syy = Syy*0.98 + 0.02;
    Szz = Szz*0.98 + 0.02;

    m_diag = Vector3f(Sxx,Syy,Szz);
    m_tri = Vector3f(Sxy,Sxz,Syz);
    if (testIsNull()) {
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
    }
}


// Assume a symmetric matrix
void SGGX::fromMatrix(Matrix3x3 matrix) {

    if (    std::abs(matrix(1,0) - matrix(0,1)) > 0.001
         || std::abs(matrix(1,2) - matrix(2,1)) > 0.001
         || std::abs(matrix(0,2) - matrix(2,0)) > 0.001)
        Log(EError, "Matrix is not symmetric!");

    m_diag = Vector3f(matrix(0,0), matrix(1,1), matrix(2,2));
    m_tri = Vector3f(matrix(1,0), matrix(2,0), matrix(2,1));
    if (testIsNull()) {
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
    }
}

Matrix3x3 SGGX::toMatrix() const {
    return Matrix3x3(m_diag[0], m_tri[0], m_tri[1],
                     m_tri[0], m_diag[1], m_tri[2],
                     m_tri[1], m_tri[2], m_diag[2]);
}

bool SGGX::getNormal(Vector3f &normal) const {
   Matrix3x3 sggx = toMatrix();
   Float eigenvalues[3];
   bool success = eig3(sggx, eigenvalues); // replace sggx by eigen vectors
   if (success) {
       normal = sggx.col(0); // heighest eigen value
   } else {
       Log(EError, "Eigen failled, no normal!");
   }
   return success;
}

void SGGX::fromFilteredSGGX(std::vector<SGGX> vec, std::vector<Float> weights) {
    if (vec.size() != weights.size())
        Log(EError, "Not as many SGGX as weights!");

    if(vec.size()<=0) {
        // set to null
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
        return;
    }

    Matrix3x3 meanSGGX(0.0);
    for (unsigned i = 0; i < vec.size(); i++) {
        meanSGGX += vec[i].toMatrix() * weights[i];
    }
    fromMatrix(meanSGGX / float(vec.size())); // useless ?
    normalizeSGGX();

    if (testIsNull())
        Log(EError, "Got invalid sggx!");
}

void SGGX::printEigenValues() const {

    Matrix3x3 sggx = toMatrix();
    Float eigenvalues[3];
    bool success = eig3(sggx, eigenvalues); // remplace sggx by eigen vectors
    if (success) {
        std::cout << "Eigen values : "
                  << eigenvalues[0] << " "
                  << eigenvalues[1] << " "
                  << eigenvalues[2] <<  std::endl;
    }
}

void SGGX::normalizeSGGX() {

    Matrix3x3 sggx = toMatrix();
    Float eigenvalues[3];
    bool success = eig3(sggx, eigenvalues); // replace sggx by eigen vectors
    if (!success || eigenvalues[0] == 0 
                 || eigenvalues[1] == 0 
                 || eigenvalues[2] == 0) {
        m_diag = Vector3f(1.0,1.0,1.0);
        m_tri = Vector3f(0.0,0.0,0.0);
    }
    // divide by the highest eigenvalue
    m_tri /= eigenvalues[0];
    m_diag /= eigenvalues[0];
}



/*
    This function solves for a density parameter given a target
    mean opacity and this sggx. There is no closed-form expression
    for this problem, so the approach is quite naive: sample
    many directions, approximate the mean projected area, 
    and numerically solve for a density parameter.
*/
float SGGX::optimalDensity2(float meanTransparency, Sampler *sampler) {

    assert(meanTransparency > 0 && meanTransparency <= 1);

    float initial = -std::log(meanTransparency);

    assert(initial > 0);

    float density = initial;
    float eps = 0.001;
    float currentT;
    float current_dT_dx;
    int step = 0;

    std::vector<float> projectedAreas;
    for (int i = 0; i < 30 ; i++)  {
        Vector3f view = warp::squareToUniformSphere(sampler->next2D());
        projectedAreas.push_back(sigma(view));
    }

    for (int i = 0; i < 100 ; i++) {

        // At each iteration, compute the average opacity 
        // (exp(- density * projectedAreas)) and its derivative
        // with respect to density (which is the parameter we are optimizing).

        currentT = 0;
        current_dT_dx = 0;

        for (unsigned v = 0; v < projectedAreas.size(); v++) {
            currentT += std::exp(-density*projectedAreas[v]);
            current_dT_dx += -projectedAreas[v] * std::exp(-density*projectedAreas[v]);
        }

        currentT /= projectedAreas.size();
        current_dT_dx /= projectedAreas.size();

        if (std::abs(currentT - meanTransparency) < eps)
            break;

        // Using the gradient, estimate a better density parameter
        density = -(currentT - meanTransparency)/current_dT_dx + density;

        step++;
    }

    return density;
}


/*
 * PUBLIC VERSIONS
 */

Float SGGX::eval_specular(Vector3f wi, Vector3f wo) const {
    return eval_specular(wi, wo,
                         m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z);
}

Vector3f SGGX::sample_specular(const Vector3f wi, const Float U1, const Float U2) const {
    return sample_specular(wi,
                           m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z,
                           U1, U2);
}

Float SGGX::eval_diffuse(Vector3f wi, Vector3f wo,
                   const Float U1, const Float U2) const {
    return eval_diffuse(wi, wo,
                        m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z,
                        U1, U2);
}

Vector3f SGGX::sample_diffuse(const Vector3f wi,
                        const float U1, const float U2, const float U3, const float U4) const {
    return sample_diffuse(wi,
                          m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z,
                          U1, U2, U3, U4);
}

Vector3f SGGX::sample_VNDF(const Vector3f wi,
                        const float U1, const float U2) const {

    return sample_VNDF(wi, m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z,
                       U1, U2);
}

Float SGGX::sigma(Vector3f wi) const {


    if (!(wi.length() > 0.99 && wi.length()< 1.01)) {
        wi /= wi.length();
    }

    return sigma(wi, m_diag.x, m_diag.y, m_diag.z, m_tri.x, m_tri.y, m_tri.z);
}

/*
 * FROM THE SGGX PAPER
 */

Vector3f SGGX::sample_VNDF(const Vector3f wi,
                     const Float S_xx, const Float S_yy, const Float S_zz,
                     const Float S_xy, const Float S_xz, const Float S_yz,
                     const Float U1, const Float U2) const {

    // generate sample (u, v, w)
    const Float r = sqrt(U1);
    const Float phi = 2.0f*M_PI*U2;
    const Float u = r*cos(phi);
    const Float v= r*sin(phi);
    const Float w = sqrt(1.0f - u*u - v*v);

    // build orthonormal basis
    Vector3f wk, wj;
    buildOrthonormalBasis(wk, wj, wi);

    // project S in this basis
    const Float S_kk = wk.x*wk.x*S_xx + wk.y*wk.y*S_yy + wk.z*wk.z*S_zz
            + 2.0f * (wk.x*wk.y*S_xy + wk.x*wk.z*S_xz + wk.y*wk.z*S_yz);

    const Float S_jj = wj.x*wj.x*S_xx + wj.y*wj.y*S_yy + wj.z*wj.z*S_zz
            + 2.0f * (wj.x*wj.y*S_xy + wj.x*wj.z*S_xz + wj.y*wj.z*S_yz);

    const Float S_ii = wi.x*wi.x*S_xx + wi.y*wi.y*S_yy + wi.z*wi.z*S_zz
            + 2.0f * (wi.x*wi.y*S_xy + wi.x*wi.z*S_xz + wi.y*wi.z*S_yz);

    const Float S_kj = wk.x*wj.x*S_xx + wk.y*wj.y*S_yy + wk.z*wj.z*S_zz
            + (wk.x*wj.y + wk.y*wj.x)*S_xy
            + (wk.x*wj.z + wk.z*wj.x)*S_xz
            + (wk.y*wj.z + wk.z*wj.y)*S_yz;

    const Float S_ki = wk.x*wi.x*S_xx + wk.y*wi.y*S_yy + wk.z*wi.z*S_zz
            + (wk.x*wi.y + wk.y*wi.x)*S_xy + (wk.x*wi.z + wk.z*wi.x)*S_xz + (wk.y*wi.z + wk.z*wi.y)*S_yz;

    const Float S_ji = wj.x*wi.x*S_xx + wj.y*wi.y*S_yy + wj.z*wi.z*S_zz
            + (wj.x*wi.y + wj.y*wi.x)*S_xy
            + (wj.x*wi.z + wj.z*wi.x)*S_xz
            + (wj.y*wi.z + wj.z*wi.y)*S_yz;

    // compute normal
    Float sqrtDetSkji = sqrt(fabsf(S_kk*S_jj*S_ii - S_kj*S_kj*S_ii - S_ki*S_ki*S_jj - S_ji*S_ji*S_kk + 2.0f*S_kj*S_ki*S_ji));
    Float inv_sqrtS_ii = 1.0f / sqrt(S_ii);
    if (S_ii == 0) {
        //std::cout << "problem here" << std::endl;
        Vector3f r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }
    Float tmp = sqrt(S_jj*S_ii-S_ji*S_ji);
    if (tmp == 0) {
        //std::cout << "problem here" << std::endl;
        Vector3f r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }
    Vector3f Mk(sqrtDetSkji/tmp, 0.0f, 0.0f);
    Vector3f Mj(-inv_sqrtS_ii*(S_ki*S_ji-S_kj*S_ii)/tmp , inv_sqrtS_ii*tmp, 0);
    Vector3f Mi(inv_sqrtS_ii*S_ki, inv_sqrtS_ii*S_ji, inv_sqrtS_ii*S_ii);
    Vector3f test = u*Mk+v*Mj+w*Mi;
    if (test.length() == 0) {
        //std::cout << "problem here" << std::endl;
        Vector3f r = sample_VNDF(wi, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, U1, U2);
        if (std::isnan(r[0]))
            std::cout << "is nan again" << std::endl;
        return r;
    }

    Vector3f wm_kji = normalize(u*Mk+v*Mj+w*Mi);

    // rotate back to world basis
    Vector3f res = wm_kji.x * wk + wm_kji.y * wj + wm_kji.z * wi;

    return res/res.length(); // add norm to get more precise

}


Float SGGX::D(Vector3f wm,
        Float S_xx, Float S_yy, Float S_zz,
        Float S_xy, Float S_xz, Float S_yz) const {
    const Float detS = S_xx*S_yy*S_zz - S_xx*S_yz*S_yz - S_yy*S_xz*S_xz - S_zz*S_xy*S_xy + 2.0f*S_xy*S_xz*S_yz;

    const Float den = wm.x*wm.x*(S_yy*S_zz-S_yz*S_yz) + wm.y*wm.y*(S_xx*S_zz-S_xz*S_xz) + wm.z*wm.z*(S_xx*S_yy-S_xy*S_xy)
            + 2.0f*(wm.x*wm.y*(S_xz*S_yz-S_zz*S_xy) + wm.x*wm.z*(S_xy*S_yz-S_yy*S_xz) + wm.y*wm.z*(S_xy*S_xz-S_xx*S_yz));
    if (den == 0)
        return 0.0;

    const Float D = pow(fabs(detS), 1.5f) / (M_PI*den*den);
    return D;
}


Float SGGX::sigma(Vector3f wi,
            Float S_xx, Float S_yy, Float S_zz,
            Float S_xy, Float S_xz, Float S_yz) const {
    const float sigma_squared = wi.x*wi.x*S_xx + wi.y*wi.y*S_yy + wi.z*wi.z*S_zz
            + 2.0f * (wi.x*wi.y*S_xy + wi.x*wi.z*S_xz + wi.y*wi.z*S_yz);

    return (sigma_squared > 0.0f) ? sqrtf(sigma_squared) : 0.0f; // conditional to avoid numerical errors
}

void SGGX::buildOrthonormalBasis(Vector3f& omega_1, Vector3f& omega_2, const Vector3f& omega_3) const {
    if(omega_3.z < -0.9999999f) {
        omega_1 = Vector3f ( 0.0f , -1.0f , 0.0f );
        omega_2 = Vector3f ( -1.0f , 0.0f , 0.0f );
    } else {
        const float a = 1.0f /(1.0f + omega_3.z );
        const float b = -omega_3.x*omega_3 .y*a ;
        omega_1 = Vector3f (1.0f - omega_3.x*omega_3. x*a , b , -omega_3.x );
        omega_2 = Vector3f (b , 1.0f - omega_3.y*omega_3.y*a , -omega_3.y );
    }
}

Float SGGX::eval_specular(Vector3f wi, Vector3f wo,
                    const float S_xx, const float S_yy, const float S_zz,
                    const float S_xy, const float S_xz, const float S_yz) const {
    Vector3f wh = normalize(wi + wo);
    return 0.25f * D(wh, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz) / sigma(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz);
}

Vector3f SGGX::sample_specular(const Vector3f wi,
                         const Float S_xx, const Float S_yy, const Float S_zz,
                         const Float S_xy, const Float S_xz, const Float S_yz,
                         const Float U1, const Float U2) const {

    // sample VNDF
    const Vector3f wm = sample_VNDF(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz, U1, U2);

    // specular reflection
    const Vector3f wo = -wi + 2.0f * wm * dot(wm, wi);
    return wo;
}

Float SGGX::eval_diffuse(Vector3f wi, Vector3f wo,
                   const Float S_xx, const Float S_yy, const Float S_zz,
                   const Float S_xy, const Float S_xz, const Float S_yz,
                   const Float U1, const Float U2) const {

    // sample VNDF
    const Vector3f wm = sample_VNDF(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz, U1, U2);

    // eval diffuse
    return 1.0f / M_PI * std::max(0.0f, dot(wo, wm));

}

Vector3f SGGX::sample_diffuse(const Vector3f wi,
                        const float S_xx, const float S_yy, const float S_zz,
                        const float S_xy, const float S_xz, const float S_yz,
                        const float U1, const float U2, const float U3, const float U4) const {

    // sample VNDF
    const Vector3f wm = sample_VNDF(wi, S_xx, S_yy, S_zz, S_xy, S_xz, S_yz, U1, U2);
    // sample diffuse reflection
    Vector3f w1, w2;
    buildOrthonormalBasis(w1, w2, wm);
    float r1 = 2.0f*U3 - 1.0f;
    float r2 = 2.0f*U4 - 1.0f;

    // concentric map code from
    // http://psgraphics.blogspot.ch/2011/01/improved-code-for-concentric-map.html
    float phi, r;
    if (r1 == 0 && r2 == 0) {
        r = phi = 0;
    } else if (r1*r1 > r2*r2) {
        r = r1;
        phi = (M_PI/4.0f) * (r2/r1);
    } else {
        r = r2;
        phi = (M_PI/2.0f) - (r1/r2) * (M_PI/4.0f);
    }
    float x = r*cosf(phi);
    float y = r*sinf(phi);
    float z = sqrtf(1.0f - x*x - y*y);
    Vector3f wo = x*w1 + y*w2 + z*wm;
    return wo;
}

MTS_NAMESPACE_END
