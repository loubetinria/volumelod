/*
  This file is part of Mitsuba, a physically based rendering system.

  Copyright (c) 2007-2014 by Wenzel Jakob and others.

  Mitsuba is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License Version 3
  as published by the Free Software Foundation.

  Mitsuba is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/core/frame.h>
#include <mitsuba/render/phase.h>
#include <mitsuba/render/medium.h>
#include <mitsuba/render/sampler.h>
#include <mitsuba/render/trigphase.h>

/// The following file implements the micro-flake distribution
/// for rough fibers

MTS_NAMESPACE_BEGIN

class MicroflakeTriglobe : public PhaseFunction {
public:
    MicroflakeTriglobe(const Properties &props) : PhaseFunction(props) {
	}

    MicroflakeTriglobe(Stream *stream, InstanceManager *manager)
		: PhaseFunction(stream, manager) {
		configure();
	}

    virtual ~MicroflakeTriglobe() { }

	void configure() {
		PhaseFunction::configure();
	}

	void serialize(Stream *stream, InstanceManager *manager) const {
		PhaseFunction::serialize(stream, manager);
	}

	Float eval(const PhaseFunctionSamplingRecord &pRec) const {
        Log(EError, "For this shadowing-based medium, eval_spectrum must be used instead!");
		return 0.0;
	}

	Float eval(const PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
        Log(EError, "For this shadowing-based medium, eval_spectrum must be used instead!");
        return 0.0;
	}

	// peut-être donner un autre nom
    Spectrum eval_spectrum(const PhaseFunctionSamplingRecord &pRec) const {
		return pRec.mRec.trigPhase.eval(pRec.wi, pRec.wo,
										pRec.mRec.ssAlbedo, pRec.mRec.msAlbedo);
    }

	Float sample(PhaseFunctionSamplingRecord &pRec,
				 Sampler *sampler) const {
        Log(EError, "For this shadowing-based medium, sample_spectrum must be used instead!");
		return 0.f;
	}

	Float sample(PhaseFunctionSamplingRecord &pRec,
				 Float &pdf, Sampler *sampler) const {
        Log(EError, "For this shadowing-based medium, sample_spectrum must be used instead!");
		return 0.f;
	}
	
    Float pdf(const PhaseFunctionSamplingRecord &pRec) const {
		return pRec.mRec.trigPhase.pdf(pRec.wi, pRec.wo,
									   pRec.mRec.ssAlbedo, pRec.mRec.msAlbedo);
    }

	Spectrum sample_spectrum(PhaseFunctionSamplingRecord &pRec,
							 Float &pdf, Sampler *sampler) const {
		Spectrum s;
		pRec.wo = pRec.mRec.trigPhase.sample(pRec.wi,
											 pRec.mRec.ssAlbedo, pRec.mRec.msAlbedo,
											 s, pdf, sampler);
		
		return 	s;
	}

	Spectrum sample_spectrum(PhaseFunctionSamplingRecord &pRec,
							 Sampler *sampler) const {
		Spectrum s;
		Float pdf;
		pRec.wo = pRec.mRec.trigPhase.sample(pRec.wi,
											 pRec.mRec.ssAlbedo, pRec.mRec.msAlbedo,
											 s, pdf, sampler);

		return 	s;
	}

	bool needsDirectionallyVaryingCoefficients() const { return true; }

	std::string toString() const {
		std::ostringstream oss;
        oss << "MicroflakeTriglobe[" << endl
			<< "]";
		return oss.str();
	}

	MTS_DECLARE_CLASS()
	private:

};

MTS_IMPLEMENT_CLASS_S(MicroflakeTriglobe, false, PhaseFunction)
MTS_EXPORT_PLUGIN(MicroflakeTriglobe, "Microflake trig lobes phase function");
MTS_NAMESPACE_END
