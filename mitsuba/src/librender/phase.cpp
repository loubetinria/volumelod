#include <mitsuba/render/phase.h>
#include <mitsuba/render/medium.h>

MTS_NAMESPACE_BEGIN

std::string PhaseFunctionSamplingRecord::toString() const {
    std::ostringstream oss;
    oss << "PhaseFunctionSamplingRecord[" << endl
        << "  mRec = " << indent(mRec.toString()) << "," << endl
        << "  wi = " << wi.toString() << "," << endl
        << "  wo = " << wo.toString() << "," << endl
        << "  mode = " << mode << endl
        << "]";
    return oss.str();
}

void PhaseFunction::configure() {
    m_type = 0;
}

Float PhaseFunction::pdf(const PhaseFunctionSamplingRecord &pRec) const {
    return eval(pRec);
}

Float PhaseFunction::pdf(const PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
    Log(EError, "%s::pdf(pRec, sampler) is not implemented", getClass()->getName().c_str());
    return 0.f;
}

Float PhaseFunction::eval(const PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
    Log(EError, "%s::eval(pRec, sampler) is not implemented", getClass()->getName().c_str());
    return 0.f;
}


Spectrum PhaseFunction::eval_spectrum(const PhaseFunctionSamplingRecord &pRec) const {
    Log(EError, "%s::eval_spectrum() is not implemented", getClass()->getName().c_str());
    return Spectrum();
}


Spectrum PhaseFunction::sample_spectrum(PhaseFunctionSamplingRecord &pRec,
                                        Float &pdf, Sampler *sampler) const {
    Log(EError, "%s::sample_spectrum() is not implemented", getClass()->getName().c_str());
    return Spectrum();
}

Spectrum PhaseFunction::sample_spectrum(PhaseFunctionSamplingRecord &pRec,
                                        Sampler *sampler) const {
    Log(EError, "%s::sample_spectrum() is not implemented", getClass()->getName().c_str());
    return Spectrum();
}

bool PhaseFunction::needsDirectionallyVaryingCoefficients() const {
    return false;
}

Float PhaseFunction::sigmaDir(Float cosTheta) const {
    Log(EError, "%s::sigmaDir(Float) is not implemented (this is not "
        "an anisotropic medium!)", getClass()->getName().c_str());
    return 0.0f;
}

Float PhaseFunction::sigmaDirMax() const {
    Log(EError, "%s::sigmaDirMax() is not implemented (this is not "
        "an anisotropic medium!)", getClass()->getName().c_str());
    return 0.0f;
}

Float PhaseFunction::getMeanCosine() const {
    Log(EError, "%s::getMeanCosine() is not implemented!",
        getClass()->getName().c_str());
    return 0.0f;
}

MTS_IMPLEMENT_CLASS(PhaseFunction, true, ConfigurableObject)
MTS_NAMESPACE_END
