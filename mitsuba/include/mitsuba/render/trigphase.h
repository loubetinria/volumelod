#pragma once
#if !defined(__MITSUBA_RENDER_TRIGPHASE_H_)
#define __MITSUBA_RENDER_TRIGPHASE_H_

#include <mitsuba/mitsuba.h>
#include <mitsuba/render/sampler.h>
#include <mitsuba/core/warp.h>

#include <mitsuba/core/logger.h>

#define MIN_ROUGHNESS 0.0001

MTS_NAMESPACE_BEGIN

/*
 * Contains data for cos^n and sin^n phase
 * functions with shadowing, as well as eval, pdf
 * and sampling methods.
 */ 

class MTS_EXPORT_RENDER Shadowing  : public Object {

  public:
	Shadowing();

	Shadowing(Float a1, Float a2, Float a3,
			  Vector3f XA1, Vector3f XA2, Vector3f XA3);

	Shadowing(const Matrix3x3 &m);
	
	Shadowing(const Vector3f &diag, const Vector3f &tri);
	
	void computeEigenV();

	inline bool hasEigenv() const {return m_has_eigenv;}
	
	void print() const;
	
	inline Float shadowing(const Vector3f &w) const {
		return dot(m_SA*w,w);
	}

	Matrix3x3 m_SA;

	inline Vector3f getEigenvalues() const {
		if (!m_has_eigenv)
			Log(EError, "Eigenvalues have not been computed");
		return Vector3f(m_a1, m_a2, m_a3);
	}

	inline Vector3f getXA1() const {
		if (!m_has_eigenv)
			Log(EError, "Eigenvectors have not been computed");
		return m_XA1;
	}

	inline Vector3f getXA2() const {
		if (!m_has_eigenv)
			Log(EError, "Eigenvectors have not been computed");
		return m_XA2;
	}

	inline Vector3f getXA3() const {
		if (!m_has_eigenv)
			Log(EError, "Eigenvectors have not been computed");
		return m_XA3;
	}


	
  protected:

	bool m_has_eigenv;
	
	Float m_a1, m_a2, m_a3;
	Vector3f m_XA1, m_XA2, m_XA3;
	
};

struct MTS_EXPORT_RENDER TrigLobe : public Object {
	/// Used by triglobe based phase function, Distribution
	int n_cos, n_sin;
	Vector3f XD_cos, XD_sin;
	Float w_cos, w_sin;

	TrigLobe() {
		n_cos = 1;
		n_sin = 1;
		XD_cos = Vector3f(1,0,0);
		XD_sin = Vector3f(1,0,0);
		w_cos = 0;
		w_sin = 0;
	}
	
    TrigLobe(int n_cos, int n_sin,
			 Vector3f XD_cos, Vector3f XD_sin,
			 Float w_cos, Float w_sin) : n_cos(n_cos), n_sin(n_sin),
		XD_cos(XD_cos), XD_sin(XD_sin), w_cos(w_cos), w_sin(w_sin) {
	}

	void print() const {
		std::cout << "TrigLobe:  n_cos = " << n_cos << std::endl;
		std::cout << "           n_sin = " << n_sin << std::endl;
		std::cout << "           XD_cos = " << XD_cos.toString() << std::endl;
		std::cout << "           XD_sin  = " << XD_sin.toString() << std::endl;
		std::cout << "           w_cos  = " << w_cos << std::endl;
		std::cout << "           w_sin  = " << w_sin << std::endl;
	}
	
};
	
class MTS_EXPORT_RENDER TrigPhase  : public Object {

  public:

	TrigPhase();
	
    Spectrum eval(Vector3f wi, Vector3f wo, Spectrum albedo_ss, Spectrum albedo_ms) const;
	
    Float pdf(Vector3f wi, Vector3f wo, Spectrum albedo_ss, Spectrum albedo_ms) const;

	Vector3f sample(Vector3f wi, Spectrum albedo_ss, Spectrum albedo_ms, Spectrum &s,
					Float &pdf, Sampler *sampler) const;

	// HELPERS

	void buildOrthonormalBasis(Vector3f& omega_1,
							   Vector3f& omega_2,
							   const Vector3f& omega_3) const;


	Vector3f sample_ss(Vector3f wi, Sampler *sampler) const;

	Vector3f sample_ms(Sampler *sampler) const;

	
	// COMBINED LOBES
	
	Float sigma_t(Vector3f wi) const;

	
	Float sigma_ss(Vector3f wi) const;

	
	Float int_sigma_t() const;

	
	Float int_sigma_ss() const;

	
	float phase_ss(Vector3f wi, Vector3f wo) const;

	
	Float phase_ms(Vector3f wo) const;


	// COS LOBE FUNCTIONS
	
	Float coslobe_sigma_t(int n, Vector3f wi, Vector3f XD, const Shadowing &sh) const;

	
	Float coslobe_sigma(int n, Vector3f wi, Vector3f XD) const;

	
	Float coslobe_sigma_ss(int n, Vector3f wi, Vector3f XD, const Shadowing &sh) const;

	
	Float coslobe_int_sigma_t(int n, Vector3f XD, const Shadowing &sh) const;


	Float coslobe_int_sigma_ss(int n, Vector3f XD, const Shadowing &sh) const;

	Float coslobe_phase_ss(int n, Vector3f wi, Vector3f wo, Vector3f XD, const Shadowing &sh) const;

	Float coslobe_D(int n, Vector3f w, Vector3f XD) const;

	Float coslobe_phase_ms(int n, Vector3f wo, Vector3f XD, const Shadowing &sh) const;

	Vector3f coslobe_sample_visible(int n, Vector3f wi, Vector3f XD,
									int &nb_it,
									Sampler *sampler) const;

	Float coslobe_pdf_squared(int n, Vector3f wi, Vector3f XD,
							  Vector3f wm) const;

	void coslobe_sample_squared(int n, Vector3f wi, Vector3f XD,
								Vector3f &wo,
								Sampler *sampler) const;

	Vector3f coslobe_sample_sigma(int n, Vector3f XD,
								  Sampler *sampler) const;

	Float coslobe_pdf_sigma(int n, Vector3f w, Vector3f XD) const;

	
	Vector3f coslobe_sample_D(int n, Vector3f XD,
							  Sampler *sampler) const;

	Vector3f coslobe_sample_ms(int n, Vector3f XD,
							   const Shadowing &sh,
							   Sampler *sampler) const;


	Vector3f coslobe_sample_ss(int n, Vector3f wi, Vector3f XD,
							   const Shadowing &sh,
							   Sampler *sampler) const;

	// SIN LOBE FUNCTIONS
	
	Float sinlobe_sigma_t(int n, Vector3f wi, Vector3f XD, const Shadowing &sh) const;


	Float sinlobe_sigma(int n, Vector3f wi, Vector3f XD) const;

	
	Float sinlobe_sigma_ss(int n, Vector3f wi, Vector3f XD, const Shadowing &sh) const;

	Float sinlobe_int_sigma_t(int n, Vector3f XD, const Shadowing &sh) const;

	Float sinlobe_int_sigma_ss(int n, Vector3f XD, const Shadowing &sh) const;

	
	Float sinlobe_phase_ss(int n, Vector3f wi, Vector3f wo, Vector3f XD,
						   const Shadowing &sh) const;
	
	Float sinlobe_D(int n, Vector3f w, Vector3f XD) const;
	
	Float sinlobe_phase_ms(int n, Vector3f wo, Vector3f XD, const Shadowing &sh) const;

	Vector3f sinlobe_sample_visible(int n, Vector3f wi, Vector3f XD,
									int &nb_it,
									Sampler *sampler) const;
	
	Float sinlobe_pdf_sigma(int n, Vector3f w, Vector3f XD) const;


	Vector3f sinlobe_sample_sigma(int n, Vector3f XD,
								  Sampler *sampler) const;
	
	Vector3f sinlobe_sample_ms(int n, Vector3f XD,
							   const Shadowing &sh,
							   Sampler *sampler) const;
	
	Vector3f sinlobe_sample_ss(int n, Vector3f wi, Vector3f XD,
							   const Shadowing &sh,
							   Sampler *sampler) const;

	// ISO LOBE FUNCTIONS
		
	Float iso_int_sigma_t(const Shadowing &sh) const;

	Float iso_int_sigma_ss(const Shadowing &sh) const;

	Float iso_sigma_t(Vector3f wi, const Shadowing &sh) const;

	Float iso_sigma_ss(Vector3f wi, const Shadowing &sh) const;

	Float iso_phase_ss(Vector3f wo, const Shadowing &sh) const;

	Float iso_phase_ms(Vector3f wo, const Shadowing &sh) const;

	Vector3f iso_sample_ss(const Shadowing &sh, Sampler *sampler) const;

	Vector3f iso_sample_ms(const Shadowing &sh, Sampler *sampler) const;	

	Shadowing sh;
	TrigLobe tl;

	MTS_DECLARE_CLASS()
protected:


	
};

MTS_NAMESPACE_END

#endif /* __MITSUBA_TRIGPHASE_H_ */
