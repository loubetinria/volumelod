#pragma once
#if !defined(__MITSUBA_CORE_SGGX_H_)
#define __MITSUBA_CORE_SGGX_H_

#include <mitsuba/mitsuba.h>
#include <mitsuba/render/sampler.h>
#include <mitsuba/core/warp.h>

#include <mitsuba/core/logger.h>

#define MIN_ROUGHNESS 0.0001

MTS_NAMESPACE_BEGIN


struct MTS_EXPORT_CORE SGGX  : public Object {

    SGGX() {
        m_diag = Vector3f(0.0);
        m_tri = Vector3f(0.0);
        m_isNull = true;
    }

    SGGX(Vector3f d, Vector3f t);

    SGGX(Vector3f normal, float roughness);

    SGGX(Matrix3x3 matrix);

    SGGX(Spectrum sigma, Spectrum r);

    // normal must be normalized
    void fromSurface(Vector3f normal, float roughnessU, float roughnessV);

    void fromSigmaR(Spectrum sigma, Spectrum r);

    void fromSigmaR(Vector3f sigma, Vector3f r);

    // matrix must be symmetric
    void fromMatrix(Matrix3x3 matrix);

    Matrix3x3 toMatrix() const;

    bool getNormal(Vector3f &normal) const;

    void fromFilteredSGGX(std::vector<SGGX> vec, std::vector<Float> weights);
    
    void printEigenValues() const;
    
    void normalizeSGGX();

    float evalNorm(Sampler *sampler) const;

    void test_sggx(Sampler *sampler);

    float optimalDensity2(float meanTransparency, Sampler *sampler);

    bool isNull() const {
        return m_isNull;
    }

    bool testIsNull() const {
        return (m_diag.length() < 0.0001 && m_tri.length() < 0.00001);
    }

    /*
     * PUBLIC VERSIONS
     */

    Float eval_specular(Vector3f wi, Vector3f wo) const;

    Vector3f sample_specular(const Vector3f wi, const Float U1, const Float U2) const;

    Float eval_diffuse(Vector3f wi, Vector3f wo,
                       const Float U1, const Float U2) const;

    Vector3f sample_diffuse(const Vector3f wi,
                            const float U1, const float U2, const float U3, const float U4) const;

    Vector3f sample_VNDF(const Vector3f wi,
                         const float U1, const float U2) const;

    Float sigma(Vector3f wi) const;

    Vector3f m_diag; // xx, yy, zz

    Vector3f m_tri;  // yx, zx, zy 

    protected:

    /*
     * FROM THE SGGX PAPER
     */

    Vector3f sample_VNDF(const Vector3f wi,
                         const Float S_xx, const Float S_yy, const Float S_zz,
                         const Float S_xy, const Float S_xz, const Float S_yz,
                         const Float U1, const Float U2) const;

    Float D(Vector3f wm,
            Float S_xx, Float S_yy, Float S_zz,
            Float S_xy, Float S_xz, Float S_yz) const;


    Float sigma(Vector3f wi,
                Float S_xx, Float S_yy, Float S_zz,
                Float S_xy, Float S_xz, Float S_yz) const;

    void buildOrthonormalBasis(Vector3f& omega_1, Vector3f& omega_2, const Vector3f& omega_3) const;

    Float eval_specular(Vector3f wi, Vector3f wo,
                        const float S_xx, const float S_yy, const float S_zz,
                        const float S_xy, const float S_xz, const float S_yz) const;
    
    Vector3f sample_specular(const Vector3f wi,
                             const Float S_xx, const Float S_yy, const Float S_zz,
                             const Float S_xy, const Float S_xz, const Float S_yz,
                             const Float U1, const Float U2) const;

    Float eval_diffuse(Vector3f wi, Vector3f wo,
                       const Float S_xx, const Float S_yy, const Float S_zz,
                       const Float S_xy, const Float S_xz, const Float S_yz,
                       const Float U1, const Float U2) const;

    Vector3f sample_diffuse(const Vector3f wi,
                            const float S_xx, const float S_yy, const float S_zz,
                            const float S_xy, const float S_xz, const float S_yz,
                            const float U1, const float U2, const float U3, const float U4) const;

    bool m_isNull;
};

MTS_NAMESPACE_END

#endif /* __MITSUBA_SGGX_RAY_H_ */
